#include <helix.h>

//struct to communicate between processors for voice
struct firV{
	double sample;
	double output;
} firV1, firV2, firV3, firV4, firV5;
//struct to communicate between processors for overlapping sound
struct firO{
	double sample;
	double output;
} firO1, firO2, firO3, firO4, firO5;

//read and write end points of cFIFO buffer for voice
CFifo<double,CFifo<>::w> *wSampleV; // write end point of a FIFO
CFifo<double,CFifo<>::r> *rSampleV; // Read end point of a FIFO
CFifo<firV,CFifo<>::w> *wFIRV1; // Write end point of a FIFO
CFifo<firV,CFifo<>::r> *rFIRV1; // Read end point of a FIFO
CFifo<firV,CFifo<>::w> *wFIRV2; // Write end point of a FIFO
CFifo<firV,CFifo<>::r> *rFIRV2; // Read end point of a FIFO
CFifo<firV,CFifo<>::w> *wFIRV3; // Write end point of a FIFO
CFifo<firV,CFifo<>::r> *rFIRV3; // Read end point of a FIFO
//read and write end points of cFIFO buffer for overlapping sound
CFifo<double,CFifo<>::w> *wSampleO; // write end point of a FIFO
CFifo<double,CFifo<>::r> *rSampleO; // Read end point of a FIFO
CFifo<firO,CFifo<>::w> *wFIRO1; // Write end point of a FIFO
CFifo<firO,CFifo<>::r> *rFIRO1; // Read end point of a FIFO
CFifo<firO,CFifo<>::w> *wFIRO2; // Write end point of a FIFO
CFifo<firO,CFifo<>::r> *rFIRO2; // Read end point of a FIFO
CFifo<firO,CFifo<>::w> *wFIRO3; // Write end point of a FIFO
CFifo<firO,CFifo<>::r> *rFIRO3; // Read end point of a FIFO
//read and write end points of cFIFO buffer for envelope follower
CFifo<double,CFifo<>::w> *wEnv; // write end point of a FIFO
CFifo<double,CFifo<>::r> *rEnv; // Read end point of a FIFO
//read and write end points of cFIFO buffer between VCA and FIR/envelope follower
CFifo<double,CFifo<>::w> *wVCAFIR; // write end point of a FIFO
CFifo<double,CFifo<>::r> *rVCAFIR; // Read end point of a FIFO
CFifo<double,CFifo<>::w> *wVCAEnv; // write end point of a FIFO
CFifo<double,CFifo<>::r> *rVCAEnv; // Read end point of a FIFO
#define ERREXIT(str) {fprintf(stderr, "Error: " str "\n"); exit(1);}
#define ERREXIT2(str, ...) {fprintf(stderr, "Error: " str "\n",__VA_ARGS__); exit(1);}
#define NUMBER_OF_COEFFICIENTS_VOICE	4
#define NUMBER_OF_COEFFICIENTS_OVERLAP	4


//Processor functions for voice
void *procSamplerV(void *arg) {
	wSampleV->validate();
	double sample;
	while(1) {
		sample= rand()%100;
		wSampleV->push(sample);
		sleep(5);
		//printf("1");
	}
	return NULL;
}

void *procFirV1(void *arg) {
	rSampleV->validate();
	wFIRV1->validate();
	double coeff[NUMBER_OF_COEFFICIENTS_VOICE]={1,2,3,4};
	double sample;
	firV1.output =0;
	while(1) {
		sample = rSampleV->front(); //sla sample tijdelijk op
		firV1.sample = sample;		//plaats sample in de struct en stuur die door
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_VOICE;i++)
		{
			firV1.output += sample*coeff[i];		//FIR berekening
		}
		wFIRV1->push(firV1);						//stuur resultaat door naar volgende
		//sleep(1);
		rSampleV->pop();
		//printf("\t\t\tprocFirV1 output: %lf and sample %lf\n", fir1.output, fir1.sample);
		firV1.output = 0;
		firV1.sample = 0;
		//printf("2)");
	}
	return NULL;
}

void *procFirV2(void *arg) {
	wFIRV2->validate();
    rFIRV1->validate();
	double coeff[NUMBER_OF_COEFFICIENTS_VOICE]={1,2,3,4};
	while(1) {
		firV2=rFIRV1->front();
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_VOICE;i++)
		{
			firV2.output += firV2.sample*coeff[i];		//FIR berekening
		}
		wFIRV2->push(firV2);
		//sleep(1);
		rFIRV1->pop();
		//printf("\t\t\tprocFirV2 output: %lf and sample %lf\n", fir2.output, fir2.sample);
		firV2.output = 0;
		firV2.sample = 0;
		//printf("3");
	}
	return NULL;
}

void *procFirV3(void *arg) {
	wFIRV3->validate();
    rFIRV2->validate();
	double coeff[NUMBER_OF_COEFFICIENTS_VOICE]={1,2,3,4};
	while(1) {
		firV3=rFIRV2->front();
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_VOICE;i++)
		{
			firV3.output += firV3.sample*coeff[i];		//FIR berekening
		}
		wFIRV3->push(firV3);
		//sleep(1);
		rFIRV2->pop();
		//printf("\t\t\tprocFirV3 output: %lf and sample %lf\n", fir3.output, fir3.sample);
		firV3.output = 0;
		firV3.sample = 0;
		//printf("4");
	}
	return NULL;
}

void *procFirV4(void *arg) {
    rFIRV3->validate();
    wEnv->validate();
	double coeff[NUMBER_OF_COEFFICIENTS_VOICE]={1,2,3,4};
	while(1) {
		firV4=rFIRV3->front();
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_VOICE;i++)
		{
			firV4.output += firV4.sample*coeff[i];		//FIR berekening
		}
        wEnv->push(firV4.output);
		//sleep(1);
		rFIRV3->pop();
		printf("\t\t\to1: %lf s: %lf\n", firV4.output, firV4.sample);
		firV4.output = 0;
		firV4.sample = 0;
	}
	return NULL;
}
//Processor functions for overlapping sound
void *procSamplerO(void *arg) {
	wSampleO->validate();
	double sample;
	while(1) {
		sample= rand()%100;
		wSampleO->push(sample);
		sleep(5);
	}
	return NULL;
}


void *procFirO1(void *arg) {
	rSampleO->validate();
	wFIRO1->validate();
	double coeff[NUMBER_OF_COEFFICIENTS_OVERLAP]={1,2,3,4};
	double sample;
	firO1.output =0;
	while(1) {
		sample = rSampleO->front(); //sla sample tijdelijk op
		firO1.sample = sample;		//plaats sample in de struct en stuur die door
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_OVERLAP;i++)
		{
			firO1.output += sample*coeff[i];		//FIR berekening
		}
		wFIRO1->push(firO1);						//stuur resultaat door naar volgende
		//sleep(1);
		rSampleO->pop();
		//printf("\t\t\tprocFirO1 output: %lf and sample %lf\n", fir1.output, fir1.sample);
		firO1.output = 0;
		firO1.sample = 0;
	}
	return NULL;
}

void *procFirO2(void *arg) {
	wFIRO2->validate();
    rFIRO1->validate();
	double coeff[NUMBER_OF_COEFFICIENTS_OVERLAP]={1,2,3,4};
	while(1) {
		firO2=rFIRO1->front();
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_OVERLAP;i++)
		{
			firO2.output += firO2.sample*coeff[i];		//FIR berekening
		}
		wFIRO2->push(firO2);
		//sleep(1);
		rFIRO1->pop();
		//printf("\t\t\tprocFirO2 output: %lf and sample %lf\n", fir2.output, fir2.sample);
		firO2.output = 0;
		firO2.sample = 0;
	}
	return NULL;
}

void *procFirO3(void *arg) {
	wFIRO3->validate();
    rFIRO2->validate();
	double coeff[NUMBER_OF_COEFFICIENTS_OVERLAP]={1,2,3,4};
	while(1) {
		firO3=rFIRO2->front();
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_OVERLAP;i++)
		{
			firO3.output += firO3.sample*coeff[i];		//FIR berekening
		}
		wFIRO3->push(firO3);
		//sleep(1);
		rFIRO2->pop();
		//printf("\t\t\tprocFirO3 output: %lf and sample %lf\n", fir3.output, fir3.sample);
		firO3.output = 0;
		firO3.sample = 0;
	}
	return NULL;
}

void *procFirO4(void *arg) {
    rFIRO3->validate();
    wVCAFIR->validate();
	double coeff[NUMBER_OF_COEFFICIENTS_OVERLAP]={1,2,3,4};
	while(1) {
		firO4=rFIRO3->front();
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_OVERLAP;i++)
		{
			firO4.output += firO4.sample*coeff[i];		//FIR berekening
		}
        wVCAFIR->push(firO4.output);
		//sleep(1);
		rFIRO3->pop();
		printf("\t\t\to2: %lf s: %lf\n", firO4.output, firO4.sample);
		firO4.output = 0;
		firO4.sample = 0;
	}
	return NULL;
}
void *procEnv(void *arg) {
    rEnv->validate();
    double env;
	while(1) {
		//amplitude detection
		env=rEnv->front();        
        
        printf("\t\t\env: %lf \n",env);
        wVCAEnv->push(2.1);
	}
	return NULL;
}

void *procVCA(void *arg) {
    rVCAEnv->validate();
    rVCAFIR->validate();
    double fir;
    double env;
	while(1) {
		//amplitude scaling
		//env=rVCAEnv->front();
		//fir=rVCAFIR->front();
		printf("\t\t\VCA fir: %lf and env: %lf \n",(double)rVCAFIR, (double)rVCAEnv);
        
        rVCAEnv->pop();
        rVCAFIR->pop();
	}
	return NULL;
}


int main(int argc, char **argv) {
	pid_t pidV0, pidV1, pidV2, pidV3, pidV4, pidO0, pidO1, pidO2, pidO3, pidO4, pidEnv, pidVCA;

	//create cFIFO for FIR voice(V)
	CFifoPtr<double> cFifoSampleV = CFifo<double>::Create(1, wSampleV, 2, rSampleV, 50);
	CFifoPtr<firV> cFifoFIRV1 = CFifo<firV>::Create(2, wFIRV1, 3, rFIRV1, 50);
	CFifoPtr<firV> cFifoFIRV2 = CFifo<firV>::Create(3, wFIRV2, 4, rFIRV2, 50);
	CFifoPtr<firV> cFifoFIRV3 = CFifo<firV>::Create(4, wFIRV3, 5, rFIRV3, 50);
	//create cFIFO for FIR overlapping(O) sound
	CFifoPtr<double> cFifoSampleO = CFifo<double>::Create(6, wSampleO, 7, rSampleO, 50);
	CFifoPtr<firO> cFifoFIRO1 = CFifo<firO>::Create(7, wFIRO1, 8, rFIRO1, 50);
	CFifoPtr<firO> cFifoFIRO2 = CFifo<firO>::Create(8, wFIRO2, 9, rFIRO2, 50);
	CFifoPtr<firO> cFifoFIRO3 = CFifo<firO>::Create(9, wFIRO3, 10, rFIRO3, 50);
    //create cFIFO for envelope follower 
    CFifoPtr<double> cFifoEnv = CFifo<double>::Create(5, wEnv, 11, rEnv, 50);
    //create cFIFO for VCA
    CFifoPtr<double> cFifoVCAFIR = CFifo<double>::Create(10, wVCAFIR, 12, rVCAFIR, 50);
    CFifoPtr<double> cFifoVCAEnv = CFifo<double>::Create(11, wVCAEnv, 12, rVCAEnv, 50);
	//create cFIFO for FIR voice
	if(!cFifoSampleV.valid()) ERREXIT("Error creating buffer");
	if(!cFifoFIRV1.valid()) ERREXIT("Error creating buffer");
	if(!cFifoFIRV2.valid()) ERREXIT("Error creating buffer");
	if(!cFifoFIRV3.valid()) ERREXIT("Error creating buffer");
	//create cFIFO for FIR overlapping sound
	if(!cFifoSampleO.valid()) ERREXIT("Error creating buffer");
	if(!cFifoFIRO1.valid()) ERREXIT("Error creating buffer");
	if(!cFifoFIRO2.valid()) ERREXIT("Error creating buffer");
	if(!cFifoFIRO3.valid()) ERREXIT("Error creating buffer");
	//create cFIFO for envelope follower
	if(!cFifoEnv.valid()) ERREXIT("Error creating buffer");
	//create cFIFO for FIR VCA
	if(!cFifoVCAEnv.valid()) ERREXIT("Error creating buffer");
	if(!cFifoVCAFIR.valid()) ERREXIT("Error creating buffer");
	//create processes for voice filtering
	if(int e=CreateProcess(pidV0, procSamplerV, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 1)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidV1, procFirV1, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 2)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidV2, procFirV2, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 3)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidV3, procFirV3, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 4)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidV4, procFirV4, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 5)) ERREXIT2("Process creation failed: %i", e);

	//create processes for overlapping sound filtering
	if(int e=CreateProcess(pidO0, procSamplerO, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 6)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidO1, procFirO1, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 7)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidO2, procFirO2, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 8)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidO3, procFirO3, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 9)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidO4, procFirO4, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 10)) ERREXIT2("Process creation failed: %i", e);
	//create process for envelope follower
	if(int e=CreateProcess(pidEnv, procEnv, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 11)) ERREXIT2("Process creation failed: %i", e);
    //create process for VCA
	if(int e=CreateProcess(pidVCA, procEnv, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 12)) ERREXIT2("Process creation failed: %i", e);

    //set process flaggs for voice
	if(int e=SetProcessFlags(pidV0, PROC_FLAG_JOINABLE, 1)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidV1, PROC_FLAG_JOINABLE, 2)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidV2, PROC_FLAG_JOINABLE, 3)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidV3, PROC_FLAG_JOINABLE, 4)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidV4, PROC_FLAG_JOINABLE, 5)) ERREXIT2("While setting process flags: %i", e);

	//set process flaggs for overlapping sound
	if(int e=SetProcessFlags(pidO0, PROC_FLAG_JOINABLE, 6)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidO1, PROC_FLAG_JOINABLE, 7)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidO2, PROC_FLAG_JOINABLE, 8)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidO3, PROC_FLAG_JOINABLE, 9)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidO4, PROC_FLAG_JOINABLE, 10)) ERREXIT2("While setting process flags: %i", e);
    //set process flaggs for envelope follower
	if(int e=SetProcessFlags(pidEnv, PROC_FLAG_JOINABLE, 11)) ERREXIT2("While setting process flags: %i", e);
    //set process flaggs for VCA
	if(int e=SetProcessFlags(pidVCA, PROC_FLAG_JOINABLE, 12)) ERREXIT2("While setting process flags: %i", e);

	//start processes of voice
	if(int e=StartProcess(pidV0, 1)) ERREXIT2("Could not start procSamplerV: %i", e);
	if(int e=StartProcess(pidV1, 2)) ERREXIT2("Could not start procFirV1: %i", e);
	if(int e=StartProcess(pidV2, 3)) ERREXIT2("Could not start procFirV2: %i", e);
	if(int e=StartProcess(pidV3, 4)) ERREXIT2("Could not start procFirV3: %i", e);
	if(int e=StartProcess(pidV4, 5)) ERREXIT2("Could not start procFirV4: %i", e);
	//start processes of overlapping sound
	if(int e=StartProcess(pidO0, 6)) ERREXIT2("Could not start procSamplerO: %i", e);
	if(int e=StartProcess(pidO1, 7)) ERREXIT2("Could not start procFirO1: %i", e);
	if(int e=StartProcess(pidO2, 8)) ERREXIT2("Could not start procFirO2: %i", e);
	if(int e=StartProcess(pidO3, 9)) ERREXIT2("Could not start procFirO3: %i", e);
	if(int e=StartProcess(pidO4, 10)) ERREXIT2("Could not start procFirO4: %i", e);
    //start processes of envelope follower
	if(int e=StartProcess(pidEnv, 11)) ERREXIT2("Could not start procEnv: %i", e);
    //start processes of VCA
	if(int e=StartProcess(pidVCA, 12)) ERREXIT2("Could not start procVCA: %i", e);
	// FIFOs of voice are destroyed when the pointers goes out of scope
	if(int e=WaitProcess(pidV0, NULL, 1)) ERREXIT2("Waiting on pidV0 %i@%i: %i\n", pidV0, 1, e);

	if(int e=WaitProcess(pidV1, NULL, 2)) ERREXIT2("Waiting on pidV1 %i@%i: %i\n", pidV1, 2, e);

	if(int e=WaitProcess(pidV2, NULL, 3)) ERREXIT2("Waiting on pidV2 %i@%i: %i\n", pidV2, 3, e);

	if(int e=WaitProcess(pidV3, NULL, 4)) ERREXIT2("Waiting on pidV3 %i@%i: %i\n", pidV3, 4, e);

	if(int e=WaitProcess(pidV4, NULL, 5)) ERREXIT2("Waiting on pidV4 %i@%i: %i\n", pidV4, 5, e);

	// FIFOs of overlapping sound are destroyed when the pointers goes out of scope
	if(int e=WaitProcess(pidO0, NULL, 6)) ERREXIT2("Waiting on pidO0 %i@%i: %i\n", pidO0, 6, e);

	if(int e=WaitProcess(pidO1, NULL, 7)) ERREXIT2("Waiting on pidO1 %i@%i: %i\n", pidO1, 7, e);

	if(int e=WaitProcess(pidO2, NULL, 8)) ERREXIT2("Waiting on pidO2 %i@%i: %i\n", pidO2, 8, e);

	if(int e=WaitProcess(pidO3, NULL, 9)) ERREXIT2("Waiting on pidO3 %i@%i: %i\n", pidO3, 9, e);

	if(int e=WaitProcess(pidO4, NULL, 10)) ERREXIT2("Waiting on pidO4 %i@%i: %i\n", pidO4, 10, e);
    // envelope follower is destroyed when the pointers goes out of scope
	if(int e=WaitProcess(pidEnv, NULL, 11)) ERREXIT2("Waiting on procEnv %i@%i: %i\n", pidEnv, 11, e);
    // VCA is destroyed when the pointers goes out of scope
	if(int e=WaitProcess(pidVCA, NULL, 12)) ERREXIT2("Waiting on procVCA %i@%i: %i\n", pidVCA, 12, e);

	return 0;
}
