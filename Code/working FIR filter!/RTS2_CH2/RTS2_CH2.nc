#!/bin/bash
function printval {
	echo -n -e "`printf "0x%08x" $1 | sed 's/0x\\(..\\)\\(..\\)\\(..\\)\\(..\\)/\\\\x\\1\\\\x\\2\\\\x\\3\\\\x\\4/'`"
}
case $1 in
	"" | "-u" )
		{
			printval 0x80000000
			printval 205408
			cat /home/rts4/workspace/RTS2_CH2/RTS2_CH2.bin
			printval 0x88000000
			printval 0
			cat /home/rts4/workspace/RTS2_CH2/RTS2_CH2.data
			printval 0xbef00000
			printval 4096
			cat /home/rts4/workspace/RTS2_CH2/RTS2_CH2.ramfile
		} |	netcat 192.168.1.1 7770 ;;
	"-t" )
		wget -O - ftp://192.168.1.1/var/run/helix_mb*.trace | /local/rts2/starburst_32cores/src/os/util/trace2vcd "/home/rts4/workspace/RTS2_CH2/RTS2_CH2.fun" > "/home/rts4/workspace/RTS2_CH2/RTS2_CH2.vcd" 2> "/home/rts4/workspace/RTS2_CH2/RTS2_CH2.stats" ;;
	"-w" )
		{
			printval $2
			printval 4
			printval $3
		} |	netcat 192.168.1.1 7770 ;;
	"-d" )
		{
			printval $2
			length=`printf "%u" $3`
			printval $length
			{ cat; while true; do echo -e -n "\\x00"; done; } | head -c $length
		} |	netcat 192.168.1.1 7770 ;;
	*)
		echo "Usage: $0 [-u|-t|-w address value|-d address length], where"
		echo "	-u   upload program (default)"
		echo "	-t   download traces via Linux"
		echo "	-w   write a word to memory"
		echo "	-d   dump stdin to memory (with maximum of length, padded with zeros)"
		exit 1;;
esac
