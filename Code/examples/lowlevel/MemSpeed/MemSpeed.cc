#include <helix.h>
#include <stdio.h>

#define ITERATIONS	10000000

static double loop_overhead_cycles=2.0;

void test_loop(){
	struct timeval start,end;
	if(unlikely(gettimeofday(&start,NULL)!=0)){
		printf("error %d\n",errno);
		abort();
	}

	int i=0;
	asm volatile ("msrclr r0, 2;");
	DUFF8(i,ITERATIONS*8,1,asm volatile (""));
	asm volatile ("msrset r0, 2;");
	
	if(unlikely(gettimeofday(&end,NULL)!=0)){
		printf("error %d\n",errno);
		abort();
	}
	
	loop_overhead_cycles=
		(end.tv_sec+(double)end.tv_usec/1000000.0-
		(start.tv_sec+(double)start.tv_usec/1000000.0))/(double)ITERATIONS*(double)GetClockFreq();
	printf("loop overhead: %.2f cycles",loop_overhead_cycles);
	if(loop_overhead_cycles<1.9)
		printf(" (that's odd for 2 instructions)\n");
	else if(loop_overhead_cycles<2.1)
		printf(" (branch prediction detected)\n");
	else if(loop_overhead_cycles<4.1)
		printf(" (no branch prediction)\n");
	else
		printf(" (unexpectedly large)\n");
}

#define TEST_INIT()									\
	struct timeval start,end;

#define TEST_START()								\
	if(unlikely(gettimeofday(&start,NULL)!=0)){		\
		printf("error %d\n",errno);					\
		abort();									\
	}												\
	asm volatile ("msrclr r0, 2;");
#define TEST_STOP(iterations,cycles,overhead)		\
	if(unlikely(gettimeofday(&end,NULL)!=0)){		\
		printf("error %d\n",errno);					\
		abort();									\
	}												\
	asm volatile ("msrset r0, 2;");					\
	cycles=											\
		(end.tv_sec+(double)end.tv_usec/1000000.0-	\
		(start.tv_sec+(double)start.tv_usec/1000000.0))/(double)(iterations)*(double)GetClockFreq()-(overhead);

typedef volatile int mem_t;

void test_mem(mem_t* mem){
	TEST_INIT();
	printf("mem@%10p: ",mem);

	// read test
	TEST_START();
	int it=0;
	DUFF8(it,ITERATIONS,1,*mem);
	TEST_STOP(ITERATIONS,double cycles_per_read,loop_overhead_cycles/8.0);
	
	//write test
	TEST_START();
	it=0;
	DUFF8(it,ITERATIONS,1,*mem=0);
	TEST_STOP(ITERATIONS,double cycles_per_write,loop_overhead_cycles/8.0);
	
	// done
	printf("%6.2f cycles/read, %6.2f cycles/write\n",cycles_per_read,cycles_per_write);
}

extern mem_t __procbss_end;
static void test_local(){
	printf("Local  ");
	test_mem((mem_t*)(&__procbss_end-1));
}

static void test_fifo(){
	printf("Fifo   ");
	mem_t* m=(mem_t*)fmalloc(sizeof(mem_t));
	if(!m)
		printf(" OOM\n");
	else{
		test_mem(m);
		ffree((void*)m);
	}
}

static void test_heap(){
	printf("Heap   ");
	mem_t* m=(mem_t*)malloc(sizeof(mem_t));
	if(!m)
		printf(" OOM\n");
	else{
		test_mem(m);
		free((void*)m);
	}
}

static mem_t bss_test;
static void test_bss(){
	printf("Bss    ");
	test_mem(&bss_test);
}

static void test_shared(){
	printf("Shared ");
	mem_t* m=(mem_t*)smalloc(sizeof(mem_t));
	if(!m)
		printf(" OOM\n");
	else{
		test_mem(m);
		sfree((void*)m);
	}
}

static void test_cache(){
	printf("Cache  ");
	TEST_INIT();
	void* p=malloc(0x20000); // twice size of cache
	if(!p){
		printf(" OOM\n");
		return;
	}
	const unsigned int mask=0xffff0000;
	mem_t* mem=(mem_t*)(((intptr_t)p+0x10000)&mask); //size of cache, will be rounded down to 64K boundary
	mem_t* mem2=mem;
	int offset=0;
	printf("mem@%10p: ",mem);
	FlushDCache();
	
	int it=ITERATIONS/16;
	TEST_START();
	asm volatile (
".Lwloop:"
		"lwi	r0, %1, 0x00;"
		"lwi	r0, %1, 0x20;"
		"lwi	r0, %1, 0x40;"
		"lwi	r0, %1, 0x60;"
		"lwi	r0, %1, 0x80;"
		"lwi	r0, %1, 0xa0;"
		"lwi	r0, %1, 0xc0;"
		"lwi	r0, %1, 0xe0;"
		"lwi	r0, %1, 0x100;"
		"lwi	r0, %1, 0x120;"
		"lwi	r0, %1, 0x140;"
		"lwi	r0, %1, 0x160;"
		"lwi	r0, %1, 0x180;"
		"lwi	r0, %1, 0x1a0;"
		"lwi	r0, %1, 0x1c0;"
		"lwi	r0, %1, 0x1e0;"
		"addik	%0, %0, 0x200;"
		"andn	%0, %0, %4;"
		"addk	%1, %3, %0;"
		"addik	%2, %2, -1;"
		"bnei	%2, .Lwloop;"
		:
		"=&r"(offset),	// %0
		"=&r"(mem2),	// %1
		"=&r"(it)		// %2
		:
		"r"(mem),		// %3
		"r"(mask),   	// %4
		"0"(offset), 	// %0
		"2"(it)		 	// %2
		:
	);
	TEST_STOP(ITERATIONS,double cycles_per_burst,(loop_overhead_cycles+3.0)/16.0);

	free(p);
	printf("%6.2f cycles/burst read\n",cycles_per_burst);
}

int main(int argc,char** argv){
	printf("\n\nMemory speed tester\n");
	test_loop();
	
	prf_mem_snapshot();

	printf("\nWith D-cache\n");
	test_local();
	test_fifo();
	test_heap();
	test_bss();
	test_shared();
	test_cache();

	printf("\nWithout D-cache\n");
	DisableDCache();
	test_local();
	test_fifo();
	test_heap();
	test_bss();
	test_shared();
	test_cache();
	
	printf("\nWithout D-cache and TFT disabled\n");
	dvi_turn_off();
	test_local();
	test_fifo();
	test_heap();
	test_bss();
	test_shared();
	test_cache();

	prf_mem_dump();
	return 0;
}

