#include <sys/io.h>

// fake being Helix for cache functions
const os_state_t os_state=OS_BOOT;

#ifdef __cplusplus
extern "C" {
#endif

#define DDR_BASE		0xb0100000
#define DDR_SIZE		0x0fc00000
//#define DDR_BASE		0xbf000000
//#define DDR_SIZE		0x01000000
#define DCACHE_RANGE1	DDR_BASE
#define DCACHE_RANGE2	0xbfff0000
#define DCACHE_RANGE	0x20000
#define TEST_GOOD		0x100
#define TEST_PATTERN1	0xff
#define TEST_PATTERN2	0x7d
#define TEST_PATTERN3	0xffffffff
#define TEST_PATTERN4	0x19caf327
#define TEST_CHUNK		0x100000
#define MAX_ERRORS		16

// do not use UART, as it is too slow for simulation; dump to LEDs instead
#if 0
#  define outbyte(c)	leds_value((int)(c))
#  define _puts(s)		outbyte(*s)
#endif

size_t GetICacheSize(){
	return 0x10000;
}

size_t GetDCacheSize(){
	return 0x10000;
}


void _putc(char c){
	c&=0xf;
	if(c<10)
		outbyte(c+'0');
	else
		outbyte(c+'A'-10);
}

void putp(void* p){
	_puts("0x");
	char* c=(char*)&p;
	for(unsigned int i=0;i<sizeof(p);i++){
		_putc(c[i]>>4);
		_putc(c[i]);
	}
}

// http://en.wikipedia.org/wiki/Random_number_generator
static unsigned int m_w = 0xdeadbeef;    /* must not be zero, nor 0x464fffff */
static unsigned int m_z = 0xcafebabe;    /* must not be zero, nor 0x9068ffff */
static void seed_random(unsigned int i){
	m_w=i==0x464fffff||i==0?0xdeadbeef:i;
	m_z=(~i)==0x9068ffff||(~i)==0?0xcafebabe:i;
}
static unsigned int get_last_random(){
	return (m_z << 16) + m_w;  /* 32-bit result */
}
static unsigned int get_random(){
	m_z = 36969 * (m_z & 65535) + (m_z >> 16);
	m_w = 18000 * (m_w & 65535) + (m_w >> 16);
	return get_last_random();
}

bool test_char(const char pattern){
	_puts("  Testing with pattern ");
	putp((void*)(int)pattern);
	_puts("...\n");

	volatile unsigned char* p;
	int errors=0;
	for(p=(volatile unsigned char*)DDR_BASE;p<(volatile unsigned char*)(DDR_BASE+TEST_GOOD);p++){
		_puts("  Writing byte to ");putp((void*)p);_puts("...\n");
		*p=TEST_PATTERN1;
	}
	barrier();
	for(p=(volatile unsigned char*)DDR_BASE;p<(volatile unsigned char*)(DDR_BASE+TEST_GOOD);p++){
		_puts("  Reading byte from ");putp((void*)p);_puts("...");
		unsigned char read=*p;
		if(*p!=TEST_PATTERN1){
			_puts(" ERROR: read ");putp((void*)(int)read);_puts(" expected ");putp((void*)(int)pattern);_puts("\n");
			if(++errors>MAX_ERRORS){
				_puts("Too many errors. Aborted!\n");
				return false;
			}
		}else
			_puts(" ok\n");
	}
	return errors==0;
}

bool test_word(bool usepattern, const unsigned int pattern){
	if(usepattern){
		_puts("  Testing with pattern ");
		putp((void*)pattern);
		_puts("...\n");
	}else
		_puts("  Testing with random number...\n");

	seed_random(pattern);
	volatile unsigned int* p;
	int errors=0;
	for(p=(volatile unsigned int*)DDR_BASE;p<(volatile unsigned int*)(DDR_BASE+DDR_SIZE);){
		_puts("  Writing ");putp((void*)p);_puts(" - ");putp((void*)(p+TEST_CHUNK));_puts("...\n");
		for(int i=0;i<TEST_CHUNK;i++){
			*p=usepattern?pattern:get_random();
			p++;
		}
	}
	barrier();
	
	seed_random(pattern);
	for(p=(volatile unsigned int*)DDR_BASE;p<(volatile unsigned int*)(DDR_BASE+DDR_SIZE);){
		_puts("  Reading ");putp((void*)p);_puts(" - ");putp((void*)(p+TEST_CHUNK));_puts("...\n");
		for(int i=0;i<TEST_CHUNK;i++){
			unsigned int read=*p;
			if(usepattern&&read!=pattern){
				_puts("  Test FAILED at ");putp((void*)p);_puts(", data read: ");putp((void*)read);
				_puts(" expected: ");putp((void*)pattern);_puts("\n");
				errors++;
			}else if(!usepattern&&read!=get_random()){
				_puts("  Test FAILED at ");putp((void*)p);_puts(", data read: ");putp((void*)read);
				_puts(" expected: ");putp((void*)get_last_random());_puts("\n");
				errors++;
			}
			if(errors>=MAX_ERRORS){
				_puts("Too many errors. Aborted!\n");
				return false;
			}
			p++;
		}
	}
	return errors==0;
}

bool test_leds(){
	_puts("* Turning on all leds...\n");
	leds_on(-1);
	return true;
}

bool test_ddr_basic(){
	bool success=true;
	_puts("* Testing DDR communication, base:");putp((void*)DDR_BASE);_puts("\n");
	success&=test_char(TEST_PATTERN1);
	success&=test_char(TEST_PATTERN2);
	return success;
}

unsigned int test_dcache_(void*,size_t);

bool test_dcache(void* range){
	_puts("* Testing DCache, base:");putp(range);_puts("\n");
//	volatile intptr_t* p=(volatile intptr_t*)range;
//	for(int i=DCACHE_RANGE/sizeof(*p)-1;i>=0;i--)
//		p[i]=(intptr_t)&p[i];
	InvalidateDCache();
	int res=test_dcache_(range,DCACHE_RANGE);

	switch(res){
	case 1:break;
	case 2:_puts("Error in single word read test\n");break;
	case 3:_puts("Error in half word read test\n");break;
	case 4:_puts("Error in byte read test\n");break;
	default:_puts("DCache error ");putp((void*)res);_puts("\n");
	}
	return res==1;
}

bool test_ddr_full(){
	bool success=true;
	_puts("* Testing DDR memory, base:");putp((void*)DDR_BASE);_puts("\n");
	success&=test_word(true,TEST_PATTERN3);
	success&=test_word(true,TEST_PATTERN4);
	success&=test_word(false,0);
	return success;
}

bool test_video(){
	barrier();
	_puts("* Testing video...\n");
	dvi_set_address((void*)(DDR_BASE+0x1fffff));
	for(int y=0;y<256;y++)
		for(int c=0;c<3;c++)
			for(int x=0;x<=IO_DVI_WIDTH/3;x++){
				dvi_set_pixel(x,y,y,0,0);
				dvi_set_pixel(x+IO_DVI_WIDTH/3,y,0,y,0);
				dvi_set_pixel(x+IO_DVI_WIDTH*2/3,y,0,0,y);
			}
	for(int y=0;y<256;y++)
		for(int c=0;c<3;c++)
			for(int x=0;x<=IO_DVI_WIDTH/3;x++){
				dvi_set_pixel(x,y+256,255,y*2,y*2);
				dvi_set_pixel(x+IO_DVI_WIDTH/3,y+256,y*2,255,y*2);
				dvi_set_pixel(x+IO_DVI_WIDTH*2/3,y+256,y*2,y*2,255);
			}
	return true;
}

int main(int argc, char** argv){
	dvi_turn_off();
	_puts("\nDevice checker\n");
	dvi_turn_on();
	dvi_set_address((void*)DDR_BASE);

	bool success=true;
	success&=test_leds();
	success&=test_ddr_basic();

	success&=test_dcache((void*)DCACHE_RANGE1);
	success&=test_dcache((void*)(DCACHE_RANGE1+0x4));
	success&=test_dcache((void*)DCACHE_RANGE2);
	success&=test_dcache((void*)(DCACHE_RANGE2+0x4));
	
	_puts("* Enabling instruction cache...\n");
	InitICache();
	success&=test_ddr_full();
	
	_puts("* Enabling data cache...\n");
	InitDCache();
	success&=test_ddr_full();
	success&=test_video();

	FlushDCache();

	if(success)
		_puts("* Test finished\n");
	else
		_puts("* Test FAILED\n");
	while(1);
	return 0;
}

#ifdef __cplusplus
} // extern "C"
#endif

