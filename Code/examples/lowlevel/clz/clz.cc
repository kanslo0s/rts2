#include <stdio.h>
#include <sys/device.h>

void clz_test(unsigned int val){
	printf("clz(0x%08x): %d\n",val,clz(val));
}

int main(int argc,char** argv){
	clz_test(0);
	clz_test(0x10);
	clz_test(0x00314159);
	return 0;
}

