/*
 * bs_joystick.cc - Demo showing 2 core interaction with visual output and joystick input
 * Note: this example does not use core to core FIFOs but rather manages memory consistency (for communication) directly.
 *
 *  Created on: Apr 2, 2012
 *      Author: Berend Dekens <b.h.j.dekens@utwente.nl>
 */

#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>

#include <render/render.h>
#include <par.h>
#include <mc.h>
#include <helix.h>

#define PRINT_EVENTS 1

#define BUTTON_Y_OFFSET 400
#define BUTTON_X_OFFSET 200
#define BUTTON_X_SPACE 8
#define BUTTON_Y_SIZE 12
#define BUTTON_X_SIZE 20

#define AXIS2_Y_OFFSET 100
#define AXIS_Y_OFFSET 250
#define AXIS_X_OFFSET 200
#define AXIS_Y_SIZE 100
#define AXIS_X_SIZE 20
#define AXIS_X_SPACE 12

#define BGCOL gray
#define NUM_BUTTONS 11
#define NUM_AXIS 8

#define JS_EVENT_BUTTON         0x01    /* button pressed/released */
#define JS_EVENT_AXIS           0x02    /* joystick moved */
#define JS_EVENT_INIT           0x80    /* initial state of device */

// This sequence is the kill switch to cleanly shut down
#define KILL_TEST buttons[1] + buttons[3] + buttons[6] == 3

typedef struct {
  uint32_t time;     /* event timestamp in milliseconds */
  int16_t value;    /* value */
  uint8_t type;      /* event type */
  uint8_t number;    /* axis/button number */
} __attribute__((__packed__)) js_event_t;

// The shared arrays are used to update state
int buttons[NUM_BUTTONS] ATTR_DATA_ALIGNED;
int axis[NUM_AXIS] ATTR_DATA_ALIGNED;

// Process to read from the joystick device
int readjoystick();
// Process to draw the state
int drawstate();

void renderbutton(int number, int pushed);
void renderaxis(int number, short value);
void renderaxis2(int number, int n1, int n2, short x, short y);

int main(int argc, char** args) {
	printf("Joystick demo with visual feedback\n");

	pthread_t read, draw;
	if(par(readjoystick, &read) != 0) goto error_exit;
	if(par(drawstate, &draw) != 0) goto error_exit;

	if(pthread_join(draw, NULL) != 0) goto error_exit;
	if(pthread_join(read, NULL) != 0) goto error_exit;
	printf("done\n");

	return 0;
error_exit:
	perror("Error starting demo");
	return 1;
}

/**
 * Process thread to pull data from the linux device interface. Note that using the analog interfaces
 * can introduce some lag.
 */
int readjoystick() {
	js_event_t e;

	// Open the device
	printf("Opening joystick...\n");
	int fh = open("/dev/input/js0", O_RDONLY);
	if(fh == -1) {
		printf("Error: could not open /dev/input/js0!\n");
		return 0;
	}
	printf("Device open\n");

	int kill = 0;	// Kill flag, triggered by button combo
	while( read(fh, &e, sizeof(js_event_t)) > 0 && kill == 0) {
#if PRINT_EVENTS
		printf("Time: 0x%08X - Value: %hi - Type: %hhu - Number: %hhu\n", (unsigned int)e.time, e.value, e.type, e.number);
#endif
		if((e.type & JS_EVENT_BUTTON) != 0) {
			if(e.number < NUM_BUTTONS) {
				mc_entry_wou(buttons);
				buttons[e.number] = e.value;
				mc_exit_wou(buttons);
			}
		}

		if((e.type & JS_EVENT_AXIS) != 0) {
			if(e.number < NUM_AXIS) {
				mc_entry_wou(axis);
				axis[e.number] = e.value;
				mc_exit_wou(axis);
			}
		}

		// Kill switch
		if(KILL_TEST) kill = 1;
	}

	close(fh);

	return 0;
}

/**
 * Process function to render the state of the joystick multiple times per second
 */
int drawstate() {
	if(render_init(1) != RENDER_OK) {
		printf("Error: init display!\n");
		return 0;
	}

	// Render all holders
	for(int j=0;j<2;j++) {
		fillrect(0,0,639,479,BGCOL);
		for(int i=0;i<NUM_BUTTONS;i++) {
			renderbutton(i,0);
			buttons[i] = 0;
		}
		char buff[64];
		snprintf(buff, 64, "Buffer %i", j);
		drawstring(10,460,buff,black);
		drawstring(10,10,"Kill sequence: 1+3+6 = Y+B+Back",yellow);
		render_flip_buffer();
	}

	// Go into the update loop
	int kill = 0;
	while(kill == 0) {
		// Render all buttons
		mc_entry_ro(buttons);
		for(int i=0;i<NUM_BUTTONS;i++) {
			renderbutton(i, buttons[i]);
		}
		mc_exit_ro(buttons);

		// Render all axis
		mc_entry_ro(axis);
		for(int i=0;i<NUM_AXIS;i++) {
			renderaxis(i, axis[i]);
		}
		// Manual fix: combine the axis controllers as well
		renderaxis2(0, 0, 1, axis[0], axis[1]);
		renderaxis2(1, 3, 4, axis[3], axis[4]);
		renderaxis2(2, 6, 7, axis[6], axis[7]);
		mc_exit_ro(axis);

		// Kill switch
		if(KILL_TEST) kill = 1;
		if(kill == 1)
			drawstring(200, 230, "KILL SEQUENCE DETECTED - DEMO ENDED", red);

		// Flip to the other screen
		render_flip_buffer();

		// Delay until the next redraw
		usleep(10000);
	}

	return 0;
}

/**
 * Render the indicator for the button status.
 * @param number Id of the button
 * @param pushed 0 for unpressed, something else for pressed
 */
void renderbutton(int number, int pushed) {
	int x1 = number * (BUTTON_X_SPACE + BUTTON_X_SIZE) + BUTTON_X_OFFSET;
	int x2 = number * (BUTTON_X_SPACE + BUTTON_X_SIZE) + BUTTON_X_OFFSET + BUTTON_X_SIZE;
	int y1 = BUTTON_Y_OFFSET;
	int y2 = BUTTON_Y_OFFSET + BUTTON_Y_SIZE;

	// Prepare the string
	char buff[32];
	snprintf(buff, 32, "%i", number);

	// Draw the boundary of the box
	drawrect(x1,y1,x2,y2,black);
	// Either fill it with the background, or the foreground
	if(pushed == 0) {
		fillrect(x1+1,y1+1,x2-1,y2-1,gray);
		drawstring(x1+3, y2-2, buff, red);
	} else {
		fillrect(x1+1,y1+1,x2-1,y2-1,green);
		drawstring(x1+3, y2-2, buff, black);
	}
}

/**
 * Render a rectangle with a gauge to indicate the reading from the joystick. The
 * value can range from -32767 to 32768.
 * The axis number sets the offset on the X axis to keep all axis separated.
 */
void renderaxis(int number, short value) {
	int x1 = number * (AXIS_X_SPACE + AXIS_X_SIZE) + AXIS_X_OFFSET;
	int x2 = number * (AXIS_X_SPACE + AXIS_X_SIZE) + AXIS_X_OFFSET + AXIS_X_SIZE;
	int y1 = AXIS_Y_OFFSET;
	int y2 = AXIS_Y_OFFSET + AXIS_Y_SIZE;

	// Create the string
	char buff[32];
	snprintf(buff, 32, "%i", number);

	// Create the box and fill it
	drawrect(x1,y1,x2,y2,black);
	fillrect(x1+1,y1+1,x2-1,y2-1,gray);

	// Calculate the pixel offset based on the axis readout
	int offset = 1 + (AXIS_Y_SIZE-2) / 2 + ((int)value * (AXIS_Y_SIZE)) / USHRT_MAX;

	// Draw the line for the gauge
	drawline(x1+1,y1+offset,x2-1,y1+offset,red);
	// Add the label
	drawstring(x1+3, y2-2, buff, black);
}

/**
 * For demonstration purposes we can render 2 axis simultaneously to show the axis dual axis control
 * inputs like on the Logitech controller.
 * @param number The logical number for this combination - used as axis numbers can no longer be used for the offset
 * @param n1 Axis number used for the X axis - printed
 * @param n2 Axis number used for the Y axis - printed
 * @param x Value for the X axis, from -32767 to 32768
 * @param y Value for the Y axis, from -32767 to 32768
 */
void renderaxis2(int number, int n1, int n2, short x, short y) {
	int x1 = number * (AXIS_X_SPACE + AXIS_Y_SIZE) + AXIS_X_OFFSET;
	int x2 = number * (AXIS_X_SPACE + AXIS_Y_SIZE) + AXIS_X_OFFSET + AXIS_Y_SIZE;
	int y1 = AXIS2_Y_OFFSET;
	int y2 = AXIS2_Y_OFFSET + AXIS_Y_SIZE;

	// Prepare the label
	char buff[64];
	snprintf(buff, 32, "X: %i - Y: %i", n1, n2);

	// Create the box and fill it
	drawrect(x1,y1,x2,y2,black);
	fillrect(x1+1,y1+1,x2-1,y2-1,gray);

	// Calculate the offsets based on the axis value
	int offsetx = 1 + (AXIS_Y_SIZE-2) / 2 + ((int)x * (AXIS_Y_SIZE)) / USHRT_MAX;
	int offsety = 1 + (AXIS_Y_SIZE-2) / 2 + ((int)y * (AXIS_Y_SIZE)) / USHRT_MAX;

	// Draw the cross
	drawline(x1+1,y1+offsety,x2-1,y1+offsety,red);
	drawline(x1+offsetx,y1+1,x1+offsetx,y2-1,red);
	// Add the label
	drawstring(x1+3, y2-2, buff, black);
}
