#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <helix.h>

#define TEST_LOOPS 10

#define TEST_START(name,a...)		\
	printf("Testing " name "...\n",##a);\
	for(int f=0;f<TEST_LOOPS;f++){	\
		timestamp_t t;				\
		timestamp_start(&t);

#define TEST_END(frames)			\
		printf("FPS: %.3f\n",1000.0/(double)timestamp_diff_ms(&t)*frames ## .0);	\
	}

void test_reset(){
	fillrect(0,0,DVI_WIDTH,DVI_HEIGHT,black);
	render_flip_buffer();
	fillrect(0,0,DVI_WIDTH,DVI_HEIGHT,black);
}

void test_clip(){

	render_init(1,4,3);
	test_reset();

#define CENTER_X 150
#define CENTER_Y 150
#define LENGTH 100
#define STEPS 20
	for(int i=0;i<2*LENGTH;i+=LENGTH/STEPS){
		drawline(CENTER_X,CENTER_Y,CENTER_X+LENGTH,CENTER_Y+LENGTH-i,blue,0,0);
		drawline(CENTER_X,CENTER_Y,CENTER_X+LENGTH-i,CENTER_Y-LENGTH,blue,0,0);
		drawline(CENTER_X,CENTER_Y,CENTER_X-LENGTH,CENTER_Y-LENGTH+i,blue,0,0);
		drawline(CENTER_X,CENTER_Y,CENTER_X-LENGTH+i,CENTER_Y+LENGTH,blue,0,0);
		drawline(CENTER_X,CENTER_Y,CENTER_X+LENGTH,CENTER_Y+LENGTH-i,green,1,0);
		drawline(CENTER_X,CENTER_Y,CENTER_X+LENGTH-i,CENTER_Y-LENGTH,green,1,0);
		drawline(CENTER_X,CENTER_Y,CENTER_X-LENGTH,CENTER_Y-LENGTH+i,green,1,0);
		drawline(CENTER_X,CENTER_Y,CENTER_X-LENGTH+i,CENTER_Y+LENGTH,green,1,0);
		drawline(CENTER_X,CENTER_Y,CENTER_X+LENGTH,CENTER_Y+LENGTH-i,red,0,1);
		drawline(CENTER_X,CENTER_Y,CENTER_X+LENGTH-i,CENTER_Y-LENGTH,red,0,1);
		drawline(CENTER_X,CENTER_Y,CENTER_X-LENGTH,CENTER_Y-LENGTH+i,red,0,1);
		drawline(CENTER_X,CENTER_Y,CENTER_X-LENGTH+i,CENTER_Y+LENGTH,red,0,1);
	}
	drawrect(CENTER_X-LENGTH-2,CENTER_Y-LENGTH-2,CENTER_X+LENGTH+2,CENTER_Y+LENGTH+2,blue,0,0);

	render_flip_buffer();
	sleep(1);
	render_destroy();
}

#define SLAVES 4

se_t<SLAVES-1> render_slaves ATTR_DATA_ALIGNED;
se_t<1> render_master ATTR_DATA_ALIGNED;

void test_fade_multi_slave(int nr){
	for(int i=0;i<256;i++){
		color_t c(i,0,0);
		fillrect(0,0,640,480,c,255,0,nr);
		render_flip_buffer(0,nr,render_master,render_slaves);
	}
}

void test_fade_multi_slave_wrapper(int nr){
	for(int l=0;l<TEST_LOOPS;l++)
		test_fade_multi_slave(nr);
}

void test_fade_multi(){
	render_init(1,1,SLAVES);
	test_reset();

	se_create_pt(render_master,pthread_self());
	pthread_t p[SLAVES-1];

	for(int s=1;s<SLAVES;s++)
		par(test_fade_multi_slave_wrapper,s,&p[s-1]);

	se_create_pta(render_slaves,p);

	TEST_START("fade multi")
	test_fade_multi_slave(0);
	TEST_END(256)

	render_destroy();
}

void test_fade(){
	TEST_START("fade")
	for(int i=0;i<256;i++){
		color_t c(i,0,0);
		fillrect(0,0,640,480,c);
		render_flip_buffer();
	}
	TEST_END(256)
}
	
void test_alpha(){
	TEST_START("alpha")

	for(int b=0;b<2;b++){
		fillrect(0,0,160,480,red);
		fillrect(160,0,320,480,green);
		fillrect(320,0,480,480,blue);
		fillrect(480,0,640,480,yellow);
		if(b==1)fillrect(80,120,560,360,white,16);
		render_flip_buffer();
	}

	for(int i=0;i<64;i++){
		fillrect(80,120,560,360,white,32);
		render_flip_buffer();
	}
		
	TEST_END(64)
}

void test_poly(){
	TEST_START("open polygons")

	for(int i=0;i<256;i++){
		coordinate_t p[3]={
			{(coord_t)(rand()%DVI_WIDTH),(coord_t)(rand()%DVI_HEIGHT)},
			{(coord_t)(rand()%DVI_WIDTH),(coord_t)(rand()%DVI_HEIGHT)},
			{(coord_t)(rand()%DVI_WIDTH),(coord_t)(rand()%DVI_HEIGHT)}};
		poly_t<3> t(p);

		color_t c(rand());
		drawpolygon(t,c,0,0);
		render_flip_buffer();
		drawpolygon(t,c,0,0);
	}
	
	TEST_END(256)
}

void test_poly2(unsigned char alpha){
	TEST_START("filled polygons with alpha %hhu",alpha)

	for(int i=0;i<256;i++){
		coordinate_t p[3]={
			{(coord_t)(rand()%DVI_WIDTH),(coord_t)(rand()%DVI_HEIGHT)},
			{(coord_t)(rand()%DVI_WIDTH),(coord_t)(rand()%DVI_HEIGHT)},
			{(coord_t)(rand()%DVI_WIDTH),(coord_t)(rand()%DVI_HEIGHT)}};
		poly_t<3> t(p);

		color_t c(rand());
		fillpoly(t,c,alpha);
		render_flip_buffer();
		fillpoly(t,c,alpha);
	}
		
	TEST_END(256)
}

void test_text(){
	printf("Testing text output...\n");
	drawstring(100,100,"Hello World!!1",red);
	render_flip_buffer();
}


int main(int argc,char** argv){

	printf("Testing dvi...\n");

	render_init(1);
	test_reset();
	test_fade();
	test_alpha();
	test_poly();
	test_poly2(255);
	test_poly2(64);
	test_text();
	render_destroy();
	
	test_fade_multi();
	test_clip();

	printf("All done.\n");
	return 0;
}

