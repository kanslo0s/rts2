#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <cstdio>
using namespace std;

int main(int argc,char** argv){
	printf("UART tests\n");

	char s[128];
	printf("Enter two chars > ");
	fflush(stdout);
	printf("\ngetchar() 1: %c\n",getchar());
	printf("getchar() 2: %c\n",getchar());

	printf("Enter string > ");
	printf("gets(): %s\n",fgets(s,sizeof(s),stdin));
	fflush(stdout);
	
	printf("Enter number > ");
	fflush(stdout);
	int i;
	scanf("%d",&i);
	printf("scanf(): %d\n\n",i);

	cout << "Enter number > " << flush;
	cin >> i;
	cout << "cin/2 >> " << i/2 << endl;

	cout << endl << "done" << endl;
	return 0;
}

