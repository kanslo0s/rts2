#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>

static const size_t bulk_sizes[]={35,12,3,1};

static void err(const char* msg,int e){
	printf("ERROR: %s: %d\n",msg,e);
	abort();
}

static void* prod(void* arg){
	usleep(100000);
	int fd=(long)arg;

	const char msg[]={
		"Q:      How many existentialists does it take to screw in a light bulb?\n"
		"A:      Two.  One to screw it in and one to observe how the light bulb\n"
		"        itself symbolizes a single incandescent beacon of subjective\n"
		"        reality in a netherworld of endless absurdity reaching out toward a\n"
		"        maudlin cosmos of nothingness.\n"};

	int w=sizeof(msg);
	int res;
//	fcntl(fd,F_SETFL,fcntl(fd,F_GETFL)|O_NONBLOCK);
retry:
	while(w>0&&(res=write(fd,&msg[sizeof(msg)-w],w))>=0)w-=res;
	if(res<0){
		if(errno==EAGAIN)
			goto retry;
		else
			err("write",errno);
	}
	close(fd);
	usleep(100000);
	return NULL;
}

static void cons(int fd,size_t bulk){
	usleep(100000);
	char* buf=(char*)malloc(bulk);
	if(buf==NULL)
		err("buf",ENOMEM);
	int r;
retry:
	while((r=read(fd,buf,bulk))>0){
		fwrite(buf,r,1,stdout);
		fflush(stdout);
		usleep(10000);
	}
	if(r==-1){
		if(errno==EAGAIN){
			usleep(10000);
			goto retry;
		}else
			err("read",errno);
	}
	close(fd);
	free(buf);
	usleep(100000);
}

static int cons_blocking(int fd,size_t bulk){
	cons(fd,bulk);
	return 0;
}

static int cons_nonblocking(int fd,size_t bulk){
	int flags=fcntl(fd,F_GETFL);
	if(flags==-1)
		err("fcntl",errno);
	if(fcntl(fd,F_SETFL,flags|O_NONBLOCK)==-1)
		err("fcntl",errno);

	cons(fd,bulk);
	return 0;
}

static void run_test(int(*c)(int,size_t),size_t bulk){
	int pipefd[2];
	if(pipe(pipefd)==-1)
		err("pipe",errno);
	printf("\n-> pipe rd=%d wr=%d bulk=%d\n\n",pipefd[0],pipefd[1],(int)bulk);

	pthread_t p;
	int res;
	if(	(res=pthread_create(&p,NULL,prod,(void*)(long)pipefd[1]))||
		(res=c(pipefd[0],bulk))||
		(res=pthread_join(p,NULL)))
		err("pthread",res);
}

int main(int argc,char** argv){
	printf("Pipe test\n");

	int i;
	for(i=0;i<sizeof(bulk_sizes)/sizeof(size_t);i++){
		run_test(cons_blocking,bulk_sizes[i]);
		run_test(cons_nonblocking,bulk_sizes[i]);
	}

	printf("\nall done\n");
	return 0;
}

