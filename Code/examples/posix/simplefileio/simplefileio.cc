#include <stdio.h>
#include <errno.h>

static const char motd[]={
	"Q:      How many lawyers does it take to change a light bulb?\n"
	"A:      You won't find a lawyer who can change a light bulb.  Now, if\n"
	"        you're looking for a lawyer to screw a light bulb...\n"};

int main(int argc,char** argv){
	printf("Simple file IO\n");
	printf(">> dump:\n");

	FILE* f=fopen("input.txt","r");
	if(!f){
		printf("open error %d\n",errno);
		return 1;
	}

	char buf[10];
	while(!feof(f)&&!ferror(f)){
		int read=fread(buf,1,sizeof(buf),f);
		fwrite(buf,1,read,stdout);
	}
	if(ferror(f))
		printf("read error %d\n",errno);

	fflush(stdout);
	fclose(f);

	printf("<< dump\n");
	if(!(f=fopen("output.txt","w"))){
		printf("open error %d\n",errno);
		return 2;
	}
	if(fwrite(motd,1,sizeof(motd)-1,f)!=sizeof(motd)-1)
		printf("write error %d\n",errno);
	fclose(f);
	
	printf(">> done\n");

	return 0;
}

