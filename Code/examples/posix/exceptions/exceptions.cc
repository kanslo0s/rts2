#include <stdio.h>

void foo(){
	printf("throwing...\n");
	throw 42;
}

int main(){
	try {
		foo();
	}catch(int e){
		printf("got %d\n",e);
	}
}

