#include <stdio.h>
#include <stddef.h>

int main(int argc,char** argv){
	printf("size of char:        %lu\n",(long)sizeof(char));
	printf("size of short:       %lu\n",(long)sizeof(short));
	printf("size of int:         %lu\n",(long)sizeof(int));
	printf("size of long:        %lu\n",(long)sizeof(long));
	printf("size of long long:   %lu\n",(long)sizeof(long long));
	printf("size of float:       %lu\n",(long)sizeof(float));
	printf("size of double:      %lu\n",(long)sizeof(double));
	printf("size of long double: %lu\n",(long)sizeof(long double));
	printf("size of void*:       %lu\n",(long)sizeof(void*));
	printf("size of size_t:      %lu\n",(long)sizeof(size_t));

	char c=0xff;
	if(c>1)
		printf("char is unsigned\n");
	else
		printf("char is signed\n");
	
	union { char c[4]; int i; } u;
	u.i=1;
	if(u.c[3]==1)
		printf("your processor is big endian\n");
	else
		printf("your processor is little endian\n");
	
	return 0;
}

