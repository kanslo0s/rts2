#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <linfs.h>

static void test_file_endianness(char* filename){
	char buf[32];
	int fd=open(filename,O_RDONLY);
	if(fd==-1){
		printf("open error: (%d) %s\n",errno,strerror(errno));
		return;
	}

	unsigned int r=0,res;
	char* b=buf;
	do{
		res=read(fd,b,sizeof(buf)-r);
		if(res>0){
			b+=res;
			r+=res;
		}
	}while(res>0&&r<sizeof(buf));

	if(res<0){
		printf("read error: (%d) %s\n",errno,strerror(errno));
	}else if(r==0)
		printf("file is empty\n");
	else{
		printf("first %2u bytes:  ",r);
		for(unsigned int i=0;i<r;i++)
			printf("%02hhx ",buf[i]);
		printf("\n");
		
		printf("first %2lu hwords: ",r/sizeof(short));
		for(unsigned int i=0;i<r/sizeof(short);i++)
			printf("%04hx  ",((short*)buf)[i]);
		printf("\n");
		
		printf("first %2lu words:  ",r/sizeof(int));
		for(unsigned int i=0;i<r/sizeof(int);i++)
			printf("%08x    ",((int*)buf)[i]);
		printf("\n\n");
	}

	close(fd);
}

static void test_read_speed(char* filename,int bufsize){
	int res,size=0,ms;
	struct timeval tv_start,tv_end;

	char* buf=(char*)malloc(bufsize);
	if(!buf){
		printf("out of memory\n");
		return;
	}
	int fd=open(filename,O_RDONLY);
	if(fd==-1){
		printf("open error: (%d) %s\n",errno,strerror(errno));
		goto abort;
	}

	gettimeofday(&tv_start,NULL);
	while((res=read(fd,buf,bufsize))>0)
		size+=res;
	gettimeofday(&tv_end,NULL);
	if(res<0){
		printf("read error: (%d) %s\n",errno,strerror(errno));
	}else{
		ms=tv_end.tv_sec*1000+tv_end.tv_usec/1000-tv_start.tv_sec*1000-tv_start.tv_usec/1000;
		printf("chunk=0x%x, read %d bytes in %.3f s, %.3f MB/s\n",bufsize,size,(double)ms/1000.0,(double)size/1024.0/1024.0/((double)ms/1000.0));
	}

	close(fd);
abort:
	free(buf);
}

#define WRITE_SIZE	(1<<22)
static void test_write_speed(char* filename,int bufsize){
	int res,size=WRITE_SIZE,ms;
	struct timeval tv_start,tv_end;

	char* buf=(char*)malloc(bufsize);
	if(!buf){
		printf("out of memory\n");
		return;
	}
	int fd=creat(filename,0666);
	if(fd==-1){
		printf("open error: (%d) %s\n",errno,strerror(errno));
		goto abort;
	}

	gettimeofday(&tv_start,NULL);
	while((res=write(fd,buf,bufsize))==bufsize&&size>0)size-=bufsize;
	gettimeofday(&tv_end,NULL);
	if(res!=bufsize){
		printf("write error: (%d) %s (ret=%d)\n",errno,strerror(errno),res);
	}else{
		ms=tv_end.tv_sec*1000+tv_end.tv_usec/1000-tv_start.tv_sec*1000-tv_start.tv_usec/1000;
		printf("chunk=0x%x, write %d bytes in %.3f s, %.3f MB/s\n",bufsize,WRITE_SIZE,(double)ms/1000.0,(double)WRITE_SIZE/1024.0/1024.0/((double)ms/1000.0));
	}

	close(fd);
abort:
	free(buf);
}

static void test_copy_speed(char* file1,char* file2,int bufsize){
	char *ping,*pong;
	struct timeval tv_start,tv_end;
	int ms,r,w,fd_src,fd_dest,size=0;

	if(!(ping=(char*)malloc(bufsize))){
		printf("out of memory for ping\n");
		goto abort0;
	}
	if(!(pong=(char*)malloc(bufsize))){
		printf("out of memory for pong\n");
		goto abort1;
	}
	if((fd_src=open(file1,O_RDONLY))==-1){
		printf("cannot open src %s: (%d) %s\n",file1,errno,strerror(errno));
		goto abort2;
	}
	if((fd_dest=open(file2,O_CREAT|O_TRUNC|O_WRONLY|O_ASYNC))==-1){
		printf("cannot open dest %s: (%d) %s\n",file2,errno,strerror(errno));
		goto abort3;
	}

	gettimeofday(&tv_start,NULL);
	while(true){
		if((r=read(fd_src,ping,bufsize))<0)
			goto read_error;
		if(r==0)
			break;
		size+=r;
		if((w=write(fd_dest,ping,r))<0)
			goto write_error;
		if((r=read(fd_src,pong,bufsize))<0)
			goto read_error;
		if(r==0)
			break;
		size+=r;
		if((w=write(fd_dest,pong,r))<0)
			goto write_error;
	}
	gettimeofday(&tv_end,NULL);
	ms=tv_end.tv_sec*1000+tv_end.tv_usec/1000-tv_start.tv_sec*1000-tv_start.tv_usec/1000;
	printf("chunk=0x%x, copy %d bytes in %.3f s, %.3f MB/s\n",bufsize,size,(double)ms/1000.0,(double)size/1024.0/1024.0/((double)ms/1000.0));

abort4:
	close(fd_dest);
abort3:
	close(fd_src);
abort2:
	free(pong);
abort1:
	free(ping);
abort0:
	return;

read_error:
	printf("read error: (%d) %s\n",errno,strerror(errno));
	goto abort4;
write_error:
	printf("write error (%d) %s\n",errno,strerror(errno));
	goto abort4;
}

static void read_filename(const char* msg,char* file,size_t maxlength){
	while(true){
		printf(msg);
		fflush(stdout);
		int len;
		if(!fgets(file,maxlength,stdin))
			continue;
		if((len=strlen(file))<1)
			continue;
		file[len-1]='\0';//strip newline
		return;
	}
}

int main(int argc,char** argv){
	sleep(1);
	printf("-> Testing linfs\n");

	char file1[128],file2[128];
	while(true){
		read_filename("Enter filename to read: ",file1,sizeof(file1));
		if(file1[0]){
			test_file_endianness(file1);
			for(int b=(1<<14);b<=(1<<20);b<<=1)
				test_read_speed(file1,b);
		}
		
		read_filename("Enter filename to write: ",file1,sizeof(file1));
		if(file1[0])
			for(int b=(1<<14);b<=(1<<20);b<<=1)
				test_write_speed(file1,b);

		read_filename("Enter source filename to copy: ",file1,sizeof(file1));
		read_filename("Enter dest filename to copy: ",file2,sizeof(file2));
		if(file1[0]&&file2[0])
			test_copy_speed(file1,file2,(1<<19));
	}
/*	
	printf("cat /home/tmp/test.txt:\n");
	int fd2=creat("/home/tmp/output",0666);
	int fd=open("/dev/input/event0",O_RDONLY);
	if(fd==-1){
		printf("open error %d\n",errno);
		exit(1);
	}
	printf("fd=%d\n",fd);
	char buf[16];
	int r=0;
	int it=20;
	while((r=read(fd,buf,sizeof(buf)))>0&&--it>0){
		fwrite(buf,r,1,stdout);
		write(fd2,buf,r);
	}
	fflush(stdout);
	if(r<0)
		printf("read error %d/%d\n",r,errno);
	if(close(fd)!=0)
		printf("close error %d\n",errno);
	printf("-> done\n");
	close(fd2);
*/
	return 0;
}

