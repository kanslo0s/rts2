#include <helix.h>

// application implementation

void* ProdProcess(void* arg){
	WFifo<int> f((Fifo*)OpenChannel(0));
	unsigned int total=0;

	for(int i=0;i<1000;i++){
		printf("producing %d...\n",i);
		total+=i;
		f.push(i);
	}

	printf("waiting for consumer...\n");
	unsigned int r;
	r=MailRead((SimpleMail*)OpenChannel(1));
	if(r==total)
		printf("consumer has received everything, really, I checked: %d\n",r);
	else
		printf("Uh oh, consumer has received %d items, instead of %d!\n",r,total);

	printf("reset vector: %p %p\n",*(void**)0,*(void**)4);

	return (void*)1;
}

void* ConsProcess(void* arg){
	printf("cons channel 0: %p\n",OpenChannel(0));
	RFifo<int> f((Fifo*)OpenChannel(0));
	unsigned int total=0;

	for(int i=0;i<1000;i++){
		int v;
		v=*f.front();
		f.pop();
		total+=v;
		printf("consuming %d...\n",v);
	}

	printf("Sending 'I Love You' mail to producer...\n");
	MailWrite((SimpleMail*)OpenChannel(1),total);
	printf("consumer done\n");

	return (void*)total;
}

// task graph

map_core_t mb0={/*.type=*/CORE_MICROBLAZE,{/*.coreid=*/(void*)0},/*.data=*/NULL};
map_core_t mb1={/*.type=*/CORE_MICROBLAZE,{/*.coreid=*/(void*)1},/*.data=*/NULL};
map_core_t heap={/*.type=*/CORE_SHARED_HEAP,{/*.manager=*/&mb0},/*.data=*/NULL};
map_core_t fifomem0={/*.type=*/CORE_COMM_MEM,{/*.manager=*/&mb0},/*.data=*/NULL};
map_core_t fifomem1={/*.type=*/CORE_COMM_MEM,{/*.manager=*/&mb1},/*.data=*/NULL};
map_core_t* cores[]={&mb0,&mb1,&heap,&fifomem0,&fifomem1,NULL};
map_hardware_t hardware={/*.core=*/cores};

extern map_task_t prod_task;
extern map_task_t cons_task;
extern map_task_t fifo_task;
extern map_task_t mail_task;
extern map_port_t fifo_port_in;
extern map_port_t fifo_port_out;
extern map_port_t mail_port_in;
extern map_port_t mail_port_out;

map_implementation_t directlink_impl={/*.type=*/IMPL_DEDICATED,{/*.link=*/NULL}};
map_implementation_t* directlink_impls[]={&directlink_impl,NULL};

map_port_t prod_port_fifo={/*.role=*/ROLE_PRODUCER,/*.implementation=*/directlink_impls,/*.task=*/&prod_task,/*.port=*/&fifo_port_in};
map_port_t prod_port_mail={/*.role=*/ROLE_CONSUMER,/*.implementation=*/directlink_impls,/*.task=*/&prod_task,/*.port=*/&mail_port_out};
map_port_t* prod_ports[]={&prod_port_fifo,&prod_port_mail,NULL};
map_impl_process_t prod_proc={/*.main=*/(void(*)(void*))&ProdProcess,/*.arg=*/NULL,/*.stack=*/PROC_DEFAULT_STACK,/*.timeslice=*/PROC_DEFAULT_TIMESLICE};
map_implementation_t prod_impl={/*.type=*/IMPL_PROCESS,{/*.process=*/&prod_proc}};
map_implementation_t* prod_impls[]={&prod_impl,NULL};
map_task_t prod_task={/*.implementation=*/prod_impls,/*.port=*/prod_ports};
map_impl_layout_t prod_port_layout={/*.binding=*/&directlink_impl,/*.mapping=*/&mb0,{/*.address=*/NULL}};
map_impl_layout_t* prod_ports_layout[]={&prod_port_layout,&prod_port_layout,NULL};
map_task_layout_t prod_layout={/*.task=*/&prod_task,/*.task_layout=*/{/*.binding=*/&prod_impl,/*.mapping=*/&mb0,{/*.address=*/NULL}},/*.port_layout=*/prod_ports_layout};

map_port_t cons_port_fifo={/*.role=*/ROLE_CONSUMER,/*.implementation=*/directlink_impls,/*.task=*/&cons_task,/*.port=*/&fifo_port_out};
map_port_t cons_port_mail={/*.role=*/ROLE_PRODUCER,/*.implementation=*/directlink_impls,/*.task=*/&cons_task,/*.port=*/&mail_port_in};
map_port_t* cons_ports[]={&cons_port_fifo,&cons_port_mail,NULL};
map_impl_process_t cons_proc={/*.main=*/(void(*)(void*))&ConsProcess,/*.arg=*/NULL,/*.stack=*/PROC_DEFAULT_STACK,/*.timeslice=*/PROC_DEFAULT_TIMESLICE};
map_implementation_t cons_impl={/*.type=*/IMPL_PROCESS,{/*.process=*/&cons_proc}};
map_implementation_t* cons_impls[]={&cons_impl,NULL};
map_task_t cons_task={/*.implementation=*/cons_impls,/*.port=*/cons_ports};
map_impl_layout_t cons_port_layout={/*.binding=*/&directlink_impl,/*.mapping=*/&mb1,{/*.address=*/NULL}};
map_impl_layout_t* cons_ports_layout[]={&cons_port_layout,&cons_port_layout,NULL};
map_task_layout_t cons_layout={/*.task=*/&cons_task,/*.task_layout=*/{/*.binding=*/&cons_impl,/*.mapping=*/&mb1,{/*.address=*/NULL}},/*.port_layout=*/cons_ports_layout};

map_port_t fifo_port_in={/*.role=*/ROLE_CONSUMER,/*.implementation=*/directlink_impls,/*.task=*/&fifo_task,/*.port=*/&prod_port_fifo};
map_port_t fifo_port_out={/*.role=*/ROLE_PRODUCER,/*.implementation=*/directlink_impls,/*.task=*/&fifo_task,/*.port=*/&cons_port_fifo};
map_port_t* fifo_ports[]={&fifo_port_in,&fifo_port_out,NULL};
map_impl_fifo_t fifo_info={/*.element_size=*/sizeof(int),/*.fifo_size=*/16};
map_implementation_t fifo_impl={/*.type=*/IMPL_FIFO,{/*.fifo=*/&fifo_info}};
map_implementation_t* fifo_impls[]={&fifo_impl,NULL};
map_task_t fifo_task={/*.implementation=*/fifo_impls,/*.port=*/fifo_ports};
map_impl_layout_t fifo_port_layout={/*.binding=*/&directlink_impl,/*.mapping=*/&fifomem1,{/*.address=*/NULL}};
map_impl_layout_t* fifo_ports_layout[]={&fifo_port_layout,&fifo_port_layout,NULL};
map_task_layout_t fifo_layout={/*.task=*/&fifo_task,/*.task_layout=*/{/*.binding=*/&fifo_impl,/*.mapping=*/&fifomem1,{/*.address=*/NULL}},/*.port_layout=*/fifo_ports_layout};

map_port_t mail_port_in={/*.role=*/ROLE_CONSUMER,/*.implementation=*/directlink_impls,/*.task=*/&mail_task,/*.port=*/&cons_port_mail};
map_port_t mail_port_out={/*.role=*/ROLE_PRODUCER,/*.implementation=*/directlink_impls,/*.task=*/&mail_task,/*.port=*/&prod_port_mail};
map_port_t* mail_ports[]={&mail_port_in,&mail_port_out,NULL};
map_impl_mailbox_t mail_info={/*.size=*/0};
map_implementation_t mail_impl={/*.type=*/IMPL_MAILBOX,{/*.mailbox=*/&mail_info}};
map_implementation_t* mail_impls[]={&mail_impl,NULL};
map_task_t mail_task={/*.implementation=*/mail_impls,/*.port=*/mail_ports};
map_impl_layout_t mail_port_layout={/*.binding=*/&directlink_impl,/*.mapping=*/&fifomem0,{/*.address=*/NULL}};
map_impl_layout_t* mail_ports_layout[]={&mail_port_layout,&mail_port_layout,NULL};
map_task_layout_t mail_layout={/*.task=*/&mail_task,/*.task_layout=*/{/*.binding=*/&mail_impl,/*.mapping=*/&fifomem0,{/*.address=*/NULL}},/*.port_layout=*/mail_ports_layout};

map_task_t* tasks[]={&prod_task,&cons_task,&fifo_task,&mail_task,NULL};
map_application_t app={/*.task=*/tasks};
map_task_layout_t* tasks_layout[]={&prod_layout,&cons_layout,&fifo_layout,&mail_layout,NULL};
map_app_layout_t layout={/*.task=*/tasks_layout};

int main(int argc,char** argv){

	printf("Buggy!!\n");

	printf(" >> dump\n");
	map_dump(&hardware,&app,&layout);
	printf(" >> init\n");
	printf("res: %d\n",map_init(&hardware));
	printf(" >> start\n");
	printf("res: %d\n",map_start(&hardware,&app,&layout));
	sleep(1);
	printf(" >> stop\n");
	printf("res: %d\n",map_stop(&hardware,&app,&layout));
	printf(" >> dump\n");
	map_dump(&hardware,&app,&layout);
	printf(" >> done\n");
	return 0; 
}

