#include <helix.h>
#include <unistd.h>
#include <stdio.h>

#define ITERATIONS 100

se_t<1> s ATTR_DATA_ALIGNED;

void* prod(void* arg){
	printf("producing...\n");
	se_hit(s);

	for(int i=0;i<ITERATIONS;i++){
		se_hit(s);
	}
	printf("p done\n");
	return NULL;
}

void* cons(void* arg){
	se_wait(s);
	printf("consuming...\n");

	for(int i=0;i<ITERATIONS;i++){
		se_wait(s);
		printf("c: %d\n",i);
	}
	printf("c done\n");
	return NULL;
}

int main(int argc,char** argv){
	pthread_t p,c;
	VERIFY(pthread_create(&p,NULL,prod,NULL),0);
	VERIFY(pthread_create(&c,NULL,cons,NULL),0);
	printf("Setting se...\n");
	se_create_pt(s,c);
	return 0;
}

