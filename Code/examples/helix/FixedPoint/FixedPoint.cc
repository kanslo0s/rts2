//#define FIXED_NATIVE_CONVERT
#include <helix.h>

#define ITERATIONS	1000000
typedef fixed16_t fixedtest_t;
#define RAND_SEED	1

#define FRAMEWORK_START(...)		\
	srand(RAND_SEED);				\
	stopwatch_t _sw;				\
	sw_start(_sw);					\
	for(int _i=0;_i<ITERATIONS;_i++){
#define FRAMEWORK_END(...)			\
	}								\
	sw_stop(_sw);					\
	return sw_elapsed_sec(_sw);

static float randf(){
	int a=rand(),b=rand(),c=rand();
	float f=((float)a-(float)b)/(float)c;
	return f*f*f;
}

int test_conversion(){
	srand(RAND_SEED);

	int dubious=0;
	for(int i=0;i<ITERATIONS;i++){
		float f=randf();
		fixedtest_t fi=f;
		float fi_f=(float)fi;

		double diff=abs(fi_f-f);
		if(diff>0.01&&!fi.out_of_range()){
			dubious++;
			printf("f=%20.6f raw=0x%016llx fi=%15.6f diff=%14e %s\n",(double)f,(unsigned long long int)fi.raw(),(double)fi_f,diff,diff>0.01?"  DIFF!":"");
		}
	}
	return dubious;
}

double test_math(){
	srand(RAND_SEED);
	srand48(RAND_SEED);
	
	float f=1.0f;
	fixedtest_t fi=1.0f;

	for(int i=0;i<ITERATIONS;i++){
		float op=drand48();//*100.0;
		switch(rand()%4){
		case 0: f-=op; fi-=op; break;
		case 1: f*=op; fi*=op; break;
		default:
				f+=op; fi+=op; 
		}
//		printf("%f -> %15e vs %15e (diff %15e)\n",(double)op,(double)f,(double)fi,(double)f-(double)fi);
	}

	return (double)f-(double)fi;
}

double test_rand(){
	FRAMEWORK_START()
	float f __attribute__((unused))=randf();
	FRAMEWORK_END()
}

double test_to_fixed(){
	FRAMEWORK_START()
	fixedtest_t fi=randf();
	volatile int i __attribute__((unused))=(int)fi.raw();
	FRAMEWORK_END()
}

double test_to_fixed_and_back(){
	FRAMEWORK_START()
	fixedtest_t fi=randf();
	volatile int i __attribute__((unused))=(int)fi.raw();
	barrier();
	volatile float f __attribute__((unused))=(float)fi;
	FRAMEWORK_END()
}

int main(int argc,char** argv){
#ifdef FIXED_NATIVE_CONVERT
	printf("Using native/builtin float conversion\n");
#else
	printf("Using optimized software float conversion\n");
#endif
	printf("%-20s: %10d\n","iterations",ITERATIONS);
	printf("%-20s: %10d dubious\n","conversion",test_conversion());
	printf("%-20s: %10.3e diff\n","math",test_math());
	printf("%-20s: %10.3f s\n","rand() input",test_rand());
	printf("%-20s: %10.3f s\n","rand() to fixed",test_to_fixed());
	printf("%-20s: %10.3f s\n","...and back",test_to_fixed_and_back());
	return 0;
}

