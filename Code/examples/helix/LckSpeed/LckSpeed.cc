#include <helix.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <signal.h>

#define MUTEXES	1024
#define REPEAT 128

#define TIME_DIFF(start,end,cnt)	(((float)(end.tv_sec*1000000+end.tv_usec-start.tv_sec*1000000-start.tv_usec))/((float)cnt))

typedef struct {
	float lock[REPEAT];
	float unlock[REPEAT];
	float relock[REPEAT];
	float reunlock[REPEAT];
	float avg_lock;
	float avg_unlock;
	float avg_relock;
	float avg_reunlock;
} results_t;

#define _CALC_AVG(r,field)	do{float avg=0.0f;for(int i=0;i<REPEAT;i++)avg+=r->field[i];r->avg_##field=avg/((float)REPEAT);}while(0)
#define CALC_AVG			do{_CALC_AVG(r,lock);_CALC_AVG(r,unlock);_CALC_AVG(r,relock);_CALC_AVG(r,reunlock);}while(0)
#define AVG(field)			(r->avg_##field)

#define MEASURE_INIT				struct timeval start,end;results_t *r=(results_t*)malloc(sizeof(results_t));if(r==NULL){printf("Out of memory\n");abort();}
#define MEASURE_START				do{Yield();gettimeofday(&start,NULL);}while(0)
#define MEASURE_END(field,i)		do{gettimeofday(&end,NULL);r->field[i]=TIME_DIFF(start,end,MUTEXES);}while(0)
#define MEASURE_CLEANUP				free(r)

#ifdef USE_DISTRIBUTED_LOCK
#ifndef RESERVE_DAEMON_CORE
#warning Shouldn't you define a daemon core?
#endif
#define NO_CORE -1
#define FIRST_OWN_CORE (RESERVE_DAEMON_CORE+1)
#define DO_LOCK_CORE (RESERVE_DAEMON_CORE+2)
#define CORES_IN_USE (DO_LOCK_CORE+1)

void* test_prelock(void* arg){
	int ret;
	pthread_mutex_t* m=(pthread_mutex_t*)arg;
	for(int it=0;it<REPEAT;it++){
		for(int i=0;i<MUTEXES;i++)
			if((ret=pthread_mutex_lock(&m[i])))
				printf("Prelock(%p)@%d failed: %d\n",&m[i],GetProcID(),ret);
		for(int i=0;i<MUTEXES;i++)
			if((ret=pthread_mutex_unlock(&m[i])))
				printf("Preunlock(%p)@%d failed: %d\n",&m[i],GetProcID(),ret);

		printf("Ready for test %d\n",it);
		pause();
	}
	printf("Exiting prelock\n");
	return NULL;
}

static pid_t firstownpid;
static int firstowncore;

void* test_dolock(void* arg){
	int ret;

	MEASURE_INIT;

	// make sure that the lock administration has enough elements, so no malloc is required
	pthread_mutex_t* t=(pthread_mutex_t*)malloc(sizeof(pthread_mutex_t)*MUTEXES);
	if(t==NULL){
		printf("Out of memory\n");
		return NULL;
	}
	for(int i=0;i<MUTEXES;i++)
		if((ret=pthread_mutex_init(&t[i],NULL)))
			printf("Init(%p)@%d failed: %d\n",&t[i],GetProcID(),ret);
	for(int i=0;i<MUTEXES;i++)
		if((ret=pthread_mutex_lock(&t[i])))
			printf("Lock(%p)@%d failed: %d\n",&t[i],GetProcID(),ret);
	for(int i=0;i<MUTEXES;i++)
		if((ret=pthread_mutex_unlock(&t[i])))
			printf("Unlock(%p)@%d failed: %d\n",&t[i],GetProcID(),ret);
	for(int i=0;i<MUTEXES;i++)
		if((ret=pthread_mutex_destroy(&t[i])))
			printf("Destroy(%p)@%d failed: %d\n",&t[i],GetProcID(),ret);
	free(t);

	// now do the test
	pthread_mutex_t* m=(pthread_mutex_t*)arg;
	for(int it=0;it<REPEAT;it++){
		MEASURE_START;
		for(int i=0;i<MUTEXES;i++)
			if((ret=pthread_mutex_lock(&m[i])))
				printf("Lock(%p)@%d failed: %d\n",&m[i],GetProcID(),ret);
		MEASURE_END(lock,it);

		MEASURE_START;
		for(int i=0;i<MUTEXES;i++)
			if((ret=pthread_mutex_unlock(&m[i])))
				printf("Unlock(%p)@%d failed: %d\n",&m[i],GetProcID(),ret);
		MEASURE_END(unlock,it);
		
		MEASURE_START;
		for(int i=0;i<MUTEXES;i++)
			if((ret=pthread_mutex_lock(&m[i])))
				printf("Relock(%p)@%d failed: %d\n",&m[i],GetProcID(),ret);
		MEASURE_END(relock,it);

		MEASURE_START;
		for(int i=0;i<MUTEXES;i++)
			if((ret=pthread_mutex_unlock(&m[i])))
				printf("Reunlock(%p)@%d failed: %d\n",&m[i],GetProcID(),ret);
		MEASURE_END(reunlock,it);
	
		if(firstowncore!=NO_CORE){
			SendSignal(firstownpid,SIGHUP,NULL,firstowncore);
			sleep(1);
		}else
			for(int i=0;i<MUTEXES;i++)
				if((ret=pthread_mutex_destroy(&m[i])))
					printf("Destroy(%p)@%d failed: %d\n",&m[i],GetProcID(),ret);
	}
	CALC_AVG;

	printf("lock: %.3f us, ",AVG(lock));
	printf("unlock: %.3f us, ",AVG(unlock));
	printf("relock: %.3f us, ",AVG(relock));
	printf("reunlock: %.3f us\n",AVG(reunlock));

	MEASURE_CLEANUP;
	return NULL;
}

void test_lock_core(pthread_mutex_t* m,int _firstowncore,int dolockcore){
	firstownpid=(pid_t)0;
	firstowncore=_firstowncore;
	int ret=0;
	if(firstowncore!=NO_CORE){
		if((ret=CreateProcess(firstownpid,test_prelock,(void*)m,PROC_DEFAULT_TIMESLICE,PROC_DEFAULT_STACK,firstowncore)))
			printf("Create failed: %d\n",ret);
		if((ret=SetProcessFlags(firstownpid,PROC_FLAG_JOINABLE,firstowncore)))
			printf("Flags failed: %d\n",ret);
		if((ret=StartProcess(firstownpid,firstowncore)))
			printf("Start failed: %d\n",ret);
		sleep(1);
	}

	pid_t dolockpid;
	if((ret=CreateProcess(dolockpid,test_dolock,(void*)m,PROC_DEFAULT_TIMESLICE,PROC_DEFAULT_STACK,dolockcore)))
		printf("Create failed: %d\n",ret);
	if((ret=SetProcessFlags(dolockpid,PROC_FLAG_JOINABLE,dolockcore)))
		printf("Flags failed: %d\n",ret);
	printf("*** lock %d -> %d (server: %d)\n",firstowncore,dolockcore,RESERVE_DAEMON_CORE);
	if((ret=StartProcess(dolockpid,dolockcore)))
		printf("Start failed: %d\n",ret);

	sleep(1);
	WaitProcess(dolockpid,NULL,dolockcore);
	if(firstowncore!=NO_CORE){
		WaitProcess(firstownpid,NULL,firstowncore);
	}
	printf("Test ended\n");
}

static void test_lock(pthread_mutex_t* m){
	test_lock_core(m,NO_CORE,RESERVE_DAEMON_CORE);
	test_lock_core(m,NO_CORE,DO_LOCK_CORE);
	test_lock_core(m,RESERVE_DAEMON_CORE,RESERVE_DAEMON_CORE);
	test_lock_core(m,RESERVE_DAEMON_CORE,DO_LOCK_CORE);
	test_lock_core(m,FIRST_OWN_CORE,RESERVE_DAEMON_CORE);
	test_lock_core(m,FIRST_OWN_CORE,DO_LOCK_CORE);
	test_lock_core(m,DO_LOCK_CORE,DO_LOCK_CORE);
}
#else
#define CORES_IN_USE 1

static void test_lock(pthread_mutex_t* m){
	int ret;
	MEASURE_INIT;

	printf("Locking a single mutex...\n");
	Yield();
	prf_snapshot();
	if((ret=pthread_mutex_lock(&m[0]))){
		printf("Lock failed\n");
		abort();
	}
	prf_dump();
	pthread_mutex_unlock(&m[0]);

	for(int it=0;it<REPEAT;it++){
		// make sure relock does not affect first lock
		for(int i=0;i<MUTEXES;i++)
			if((ret=pthread_mutex_init(&m[i],NULL)))
				printf("Init(%p) failed: %d\n",&m[i],ret);

		MEASURE_START;
		for(int i=0;i<MUTEXES;i++)
			if((ret=pthread_mutex_lock(&m[i])))
				printf("Lock(%p) failed: %d\n",&m[i],ret);
		MEASURE_END(lock,it);
	
		MEASURE_START;
		for(int i=0;i<MUTEXES;i++)
			if((ret=pthread_mutex_unlock(&m[i])))
				printf("Unlock(%p) failed: %d\n",&m[i],ret);
		MEASURE_END(unlock,it);
	
		MEASURE_START;
		for(int i=0;i<MUTEXES;i++)
			if((ret=pthread_mutex_lock(&m[i])))
				printf("Lock(%p) failed: %d\n",&m[i],ret);
		MEASURE_END(relock,it);
		
		MEASURE_START;
		for(int i=0;i<MUTEXES;i++)
			if((ret=pthread_mutex_unlock(&m[i])))
				printf("Unlock(%p) failed: %d\n",&m[i],ret);
		MEASURE_END(reunlock,it);
	}
	CALC_AVG;

	printf("*** free lock takes %8.3f us\n",AVG(lock));
	printf("*** unlock takes    %8.3f us\n",AVG(unlock));
	printf("*** relock takes    %8.3f us\n",AVG(relock));
	printf("*** reunlock takes  %8.3f us\n",AVG(reunlock));

	MEASURE_CLEANUP;
}
#endif

#define DDR_CACHE_BASE		0x40000000
#define DDR_BLOCK_SIZE		32
#define DDR_BLOCKS			1024
#define DDR_NOCACHE_BASE	0x49000000
#define DDR_WORDS			1024
typedef struct { volatile int w[DDR_BLOCK_SIZE/sizeof(int)]; } ddr_cacheline_t;
typedef ddr_cacheline_t *ddr_cache_t;
typedef volatile int *ddr_nocache_t;

static void test_ddr(){
	// test bursts
	ddr_cache_t pc=(ddr_cache_t)DDR_CACHE_BASE;
	printf("%d bursts...\n",DDR_BLOCKS);
	Yield();
	FlushDCache();
	prf_snapshot();
	for(int i=0;i<DDR_BLOCKS;i++)
		pc[i].w[0];
	prf_dump();
	
	// test noncached
	printf("%d words...\n",DDR_WORDS);
	ddr_nocache_t pnc=(ddr_nocache_t)DDR_NOCACHE_BASE;
	Yield();
	FlushDCache();
	prf_snapshot();
	for(int i=0;i<DDR_WORDS;i++)
		pnc[i];
	prf_dump();
}

void* TaskDaemon(void* arg){
	return (void*)DefaultMulticoreSetup(CORES_IN_USE);
}

int main(int arch,char** argv){
	printf("Checking lock speed\n");
#ifdef USE_DISTRIBUTED_LOCK
	printf("- using distributed lock\n");
#else
	printf("- using bakery lock\n");
#ifdef MUTEX_FAST_RELOCK
	printf("- fast relock\n");
#endif
#endif
	printf("- testing on %d mutexes\n",MUTEXES);
#ifdef RESERVE_DAEMON_CORE
	printf("- daemon core: %d\n",RESERVE_DAEMON_CORE);
#endif

	int ret=0;
	pthread_mutex_t* m=(pthread_mutex_t*)smalloc(sizeof(pthread_mutex_t)*MUTEXES);
	if(m==NULL){
		printf("Out of memory\n");
		return 1;
	}

	for(int i=0;i<MUTEXES;i++)
		if((ret=pthread_mutex_init(&m[i],NULL)))
			printf("Init(%p) failed: %d\n",&m[i],ret);

	test_ddr();
	test_lock(m);

	for(int i=0;i<MUTEXES;i++)
		pthread_mutex_destroy(&m[i]);

	sfree(m);
	return MUTEXES;
}

