#include <helix.h>

#define MACRO_BLOCK_SIZE	16	// can be 8, 16, 32
#define MAX_SEARCH			8	// can be 8, 16 (but <=MACRO_BLOCK_SIZE)
#define FRAME_SIZE			8	// square, in macro blocks

#define FRAME_MEMORY		::PMC_NS::spm

#define COST_MAD
//#define COST_MSE

//#define ENABLE_DEBUG


#ifdef ENABLE_DEBUG
pthread_mutex_t print_lck=PTHREAD_MUTEX_INITIALIZER;
#  define DEBUG(a...) do{auto_ret<int,0>(pthread_mutex_lock(&print_lck));printf(a);auto_ret<int,0>(pthread_mutex_unlock(&print_lck));}while(0)
#  define SLAVES			1
#  define WQ_DEPTH			1
#  define PAR_FRAMES		1
#else
#  define DEBUG(...)
#  define SLAVES			(NUM_PROCESSORS/2-1)	// number of running slaves
#  define WQ_DEPTH			8						// number of work packets in slave queue
#  define PAR_FRAMES		5						// 2^PAR_FRAMES number of parallel frames
#endif

#define IMG_SIZE			(MACRO_BLOCK_SIZE*FRAME_SIZE)

//////////////////////////////////////////////////
// Search algorithm

template <int S,int P=MACRO_BLOCK_SIZE*(FRAME_SIZE+2)-S>
class PixelView {
public:
//	typedef typename SmallestUInt<(S<32?S:32)>::type chunk_t;
	typedef uint8_t chunk_t;
	typedef struct { chunk_t c[S/(sizeof(chunk_t)*8)]; } line_t_;
	typedef typename PixelView<S,0>::line_t_ line_t;
	static const int num_pixels=S;
	static const int num_padding=P;

	PixelView() : pix() {}

	template <int A> PixelView(const PixelView<S,A>& v){
		for(int y=0;y<S;y++)
			pix[y].line=v.pix[y].line;
	}

	unsigned int get(int x,int y) const {
		if(x<0||x>=S||y<0||y>=S)
			return 0;
		const size_t chunk=sizeof(chunk_t)*8;
		return (pix[y].line.c[x/chunk]>>(x%chunk))&1;
	}

	void set(int x,int y,unsigned int col){
		if(x<0||x>=S||y<0||y>=S)
			return;
		const size_t chunk=sizeof(chunk_t)*8;
		const int shift=x%chunk;
		const chunk_t mask=1<<shift;
		chunk_t& g=pix[y].line.c[x/chunk];
		g=(g&~mask)|((col<<shift)&mask);
	}

	template <unsigned int A> operator const PixelView<S-A,P+A>&() const {
		return *reinterpret_cast<PixelView<S-A,P+A>*>(this);
	}
protected:
	struct {
		line_t line;
		char padding[P/8];
	} pix[S];

	template <int A,int B> friend class PixelView;
};

typedef PixelView<MACRO_BLOCK_SIZE,0> MacroBlock;
/*
class MacroBlock : public PixelView<MACRO_BLOCK_SIZE,0> {
public:
	typedef PixelView<MACRO_BLOCK_SIZE,0> base;
};*/

template <typename R,typename T> R __attribute__((always_inline)) EntryAll(MacroBlock*& mb) { return reinterpret_cast<T>(mb)->entry_x(); }
template <typename T> void __attribute__((always_inline)) FlushAll(T& mb) { mb.flush(); }

template <int S,int OFFSET=0>
class TiledImage {
public:
	typedef ::PMC_NS::Var<MacroBlock,FRAME_MEMORY> MacroBlockV;
	typedef MacroBlockV MacroBlockX;
	typedef typename MacroBlockV::CVar MacroBlockRO;
	typedef MacroBlockV* MacroBlockP;
	typedef ::PMC_NS::VarPtrArray<S*S,MacroBlock,FRAME_MEMORY> MacroBlockArray;

	TiledImage(MacroBlockP (&mbs)[S][S]){
		for(int y=0;y<S;y++)
			for(int x=0;x<S;x++)
				mb[y*S+x]=mbs[y][x];
	}
	
	TiledImage(const MacroBlockArray& a) : mb(a) {}

	TiledImage() : mb() {}

	MacroBlockRO* getMB_ro(int mbx,int mby) const {
		return &accessMB(mbx,mby);
	}
	
	MacroBlockX* getMB_x(int mbx,int mby) {
		return &accessMB(mbx,mby);
	}

	typedef typename ::PMC_NS::VarPtrArray<S*S,MacroBlock,FRAME_MEMORY>::GroupScopeRO ScopeRO;
	typedef typename ::PMC_NS::VarPtrArray<S*S,MacroBlock,FRAME_MEMORY>::GroupScopeX ScopeX;
//	operator typename ScopeRO::NakedData(){return mb.entry_ro();}
//	operator typename ScopeX::NakedData(){return mb.entry_x();}
	ScopeRO entry_ro() const __attribute__((always_inline)) { return mb.entry_ro();}
	ScopeX entry_x() __attribute__((always_inline)){ return mb.entry_x();}

	static inline int index(int x,int y) __attribute__((always_inline)) {
		return ((y+OFFSET*MACRO_BLOCK_SIZE)/MACRO_BLOCK_SIZE)*S+((x+OFFSET*MACRO_BLOCK_SIZE)/MACRO_BLOCK_SIZE);
	}

	static void set(ScopeX& s,int x,int y,unsigned int col) __attribute__((always_inline)) {
		if(	x>=-OFFSET*MACRO_BLOCK_SIZE&&x<(S-OFFSET)*MACRO_BLOCK_SIZE&&
			y>=-OFFSET*MACRO_BLOCK_SIZE&&y<(S-OFFSET)*MACRO_BLOCK_SIZE)
			s[index(x,y)].set(x%MACRO_BLOCK_SIZE,y%MACRO_BLOCK_SIZE,col);
	}
	
	void set(int x,int y,unsigned int col) __attribute__((always_inline)) {
		if(	x>=-OFFSET*MACRO_BLOCK_SIZE&&x<(S-OFFSET)*MACRO_BLOCK_SIZE&&
			y>=-OFFSET*MACRO_BLOCK_SIZE&&y<(S-OFFSET)*MACRO_BLOCK_SIZE)
			mb[index(x,y)]->entry_x()->set(x%MACRO_BLOCK_SIZE,y%MACRO_BLOCK_SIZE,col);
	}
	
	static unsigned int get(ScopeRO& s,int x,int y) __attribute__((always_inline)) {
		if(	x>=-OFFSET*MACRO_BLOCK_SIZE&&x<(S-OFFSET)*MACRO_BLOCK_SIZE&&
			y>=-OFFSET*MACRO_BLOCK_SIZE&&y<(S-OFFSET)*MACRO_BLOCK_SIZE)
			return s[index(x,y)].get(x%MACRO_BLOCK_SIZE,y%MACRO_BLOCK_SIZE);
		else
			return 0;
	}
	
	static unsigned int get(ScopeX& s,int x,int y) __attribute__((always_inline)) {
		if(	x>=-OFFSET*MACRO_BLOCK_SIZE&&x<(S-OFFSET)*MACRO_BLOCK_SIZE&&
			y>=-OFFSET*MACRO_BLOCK_SIZE&&y<(S-OFFSET)*MACRO_BLOCK_SIZE)
			return s[index(x,y)].get(x%MACRO_BLOCK_SIZE,y%MACRO_BLOCK_SIZE);
		else
			return 0;
	}
	
	unsigned int get(int x,int y) __attribute__((always_inline)) {
		if(	x>=-OFFSET*MACRO_BLOCK_SIZE&&x<(S-OFFSET)*MACRO_BLOCK_SIZE&&
			y>=-OFFSET*MACRO_BLOCK_SIZE&&y<(S-OFFSET)*MACRO_BLOCK_SIZE)
			return mb[index(x,y)]->entry_ro()->get(x%MACRO_BLOCK_SIZE,y%MACRO_BLOCK_SIZE);
		else
			return 0;
	}

	template <int S2,int OFFSET2> friend class TiledImage;

	template <int S2,int OFFSET2> const TiledImage<S2,OFFSET2> getSubImage(int mbx,int mby) const {
		TiledImage<S2,OFFSET2> t;
		for(int y=0;y<S2;y++)
			for(int x=0;x<S2;x++)
				t.mb[y*S2+x]=(*const_cast<MacroBlockArray*>(&mb))[(mby+OFFSET-OFFSET2+y)*S+(mbx+OFFSET-OFFSET2+x)];
		return t;
	}

	class SubWindow {
	public:
		SubWindow(TiledImage::ScopeRO& s,int x_offset,int y_offset) : sw(s), x_offset(x_offset), y_offset(y_offset) {}
		unsigned int get(int x,int y) {
			return sw[TiledImage::index(x_offset+x,y_offset+y)].get((x_offset+x+MACRO_BLOCK_SIZE)%MACRO_BLOCK_SIZE,(y_offset+y+MACRO_BLOCK_SIZE)%MACRO_BLOCK_SIZE);
		}
	private:
		TiledImage::ScopeRO& sw;
		const int x_offset;
		const int y_offset;
	};


#if 0
	class ScopeRO {
	public:
		typedef ::PMC_NS::ScopeRO< ::PMC_NS::Var<const MacroBlock,FRAME_MEMORY> > ScopeType;
		ScopeRO(const TiledImage& t)  __attribute__((always_inline)) {
			for(int y=-OFFSET;y<S+OFFSET;y++)
				for(int x=-OFFSET;x<S+OFFSET;x++)
					new(&s[y][x]) ScopeType((const_cast<TiledImage*>(&t))->getMB_ro(x,y)->entry_ro());
		}
		~ScopeRO() __attribute__((always_inline)) {
/*			for(int y=-OFFSET;y<S+OFFSET;y++)
				for(int x=-OFFSET;x<S+OFFSET;x++)
					s[y][x].exit_ro(true);*/
		}
		unsigned int get(int x,int y) __attribute__((always_inline)) {
			return s[y/MACRO_BLOCK_SIZE][x%MACRO_BLOCK_SIZE]->get(x%MACRO_BLOCK_SIZE,y%MACRO_BLOCK_SIZE);
		}
	private:
		ScopeType s[S+OFFSET*2][S+OFFSET*2];
	};
	
	class ScopeX {
	public:
		typedef ::PMC_NS::ScopeX< ::PMC_NS::Var<MacroBlock,FRAME_MEMORY> > ScopeType;
//		static ScopeType::ScopePtr EntryAll(MacroBlock*& mb) __attribute__((always_inline)) { return reinterpret_cast<MacroBlockX*>(mb)->entry_x(); }
		ScopeX(TiledImage& t)  __attribute__((always_inline)) : s(t.mb.template map<ScopeType::ScopePtr,EntryAll<ScopeType::ScopePtr,MacroBlockX*> >()) {
/*			for(int y=0;y<S-2*OFFSET;y++)
				for(int x=0;x<S-2*OFFSET;x++)
					new(&s[y][x]) ScopeType(t.getMB_x(x,y)->entry_x());*/
		}
//		static void FlushAll(ScopeType& mb) __attribute__((always_inline)) { mb.flush(); }
		~ScopeX() __attribute__((always_inline)) {
			s.template map<FlushAll>();
/*			for(int y=0;y<S-2*OFFSET;y++)
				for(int x=0;x<S-2*OFFSET;x++)
					s[y][x].flush();*/
		}
		unsigned int get(int x,int y) __attribute__((always_inline)) {
			return s[(y/MACRO_BLOCK_SIZE*S+OFFSET)+x/MACRO_BLOCK_SIZE+OFFSET]->get(x%MACRO_BLOCK_SIZE,y%MACRO_BLOCK_SIZE);
		}
		void set(int x,int y,unsigned int col) __attribute__((always_inline)) {
			s[(y/MACRO_BLOCK_SIZE*S+OFFSET)+x/MACRO_BLOCK_SIZE+OFFSET]->set(x%MACRO_BLOCK_SIZE,y%MACRO_BLOCK_SIZE,col);
		}
	private:
		ScopeX operator=(const ScopeX& rhs);
		TArray<ScopeType,S*S> s;
	};

/*	ScopeRO entry_ro() const  __attribute__((always_inline)) {
		return ScopeRO(*const_cast<TiledImage*>(this));
	}
	
	ScopeX entry_x()  __attribute__((always_inline)) {
		return ScopeX(*const_cast<TiledImage*>(this));
	}*/
#endif

protected:
	MacroBlockX& accessMB(int mbx,int mby){return *(MacroBlockX*)mb[(mby+OFFSET)*S+mbx+OFFSET];}
	MacroBlockRO& accessMB(int mbx,int mby) const {return *(MacroBlockRO*)mb[(mby+OFFSET)*S+mbx+OFFSET];}
private:
	MacroBlockArray mb;
};

class PlainMotionVector {
public:
	typedef typename SmallestInt<clog2(MAX_SEARCH)>::type vector_t;
	typedef typename SmallestUInt<sizeof(vector_t)*2*8>::type dvector_t;
	PlainMotionVector(int delta_x,int delta_y) : delta_x(delta_x), delta_y(delta_y) {}
	PlainMotionVector(const dvector_t& rhs) : delta_x(0), delta_y(0) { full=rhs; }
	PlainMotionVector& operator=(const dvector_t& rhs) {
		full=rhs;
		return *this;
	}
	union {
		struct { vector_t delta_x,delta_y; };
		dvector_t full;
	};
};

class MotionVector {
public:
	typedef PlainMotionVector::vector_t vector_t;
	typedef PlainMotionVector::dvector_t dvector_t;
	MotionVector() {
		memset(this,0x80,sizeof(this));
	}
	MotionVector(const PlainMotionVector& v) {
		this->full=v.full;
	}
	MotionVector& operator=(const PlainMotionVector& rhs) __attribute__((always_inline)) {
		ScopeX_type(this->full) s=this->full.entry_x();
		s=rhs.full;
		s.exit_x(true);
		return *this;
	}
	
	static bool isValid(vector_t v) { return v>-MAX_SEARCH-1;}
	static bool isValid(const dvector_t& v) {
		PlainMotionVector p(v);
		return isValid(p.delta_x)&&isValid(p.delta_y);
	}
	::PMC_NS::Var<dvector_t,FRAME_MEMORY> full;
private:
	MotionVector(const MotionVector& m);
	MotionVector& operator=(const MotionVector& rhs);
} ATTR_DATA_ALIGNED;

typedef TiledImage<3,1> SearchWindow;

/*class SearchWindow : public  {
public:
	typedef TiledImage<3,1> base;
	SearchWindow(const base& t) : base(t) {}
	
	class SubWindow {
	public:
		SubWindow(SearchWindow::ScopeRO& sw,int x_offset,int y_offset) : sw(sw), x_offset(MACRO_BLOCK_SIZE+x_offset), y_offset(MACRO_BLOCK_SIZE+y_offset) {}
		unsigned int get(int x,int y) {
			return sw[SearchWindow::index(x,y)].get(x_offset+x,y_offset+y);
		}
	private:
		SearchWindow::ScopeRO& sw;
		const int x_offset;
		const int y_offset;
	};

/ *	SubWindow getSubWindow(int x_offset,int y_offset) __attribute__((always_inline)) {
		return SubWindow(*this,MACRO_BLOCK_SIZE+x_offset,MACRO_BLOCK_SIZE+y_offset);
//		return reinterpret_cast<const MacroBlock*>(&this->pix[MAX_SEARCH+y_offset].line.c[(MAX_SEARCH+x_offset)/(sizeof(base::chunk_t)*8)]);
	}* /
};*/

/*
template <int P>
class SearchWindowBase : public PixelView<2*MAX_SEARCH+MACRO_BLOCK_SIZE,P> {
public:
	typedef PixelView<2*MAX_SEARCH+MACRO_BLOCK_SIZE,P> base;

	template <int A> SearchWindowBase(const SearchWindowBase<A>& s) : base(s) {}

	class SubWindow {
	public:
		SubWindow(const SearchWindowBase& sw,int x_offset,int y_offset) : sw(sw), x_offset(x_offset), y_offset(y_offset) {}
		unsigned int get(int x,int y) const {
			return sw.get(x_offset+x,y_offset+y);
		}
	private:
		const SearchWindowBase& sw;
		const int x_offset;
		const int y_offset;
	};

	const SubWindow getSubWindow(int x_offset,int y_offset) const {
		return SubWindow(*this,MAX_SEARCH+x_offset,MAX_SEARCH+y_offset);
//		return reinterpret_cast<const MacroBlock*>(&this->pix[MAX_SEARCH+y_offset].line.c[(MAX_SEARCH+x_offset)/(sizeof(base::chunk_t)*8)]);
	}

/ *	typedef typename SmallestUInt<MAX_SEARCH>::type margin_t;
	typedef typename MacroBlock::line_t mb_t;
	typedef typename SmallestUInt<2*MAX_SEARCH+MACRO_BLOCK_SIZE>::type line_t;

	SearchWindow() : pix() {}

	MacroBlock get_mb(int x_offset,int y_offset) const {
		MacroBlock mb;
		for(int y=0;y<MACRO_BLOCK_SIZE;y++)
			mb.set(y,(mb_t)(pix[MAX_SEARCH+y_offset+y].line>>(MAX_SEARCH+x_offset)));
		return mb;
	}
	void set(int y,margin_t left,mb_t middle,margin_t right){
		pix[y].left=left;
		pix[y].middle=middle;
		pix[y].right=right;
		if(pix[y].line)
			DEBUG("Search window %p: set %d = 0x%x 0x%x 0x%x == 0x%lx\n",this,y,(int)right,(int)middle,(int)left,(unsigned long)pix[y].line);
	}
private:
	union {
		struct { margin_t right; mb_t middle; margin_t left; } __attribute__((packed));
		line_t line;
	} pix[MACRO_BLOCK_SIZE+2*MAX_SEARCH];* /
};

typedef SearchWindowBase<MACRO_BLOCK_SIZE*(FRAME_SIZE+2)-(2*MAX_SEARCH+MACRO_BLOCK_SIZE)> SearchWindowInFrame;
typedef SearchWindowBase<0> SearchWindow;*/

typedef unsigned int cost_t;
#define MAX_COST ((cost_t)-1)

template <typename T>
static T abs(T i){
	return i<0?-i:i;
}

template <typename T>
static T power(T base,unsigned int power){
	if(power==0)
		return 1;
	
	T res=base;
	while(power-->0)
		res*=base;

	return res;
}

static cost_t md_match(SearchWindow::SubWindow& block_ref, const MacroBlock& block_cur){
	cost_t costs=0;
#ifdef COST_MAD
	for(int x=0;x<MACRO_BLOCK_SIZE;x++)
		for(int y=0;y<MACRO_BLOCK_SIZE;y++)
			costs+=abs((int)block_cur.get(x,y)-(int)block_ref.get(x,y));
//	costs=costs/(MACRO_BLOCK_SIZE*MACRO_BLOCK_SIZE);
#elif defined(COST_MSE)
	for(int x=0;x<MACRO_BLOCK_SIZE;x++)
		for(int y=0;y<MACRO_BLOCK_SIZE;y++)
			costs+=power((int)block_cur.get(x,y)-(int)block_ref.get(x,y),2);
//	costs=costs/(MACRO_BLOCK_SIZE*MACRO_BLOCK_SIZE);
#endif
	return costs;
}

// atomic workload packet
static void md_search(SearchWindow::ScopeRO& search_window,const MacroBlock& block,PlainMotionVector& vector){
	int min_x_offset=0;
	int min_y_offset=0;
	cost_t min_diff=MAX_COST;

/*	// check for offset (0,0)
	cost_t diff=md_match(search_window->getSubWindow(0,0),block);
	if(diff<=min_diff){
		min_x_offset=0;
		min_y_offset=0;
		min_diff=diff;
	}
*/
	// check for all offsets, except (0,0)
//	for(int i=0;i<0;i++)
	for(int x_offset=0;x_offset<2*MAX_SEARCH-2;x_offset=-x_offset+(x_offset<0?1:-1))
		for(int y_offset=0;y_offset<2*MAX_SEARCH-2;y_offset=-y_offset+(y_offset<0?1:-1))
/*			if(x_offset!=0||y_offset!=0)*/{
				SearchWindow::SubWindow sw(search_window,x_offset/2,y_offset/2);
				cost_t diff=md_match(sw,block);
				if(diff<min_diff){
					min_x_offset=x_offset/2;
					min_y_offset=y_offset/2;
					min_diff=diff;
				}
			}

	vector=PlainMotionVector(-min_x_offset,-min_y_offset);
}

//////////////////////////////////////////////////
// Frame generation

/*class Frame : public PixelView<MACRO_BLOCK_SIZE*(FRAME_SIZE+2)> {
public:
	typedef PixelView<MACRO_BLOCK_SIZE*(FRAME_SIZE+2)> base;
	typedef MotionVector MotionVectorList[FRAME_SIZE][FRAME_SIZE];
	Frame() : base(), mv() {}
	
	::PMC_NS::Var<const MacroBlockInFrame,FRAME_MEMORY>* getMacroBlock(int mbx,int mby) const { 
		return Frame::getMacroBlock(this,mbx,mby);
	}

	template <typename A> static ::PMC_NS::Var<const MacroBlockInFrame,FRAME_MEMORY>* getMacroBlock(A* that,int mbx,int mby) { 
		return Frame::getMacroBlock(reinterpret_cast<const Frame*>(that),mbx,mby);
	}

	static ::PMC_NS::Var<const MacroBlockInFrame,FRAME_MEMORY>* getMacroBlock(const Frame* that,int mbx,int mby) { 
		return reinterpret_cast< ::PMC_NS::Var<const MacroBlockInFrame,FRAME_MEMORY>*>(const_cast<base::chunk_t*>(
			&that->pix[(mby+1)*MACRO_BLOCK_SIZE].line.c[(mbx+1)*MACRO_BLOCK_SIZE/(sizeof(base::chunk_t)*8)]));
	}
	
	::PMC_NS::Var<const SearchWindowInFrame,FRAME_MEMORY>* getSearchWindow(int mbx,int mby) const {
		return Frame::getSearchWindow(this,mbx,mby);
	}

	template <typename A> static ::PMC_NS::Var<const SearchWindowInFrame,FRAME_MEMORY>* getSearchWindow(A* that,int mbx,int mby) {
		return Frame::getSearchWindow(reinterpret_cast<const Frame*>(that),mbx,mby);
	}

	static ::PMC_NS::Var<const SearchWindowInFrame,FRAME_MEMORY>* getSearchWindow(const Frame* that,int mbx,int mby) {
		return reinterpret_cast< ::PMC_NS::Var<const SearchWindowInFrame,FRAME_MEMORY>*>(const_cast<base::chunk_t*>(
			&that->pix[(mby+1)*MACRO_BLOCK_SIZE-MAX_SEARCH].line.c[((mbx+1)*MACRO_BLOCK_SIZE-MAX_SEARCH)/(sizeof(base::chunk_t)*8)]));
	}
	
	MotionVector* getMotionVector(int x,int y) const {
		return const_cast<MotionVector*>(&mv[y][x]);
	}

	MotionVectorList* getMotionVectors(){
		return reinterpret_cast<MotionVectorList*>(&mv);}
	
	unsigned int get(int x,int y) const {
		return base::get(x+MACRO_BLOCK_SIZE,y+MACRO_BLOCK_SIZE);
	}

	void set(int x,int y,unsigned int col){
		base::set(x+MACRO_BLOCK_SIZE,y+MACRO_BLOCK_SIZE,col);
	}
private:
	MotionVector mv[FRAME_SIZE][FRAME_SIZE] ATTR_DATA_ALIGNED;

/ *	Frame() : mb() {}

	const MacroBlock& getMacroBlock(int mbx,int mby) const { return mb[mby+1][mbx+1]; }
	MacroBlock& getMacroBlock(int mbx,int mby) { return mb[mby+1][mbx+1]; }

	const SearchWindow getSearchWindow(int mbx,int mby) const {
		SearchWindow s;
		for(int sy=0;sy<MAX_SEARCH;sy++)
			s.set(sy,
				getMacroBlock(mbx-1,mby-1).getRightBits(MACRO_BLOCK_SIZE-MAX_SEARCH+sy,MAX_SEARCH),
				getMacroBlock(mbx,mby-1).getLine(MACRO_BLOCK_SIZE-MAX_SEARCH+sy),
				getMacroBlock(mbx+1,mby-1).getLeftBits(MACRO_BLOCK_SIZE-MAX_SEARCH+sy,MAX_SEARCH));
		for(int sy=0;sy<MACRO_BLOCK_SIZE;sy++)
			s.set(sy+MAX_SEARCH,
				getMacroBlock(mbx-1,mby).getRightBits(sy,MAX_SEARCH),
				getMacroBlock(mbx,mby).getLine(sy),
				getMacroBlock(mbx+1,mby).getLeftBits(sy,MAX_SEARCH));
		for(int sy=0;sy<MAX_SEARCH;sy++)
			s.set(sy+MAX_SEARCH+MACRO_BLOCK_SIZE,
				getMacroBlock(mbx-1,mby+1).getRightBits(sy,MAX_SEARCH),
				getMacroBlock(mbx,mby+1).getLine(sy),
				getMacroBlock(mbx+1,mby+1).getLeftBits(sy,MAX_SEARCH));

		return s;
	}

	unsigned int get(int x,int y) const {
		if(	x>=0 && x<FRAME_SIZE*MACRO_BLOCK_SIZE &&
			y>=0 && y<FRAME_SIZE*MACRO_BLOCK_SIZE)
			return getMacroBlock(x/MACRO_BLOCK_SIZE,y/MACRO_BLOCK_SIZE).get(x%MACRO_BLOCK_SIZE,y%MACRO_BLOCK_SIZE);
		else
			return 0;
	}
	void set(int x,int y,unsigned int col) {
		if(	x>=0 && x<FRAME_SIZE*MACRO_BLOCK_SIZE &&
			y>=0 && y<FRAME_SIZE*MACRO_BLOCK_SIZE)
			getMacroBlock(x/MACRO_BLOCK_SIZE,y/MACRO_BLOCK_SIZE).set(x%MACRO_BLOCK_SIZE,y%MACRO_BLOCK_SIZE,col);
	}
	MotionVector& getMotionVector(int x,int y){return getMacroBlock(x,y).mv;}
	const MotionVector& getMotionVector(int x,int y) const {return getMacroBlock(x,y).mv;}
private:
	MacroBlock mb[FRAME_SIZE+2][FRAME_SIZE+2];* /
};*/

class Frame : public TiledImage<FRAME_SIZE+2,1> {
public:
	static const int S=FRAME_SIZE+2;
	typedef TiledImage<S,1> base;
	typedef MotionVector MotionVectorList[FRAME_SIZE][FRAME_SIZE];
	Frame(MacroBlockP (&mbs)[S][S]) : base(mbs), mv() {}
	
	const SearchWindow getSearchWindow(int mbx,int mby) const {
		return base::getSubImage<3,1>(mbx,mby);
	}
	
	MotionVector* getMotionVector(int x,int y) const {
		return const_cast<MotionVector*>(&mv[y][x]);
	}
	
	MotionVectorList* getMotionVectors() {
		return &mv;
	}
	
	static ::PMC_NS::Var<Frame,::PMC_NS::cache>* Create(){
		void* f=scalloc(1,sizeof(Frame)+S*S*sizeof(MacroBlock));
		MacroBlockV (*mbs)[S][S]=(MacroBlockV(*)[S][S])((uintptr_t)f+sizeof(Frame));
		MacroBlockP mbp[S][S];
		for(int y=0;y<S;y++)
			for(int x=0;x<S;x++)
				mbp[y][x]=&(*mbs)[y][x];
		new(f) Frame(mbp);
		return reinterpret_cast< ::PMC_NS::Var<Frame,::PMC_NS::cache>*>(f);
	}

private:
	MotionVector mv[FRAME_SIZE][FRAME_SIZE] ATTR_DATA_ALIGNED;
};


typedef ::PMC_NS::Var<Frame,::PMC_NS::cache> VFrame;
typedef typename VFrame::CVar CFrame;

class WorkPacket {
public:
	WorkPacket() {}
	WorkPacket(CFrame* frame_ref,VFrame* frame_cur, Frame::MotionVectorList* mv,int x, int y) :
		frame_ref(frame_ref), frame_cur((CFrame*)frame_cur), mv(mv), x(x), y(y) {}

	CFrame* frame_ref;
	CFrame* frame_cur;
	Frame::MotionVectorList* mv;
	typename SmallestInt<clog2(FRAME_SIZE-1)>::type x,y;
}__attribute__((aligned(sizeof(long))));

static bool md_generate_work(WorkPacket& w,bool reset,void* arg);

WorkQueue<WorkPacket,NUM_PROCESSORS/2> wq(WQ_DEPTH,md_generate_work);
mfifo_t<CFrame*,1,PAR_FRAMES,::PMC_NS::cache> output_frames PMC_VAR_ATTR_CACHE;

static VFrame* md_next_frame(Frame::MotionVectorList** mv){
	VFrame* frame=Frame::Create();
	DEBUG("Generating frame %p...\n",frame);
	ScopeX_type(*frame) f=frame->entry_x();

	static int framenr=0;
	framenr++;
	if(framenr%16==0){
		static struct timeval prev_frame_tv={};
		struct timeval tv;
		gettimeofday(&tv,NULL);
		if(prev_frame_tv.tv_sec!=0)
			printf("Frame: %.4f s\n",(double)((
				((float)tv.tv_sec+(float)tv.tv_usec/1000000.0f)-
				((float)prev_frame_tv.tv_sec+(float)prev_frame_tv.tv_usec/1000000.0f))/16.0f));
		prev_frame_tv=tv;
	}
	if(framenr%0x100==0)
		prf_mem_dump();

/*	{
		static int x=0;
		ScopeX_type(*f) p=f->entry_x();
		Frame::set(p,x,x+20,1);
		Frame::set(p,x+1,x+20,1);
		Frame::set(p,x,x+21,1);
		Frame::set(p,x+1,x+21,1);
		Frame::set(p,x*3/2-10,100-x/2,1);
		
		x=(x+2)%IMG_SIZE;
	}*/
	{
		static int x=0;
		f->set(x,x+20,1);
		f->set(x+1,x+20,1);
		f->set(x,x+21,1);
		f->set(x+1,x+21,1);
		f->set(x*3/2-10,100-x/2,1);
		
		x=(x+2)%IMG_SIZE;
	}

	*mv=f->getMotionVectors();
	f.exit_x(true);
	output_frames.push(&frame->makeConst());
	return frame;
}

static bool md_generate_work(WorkPacket& w,bool reset,void* arg){
	static CFrame* frame_ref=NULL;
	static VFrame* frame_cur=NULL;
	static int x=0;
	static int y=0;
	static Frame::MotionVectorList* mv=NULL;

	if(!frame_cur)
		frame_cur=md_next_frame(&mv);

	if(++x>=FRAME_SIZE){
		x=0;
		y++;
	}

	if(reset||!frame_ref||y>=FRAME_SIZE){
		frame_ref=(CFrame*)frame_cur;
		frame_cur=md_next_frame(&mv);
		x=0;
		y=0;
	}

	w=WorkPacket(frame_ref,frame_cur,mv,x,y);
	return true;
}

//////////////////////////////////////////////////
// Search slave

static void* md_search_slave(void* arg) __attribute__((flatten));
static void* md_search_slave(void* arg) {
	WorkPacket w;
	while(wq.Pop(w)){
		DEBUG("@%d: Got work %p->%p (%d,%d)\n",GetProcID(),w.frame_ref,w.frame_cur,w.x,w.y);
		ScopeRO_type(*w.frame_ref) scope_ref=w.frame_ref->entry_ro();
		SearchWindow sw=scope_ref->getSearchWindow(w.x,w.y);
		ScopeRO_type(sw) sws=sw.entry_ro();

		ScopeRO_type(*w.frame_cur) scope_cur=w.frame_cur->entry_ro();
		Frame::MacroBlockRO* mb=scope_cur->getMB_ro(w.x,w.y);
		ScopeRO_type(*mb) mbs=mb->entry_ro();

		PlainMotionVector pmv(0,0);
		md_search(sws,mbs,pmv);
		DEBUG("@%d: Vector (%d,%d) -> +(%d,%d): \n",GetProcID(),w.x,w.y,pmv.delta_x,pmv.delta_y);
		(*w.mv)[w.y][w.x]=pmv;
	}
	return NULL;
}

auto_ret<int,0> md_start_workers(){
	return wq.StartSlaves(SLAVES,md_search_slave);
}

//////////////////////////////////////////////////
// Drawing

static void md_render(){
	CFrame* f_ref=NULL;
	CFrame* f_cur=NULL;

	render_init(1);
	// fill screen
	fillrect(0,0,DVI_WIDTH,DVI_HEIGHT,black);
	render_flip_buffer();
	fillrect(0,0,DVI_WIDTH,DVI_HEIGHT,black);

	while(true){
		f_ref=f_cur;
		
		//draw background
		fillrect(0,0,FRAME_SIZE*MACRO_BLOCK_SIZE,FRAME_SIZE*MACRO_BLOCK_SIZE,convert_rgb(40,40,40));
		//draw grid
		for(int i=0;i<FRAME_SIZE;i++){
			drawline(0,i*MACRO_BLOCK_SIZE,FRAME_SIZE*MACRO_BLOCK_SIZE,i*MACRO_BLOCK_SIZE,convert_rgb(80,80,80));
			drawline(i*MACRO_BLOCK_SIZE,0,i*MACRO_BLOCK_SIZE,FRAME_SIZE*MACRO_BLOCK_SIZE,convert_rgb(80,80,80));
		}
		//get frame
		output_frames.pop(f_cur);
		if(!f_ref)
			continue;
		DEBUG("Drawing frame %p...\n",f_cur);
		//draw frame
		ScopeRO_type(*f_cur) frame_cur=f_cur->entry_ro();
		{
			ScopeRO_type(*f_ref) frame_ref=f_ref->entry_ro();
			for(int mby=0;mby<FRAME_SIZE;mby++)
				for(int mbx=0;mbx<FRAME_SIZE;mbx++){
					Frame::MacroBlockRO* mb_ref=frame_ref->getMB_ro(mbx,mby);
					Frame::MacroBlockRO* mb_cur=frame_cur->getMB_ro(mbx,mby);
					ScopeRO_type(*mb_ref) mbs_ref=mb_ref->entry_ro();
					ScopeRO_type(*mb_cur) mbs_cur=mb_cur->entry_ro();
					for(int y=0;y<MACRO_BLOCK_SIZE;y++)
						for(int x=0;x<MACRO_BLOCK_SIZE;x++){
							if(mbs_ref->get(x,y))
								drawpixel(x+mbx*MACRO_BLOCK_SIZE,y+mby*MACRO_BLOCK_SIZE,convert_rgb(128,128,128));
							if(mbs_cur->get(x,y))
								drawpixel(x+mbx*MACRO_BLOCK_SIZE,y+mby*MACRO_BLOCK_SIZE,convert_rgb(255,255,255));
						}
				}
		}
		// wait for motion vectors
		for(int y=0;y<FRAME_SIZE;y++)
			for(int x=0;x<FRAME_SIZE;x++){
				MotionVector* mv=frame_cur->getMotionVector(x,y);
				ScopeRO_type(mv->full) v=mv->full.entry_ro();
				bool ready=true;
				while(!MotionVector::isValid(v.read())){ready=false;Yield(false);v.refresh();}
				PlainMotionVector pmv(v.read());
				drawline(
					x*MACRO_BLOCK_SIZE+MACRO_BLOCK_SIZE/2,
					y*MACRO_BLOCK_SIZE+MACRO_BLOCK_SIZE/2,
					x*MACRO_BLOCK_SIZE+MACRO_BLOCK_SIZE/2+pmv.delta_x,
					y*MACRO_BLOCK_SIZE+MACRO_BLOCK_SIZE/2+pmv.delta_y,
					ready?green:red);
			}
//		frame_cur.exit_ro();
		// show on monitor
		render_flip_buffer();
		delete f_ref;
	}
	render_destroy();
}

static auto_ret<int,0> md_start_render(){
	return par(md_render);
}

int main(int argc,char** argv){
	printf("Starting workers...\n");
	md_start_workers();

	printf("Starting rendering...\n");
	md_start_render();
	
	prf_mem_dump();
	pause();
	return 0;
}

