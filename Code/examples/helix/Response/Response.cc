#include <helix.h>
#include <sys/time.h>
#include <sys/io.h>

#define TEST_PROC			2
#define NO_TEST_PROC		3
#define ITERATIONS			100
#define LOCAL_ITERATIONS	(ITERATIONS*10)

void* BusyProc(void* arg){
	while(true){
		for(volatile int i=0;i<1000000;i++);
//		outbyte('.');
	}
	return NULL;
}

void* f(void* arg){
//	outbyte('!');
	return NULL;
}

#define TIMESTAMP_START(id) \
	struct timeval start,end,diff; \
	{int n __attribute__((unused))=TracePhaseChange(TRACE_PHASE_RESERVED+id);} \
	gettimeofday(&start,NULL)
#define TIMESTAMP_END(iterations,group,msg)	\
	do{ \
		gettimeofday(&end,NULL); \
		int n __attribute__((unused))=TracePhaseChange(TRACE_PHASE_RUNNING); \
		unsigned int us=(end.tv_sec*1000000+end.tv_usec-start.tv_sec*1000000-start.tv_usec)/((iterations)/(group)); \
		diff.tv_sec=us/1000000; \
		diff.tv_usec=us%1000000; \
		printf("*** %-25s: %3ld.%06ld\n",msg,diff.tv_sec,diff.tv_usec); \
	}while(0)

#define sleep(s) \
	do{for(volatile int z=s*1000000;z>0;z--);}while(0)

static void printConfig(){
	printf("Task daemon timeslice: %d ms, ",TASK_DAEMON_TIMESLICE);
#ifdef SCHEDULE_SLOTS
	printf("slot length: %.3f ms, ",(float)SCHEDULE_SLOTS/(float)GetClockFreq()*1000.0);
#else
	printf("no schedule slots, ");
#endif
#ifdef SCHEDULING_IN_LOCAL_RAM
	printf("scheduling code local\n");
#else
	printf("scheduling code in DDR\n");
#endif
}

static void testScheduleStats(){
	pid_t pid;
	dCreateProcess(pid,BusyProc,NULL,PROC_DEFAULT_TIMESLICE,PROC_DEFAULT_STACK,NO_TEST_PROC);
	dStartProcess(pid,NO_TEST_PROC);
	dCreateProcess(pid,BusyProc,NULL,PROC_DEFAULT_TIMESLICE/2,PROC_DEFAULT_STACK,NO_TEST_PROC);
	dStartProcess(pid,NO_TEST_PROC);
}

static void testContextSwitch(){
	sleep(1);
	// Test context switch speed
	// Every Yield will switch to the task daemon and yield back.
	// The task daemon does nothing, except for garbage collection, which is one syscall to kGetZombie()
	TIMESTAMP_START(0);
	for(int i=0;i<LOCAL_ITERATIONS;i++)
		Yield();
	TIMESTAMP_END(LOCAL_ITERATIONS,500,"1000 yields");
}

static void testSyscall(){
	// Test syscall overhead
	// We use GetProcessFlags() with NULL argument. The syscall will fail with EINVAL immediately,
	// hence, the switch to the kernel and back is the only thing that actually happened.
	sleep(1);
	TIMESTAMP_START(1);
	for(int i=0;i<LOCAL_ITERATIONS;i++)
		lGetProcessFlags(0,NULL);
	TIMESTAMP_END(LOCAL_ITERATIONS,1000,"1000 syscalls");
}

static void testSyscallWrapper(){
	// Test syscall overhead via syscall wrapper
	sleep(1);
	TIMESTAMP_START(2);
	for(int i=0;i<LOCAL_ITERATIONS;i++)
		GetProcessFlags(0,NULL);
	TIMESTAMP_END(LOCAL_ITERATIONS,1000,"1000 syscalls wrapped");
}

static void testSyscallRedir(){
	// Test syscall overhead via task daemon
	sleep(1);
	TIMESTAMP_START(3);
	for(int i=0;i<LOCAL_ITERATIONS;i++)
		dGetProcessFlags(0,NULL);
	TIMESTAMP_END(LOCAL_ITERATIONS,1000,"1000 syscalls redirected");
}

static void testIdlePing(){
	// Ping idle core
	sleep(1);
	TIMESTAMP_START(4);
	for(int i=0;i<ITERATIONS;i++)
		dRemoteCall(f,NULL,TEST_PROC);
	TIMESTAMP_END(ITERATIONS,1,"Ping idle core");
}

static void testBusyPing(){
	// Ping busy core
	printf("Starting busy process on core %d...\n",TEST_PROC);
	pid_t pid;
	dCreateProcess(pid,BusyProc,NULL,PROC_DEFAULT_TIMESLICE,PROC_DEFAULT_STACK,TEST_PROC);
	dStartProcess(pid,TEST_PROC);
	sleep(1);
	TIMESTAMP_START(5);
	for(int i=0;i<ITERATIONS;i++)
		dRemoteCall(f,NULL,TEST_PROC);
	TIMESTAMP_END(ITERATIONS,1,"Ping busy core");
	dDestroyProcess(pid,NULL,TEST_PROC);
}

static void testSyscallRedirBusy(){
	// Test syscall overhead via task daemon on busy core
	sleep(1);
	pid_t pid;
	CreateProcess(pid,BusyProc,NULL,1000,PROC_DEFAULT_STACK);
	StartProcess(pid);
	Yield(); // allow busy proc to start
	Yield();
	Yield();
	TIMESTAMP_START(6);
	for(int i=0;i<10;i++)
		dGetProcessFlags(0,NULL);
	TIMESTAMP_END(10,10,"10 syscalls busy redir");
	DestroyProcess(pid,NULL);
}

static void testTime(){
	// Measure time to execute gettimeofday()
	sleep(1);
	struct timeval test;
	TIMESTAMP_START(7);
	for(int i=0;i<LOCAL_ITERATIONS;i++)
		gettimeofday(&test,NULL);
	TIMESTAMP_END(LOCAL_ITERATIONS,1000,"1000 gettimeofday()");
}

void BogoMIPS(){
	sleep(1);
	asm volatile (
		"   msrclr	r0, 1<<1;\n"	/* disable interrupts */
		"	nop;"
	);

	unsigned int loops=1<<16;
	float s=0.0;
	while(true){
		unsigned int cnt=loops;
		TIMESTAMP_START(8);
		asm volatile (
			"1: bneid	%0, 1b;"		/* do delay loop */
			"   addik	%0, %0, -1;"
			:"=r"(cnt):"0"(cnt):
		);
		TIMESTAMP_END(1,1,"BogoMIPS measurement");
		s=(float)diff.tv_sec+(float)diff.tv_usec/1000000.0;
		if(s>0.1||loops>=(1<<28))
			break;
		loops<<=1;
	}

	asm volatile (
		"   msrset	r0, 1<<1;\n"	/* enable interrupts */
		"	nop;"
	);
	// make sure the timer is (still) running
	Yield();

	// calculate BogoMIPS (assume time measurement is neglectible)
	printf("BogoMIPS: %.2f\n",((float)(2*loops)/1000000.0)/s);
}

int main(int argc,char** argv){
	printf("\n\nTesting reponse times\n");
	printConfig();

	testTime();
#ifndef ENABLE_TRACING
	BogoMIPS();
#endif
	testScheduleStats();
	testContextSwitch();
	testSyscall();
	testSyscallWrapper();
	testSyscallRedir();
	testIdlePing();
	testBusyPing();
	testSyscallRedirBusy();

	printf("Done\n");
	return 0;
}

