#ifndef FFT_H_
#define FFT_H_

struct complex_t {
	typedef float T;
	T i,r;
	complex_t() : i(), r() {}
	complex_t(T v) : i(), r(v) {}
	T real() const {return r;}
	T imag() const {return i;}

	complex_t& operator =(T r){	this->i =0;	this->r=r;	return *this;}

	complex_t& operator/=(T r){	this->i/=r;	this->r/=r;	return *this;}
	complex_t& operator*=(T r){	this->i*=r;	this->r*=r;	return *this;}
	complex_t& operator+=(T r){				this->r+=r;	return *this;}
	complex_t& operator-=(T r){				this->r-=r;	return *this;}

	complex_t& operator+=(complex_t const& c){ this->i+=c.i; this->r+=c.r;	return *this;}
	complex_t& operator-=(complex_t const& c){ this->i-=c.i; this->r-=c.r;	return *this;}

	complex_t operator+(complex_t const& c) const { complex_t res(*this);res+=c; return res; }
	complex_t operator-(complex_t const& c) const { complex_t res(*this);res-=c; return res; }
};

int cfft( complex_t x[], int n, int m, int itype );
int cfft2( complex_t x[], int n, int m, int itype );

#endif /* FFT_H_ */
