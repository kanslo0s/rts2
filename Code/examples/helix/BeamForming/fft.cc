/*
 * fft.c - FFT and windowing routines for use on a Microblaze to perform signal processing
 *
 * Note: this is used in the spectrum analyzer demo and as such the functions are hardcoded to 512 points. Modify to access
 * other sizes.
 *
 *  Created on: May 19, 2011
 *      Author: Berend Dekens <b.h.j.dekens@utwente.nl>
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "fft.h"

/**
 * This is a very simple Cooley-Tukey Radix-2 DIF Complex FFT.
 *
 * The data sequence is of size N = 2**M.
 * X and Y contain the real and imaginary parts of the data.
 *
 * ITYPE .ne. -1 for forward transform
 * ITYPE .eq. -1 for backward transform
 *
 * The forward transform computes
 * 		Z(k) = sum_{j=0}^{N-1} z(j)*exp(-2ijk*pi/N)
 * The backward transform computes
 * 		z(j) = (1/N) * sum_{k=0}^{N-1} Z(k)*exp(2ijk*pi/N)
 *
 * 		Steve Kifowit, 31 October 1998
 */
int cfft( complex_t x[], int n, int m, int itype )
{
   const float TWO_PI = 6.283185307179586476925287;
   register int h, j, k, l, n1, n2, ie, ia;
   float c, s, xt, yt, p, a;
   complex_t tmp;

   // Quick return for n=1
   if ( n == 1 ) {
      return 0;
   }

   // Conjugate if backward transform
   if ( itype == -1 )
      for (h=0;h<n;h++)
    	  x[h].i = -x[h].i;

	// Main loop
	p = TWO_PI / float(n);
	n2 = n;
	for (k=1; k<=m; k++) {
		n1 = n2;
		n2 /= 2;
		ie = n / n1;
		ia = 1;
		for (j=0; j<n2; j++) {
			a = float( ia - 1 ) * p;
			//printf("a : %f\r\n", a);
			c = cos( a );
			s = sin( a );
			ia += ie;
			for ( h = j; h < n; h += n1 ) {
				l = h + n2;
				xt = x[h].r - x[l].r;
				x[h].r += x[l].r;
				yt = x[h].i - x[l].i;
				x[h].i += x[l].i;
				x[l].r = c * xt + s * yt;
				x[l].i = c * yt - s * xt;
			}
		}
	}

	// Bit reversal permutation
	j = 0;
	for (h=0; h<n-1; h++) {
		if (h<j) {
			tmp = x[j];
			x[j] = x[h];
			x[h] = tmp;
		}
		k = n / 2;
		while ( k < j+1 ) {
			j -= k;
			k /= 2;
		}
		j += k;
	}

	// Conjugate and normalize if backward transform
	if (itype == -1) {
		p = float(n);
		for (h=0; h<n; h++) {
			x[h].i /= -p;
			x[h].r /= p;
		}
	}

	return 0;
}

static complex_t *W = NULL;	// Lookup table for Twiddle factors
static void free_twiddle(){ if(W)free(W);}

// Same as above but now with a lookup table for cos and sin
int cfft2( complex_t x[], int n, int m, int itype )
{
	const float TWO_PI = 6.283185307179586476925287;
	register int h, j, k, l, n1, n2, ie, ia;
	float c, s, xt, yt, p, a;
	complex_t tmp;

	// Fill lookup table if this is the first run
	if(W == NULL) {
		W = (complex_t *)malloc(sizeof(complex_t) * n);
		atexit(free_twiddle);
		if(W == NULL) {
			printf("MAlloc failed for lookup Twiddle factor lookup table\r\n");
			exit(1);
		}

		// Fill the list with Twiddle factors
		p = TWO_PI / n;
		for(k=0;k<n;k++) {
			a = float(k-1) * p;
			W[k].r = cos(a);
			W[k].i = sin(a);
		}
	}

	// Quick return for n=1
	if ( n == 1 ) return 0;

   // Conjugate if backward transform
   if ( itype == -1 )
      for (h=0;h<n;h++)
    	  x[h].i = -x[h].i;

	// Main loop
	p = TWO_PI / float(n);
	n2 = n;
	for (k=1; k<=m; k++) {
		n1 = n2;
		n2 /= 2;
		ie = n / n1;
		ia = 1;
		for (j=0; j<n2; j++) {
			//a = float( ia - 1 ) * p;
			//printf("a : %f\r\n", a);
			//c = cos( a );
			//s = sin( a );
			c = W[ia].r;
			s = W[ia].i;
			ia += ie;
			for ( h = j; h < n; h += n1 ) {
				l = h + n2;
				xt = x[h].r - x[l].r;
				x[h].r += x[l].r;
				yt = x[h].i - x[l].i;
				x[h].i += x[l].i;
				x[l].r = c * xt + s * yt;
				x[l].i = c * yt - s * xt;
			}
		}
	}

	// Bit reversal permutation
	j = 0;
	for (h=0; h<n-1; h++) {
		if (h<j) {
			tmp = x[j];
			x[j] = x[h];
			x[h] = tmp;
		}
		k = n / 2;
		while ( k < j+1 ) {
			j -= k;
			k /= 2;
		}
		j += k;
	}

	// Conjugate and normalize if backward transform
	if (itype == -1) {
		p = float(n);
		for (h=0; h<n; h++) {
			x[h].i /= -p;
			x[h].r /= p;
		}
	}

	return 0;
}

