// http://www.labbookpages.co.uk/audio/beamforming/delaySum.html

#define CFIFO_BUSY_WAIT		-1 // busy loop
//#define CFIFO_DEBUG

#ifdef __MICROBLAZE__
#  include <helix.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <memory>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <stdint.h>
#include <sys/time.h>
#include <pthread.h>
#include "fft.h"

// #define DISABLE_THREADS
// #define BF_DEBUG
// #define BF_TIME_TASKS

////////////////////////////////////////////////
// Global constants
////////////////////////////////////////////////

#ifdef __HELIX__
#  define BF_MAX_CORES			(NUM_PROCESSORS-6)
#  define BF_ANTENNAS_PER_CORE	1
#else
#  define BF_MAX_CORES			4
#  define BF_ANTENNAS_PER_CORE	8
#endif

#define BF_ARRAY_SIZE		(BF_MAX_CORES*BF_ANTENNAS_PER_CORE)
#define BF_MAX_SOURCES		8
#define BF_FILTER_TAPS		63 // must be odd
#define BF_FFT_SIZE			512 // must be power of two
#define BF_WAVE_SPEED		(Distance(10.0))  // distance per time unit
#define BF_FREQ_CAP			(Frequency(1000)) // in Hertz
#define BF_SAMPLE_FREQ		(Frequency(2.0*BF_FREQ_CAP))
#define BF_FREQ_MIN			(Frequency(BF_FREQ_CAP/2.0))
#define BF_FREQ_BAND		(BF_FREQ_CAP-BF_FREQ_MIN)
#define BF_ANTENNA_DIST		((Distance(1.0)/BF_FREQ_CAP*BF_WAVE_SPEED)/Distance(2.0))
#define BF_SOURCE_DIST		(BF_ANTENNA_DIST*BF_ARRAY_SIZE*2.0)
#define BF_ROTATE_STEPS		72
#define BF_ROTATE_STEP		(Angle(M_PI*2.0)/Angle(BF_ROTATE_STEPS))
#define BF_ROTATE_ANIM_STEP	(Angle(M_PI*2.0)/360.0)
#define BF_TIME_STEP		(Timestamp(1.0/BF_SAMPLE_FREQ))
#define BF_TIME_STEPS		BF_FFT_SIZE
#define BF_SIN_TABLE_RES	0x2000
#define BF_EPSILON			0.00001

#ifndef max
#  define max(a,b)			((a)>(b)?(a):(b))
#endif
#ifndef min
#  define min(a,b)			((a)>(b)?(b):(a))
#endif
#ifndef barrier
#  define barrier(...)		__sync_synchronize()
#endif

#ifdef __HELIX__
#  define coreid(c)		getgpidproc((c).thread)
#else
#  define coreid(c)		(-1)
#endif

#ifndef STRINGIFY
#  define STRINGIFY(s)	STRINGIFY_(s)
#  define STRINGIFY_(s) #s
#endif

#include <typeinfo>
#define type_string(t)	(typeid(typeof(t)).name())

#define INLINE		__attribute__((always_inline))
#define PURE		__attribute__((pure))
#define INLINE_PURE	INLINE PURE

#ifdef __HELIX__
#  define pthread_cancel(pid)			pthread_kill(pid,SIGKILL)
#  define pthread_setcanceltype(...)
#  define poll_sleep()
#  define sync_type(T)		mc_prim<T>
#  define mc_flush_all()	FlushDCache()
#else
#  define poll_sleep()		pthread_yield()
#  define sync_type(T)		T volatile
#  define mc_flush(...)
#  define mc_flush_all()
#  define ATTR_DATA_ALIGNED
#endif

#ifdef DISABLE_THREADS
#  define pthread_yield(...)	// does not exist on Mac
#  define pthread_create(...)	(fprintf(stderr,"Cannot create thread in non-threaded mode\n"),abort(),EINVAL)
#endif


////////////////////////////////////////////////
// Auto-typed wrapped std functions
////////////////////////////////////////////////

#define trigonometric(fun)															\
	template <typename T> T fun##_(T a){return (T)fun((double)a);}					\
	template <> float fun##_<float>(float a){return fun##f(a);}						\
	template <> long double fun##_<long double>(long double a){return fun##l(a);}
#define trigonometric2(fun)															\
	template <typename T> T fun##_(T a,T b){return (T)fun((double)a,(double)b);}	\
	template <> float fun##_<float>(float a,float b){return fun##f(a,b);}			\
	template <> long double fun##_<long double>(long double a,long double b){return fun##l(a,b);}

trigonometric(cos)
trigonometric(sin)
trigonometric(tan)
trigonometric(acos)
trigonometric(asin)
trigonometric(atan)
trigonometric(log10)
trigonometric2(atan2)
trigonometric(sqrt)
trigonometric2(fmod)

template <typename T> T sinc_(T x){
	if(x>-BF_EPSILON && x<BF_EPSILON)
		return 1;
	else
		return sin_<T>(M_PI*x)/(M_PI*x);
}

template <typename T> T window_(T n,int M=BF_FILTER_TAPS){
	// Hamming Windowing
	return 0.54-0.46*cos_<T>(2.0*M_PI*(n+0.5)/(T)M);
}

template <typename T=float,int R=BF_SIN_TABLE_RES>
class SinLUT {
	T m_lut[R]; // holds sin(0..2*pi), which could be optimized by storing only a quarter...
public:
	SinLUT() {
		for(int s=0;s<R;s++){
			T t=(T)s/(T)R*(T)(M_PI*2.0);
			m_lut[s]=sin_<T>(t);
//			printf("sinlut %d: sin(%f)=%f vs %f\n",s,t,(double)sin_<T>(t),(double)sin(t));
		}
		mc_flush(*this);
	}
	T operator()(T t) const throw() INLINE_PURE {
		return this->sin(t);
	}
	T sin(T t) const throw() INLINE_PURE {
		int i=(int)(t*(T)(1.0/(M_PI*2.0))*(T)R)%R;
		if(i<0)i+=R;
		return m_lut[i];
	}
	T cos(T t) const throw() INLINE_PURE {
		return this->sin(t+(T)M_PI_2);
	}
};


////////////////////////////////////////////////
// Basic types
////////////////////////////////////////////////

template <typename T1,typename T2> struct is_type { enum { value = false }; };
template <typename T> struct is_type<T,T> { enum { value = true }; };

template <typename T> struct deref { typedef T type; };
template <typename T> struct deref<T&> { typedef T type; };

template <typename T>
class PrimWrap {
	T t;
public:
	typedef T type;
	PrimWrap() : t() {}
	PrimWrap(T v) : t(v) {}
	T get() const INLINE { return t; }
	T get() volatile const INLINE { return const_cast<T volatile&>(t); }
	operator T const&() const INLINE {return t;}
	operator T() volatile INLINE {return get();}
	operator T&() INLINE {return t;}
protected:
	template <typename P>
	static void prints(char const* fmt,P v,char const* padl=NULL,char const* padr=NULL) {
		printf(fmt,padl?padl:"",padr?padr:"",v);
	}
	void set(T v) INLINE { t=v;}
	void set(T v) volatile INLINE { const_cast<T volatile&>(t)=v;}
};

#define PRIM_OPS(C)														\
	C& operator=(C const& t) INLINE {this->set(t); return *this;}		\
	void operator=(C const& t) volatile INLINE { this->set(t); }		\
	C const& operator[](int i) const { return *this;}					\
	C::type operator[](int i) volatile const { return this->get();}		\
	C& operator[](int i){return *this;}									\
	static const int length=1;

class Sample : public PrimWrap<float> {
public:
	typedef float T;
private:
	typedef PrimWrap<T> base;
public:
	Sample() : base() {}
	Sample(T v) : base(v) {}
	void print(char const* padl=NULL,char const* padr=NULL) const {
		prints("%s%f%s",(double)get(),padl,padr);}

	PRIM_OPS(Sample)
};

static SinLUT<float> const sin_lut;
#define cos_lut(f) (sin_lut.cos(f))

class Frequency : public PrimWrap<float> {
public:
	typedef float T;
private:
	typedef PrimWrap<T> base;
public:
	Frequency() : base() {}
	Frequency(T v) : base(v) {}
	void print(char const* padl=NULL,char const* padr=NULL) const {
		prints("%s%f Hz%s",(double)get(),padl,padr);}

	PRIM_OPS(Frequency)
};

class Timestamp : public PrimWrap<float> {
public:
	typedef float T;
private:
	typedef PrimWrap<T> base;
public:
	Timestamp() : base() {}
	Timestamp(T v) : base(v) {}
	void print(char const* padl=NULL,char const* padr=NULL) const {
		prints("%s%f%s",(double)get(),padl,padr);}

	PRIM_OPS(Timestamp)
};

class Distance : public PrimWrap<float> {
public:
	typedef float T;
private:
	typedef PrimWrap<T> base;
public:
	Distance() : base() {}
	Distance(T v) : base(v) {}
	void print(char const* padl=NULL,char const* padr=NULL) const {
		prints("%s%f%s",(double)get(),padl,padr);}
	
	PRIM_OPS(Distance)
};

// radians
class Angle : public PrimWrap<float> {
public:
	typedef float T;
private:
	typedef PrimWrap<T> base;
public:
	Angle() : base() {}
	Angle(T v) : base(v) {}
	void print(char const* padl=NULL,char const* padr=NULL) const {
		prints("%s%f%s",(double)get()/M_PI,padl,padr);}

	PRIM_OPS(Angle)
};

// The world is a xy-plane, visualized as having coordinate (-inf,inf) as top-left corner.
class Coordinate {
	Distance m_x,m_y;
public:
	Coordinate() : m_x(), m_y() {}
	Coordinate(Distance x,Distance y) : m_x(x),m_y(y) {}
	Coordinate(Angle a,Distance dist,Coordinate const& offset=Coordinate()) : m_x(cos_lut(a)*dist+offset.x()), m_y(sin_lut(a)*dist+offset.y()) {}
	Coordinate operator+(Coordinate const& o) const {
		return Coordinate(Distance(m_x+o.x()),Distance(m_y+o.y()));}
	Coordinate operator-(Coordinate const& o) const {
		return Coordinate(Distance(m_x-o.x()),Distance(m_y-o.y()));}
	Coordinate operator*(Distance::type s) const {
		return Coordinate(Distance(m_x*s),Distance(m_y*s));}
	Coordinate& operator+=(Coordinate const& o) {
		m_x+=o.x();m_y+=o.y();
		return *this;}
	Coordinate& operator-=(Coordinate const& o) {
		m_x-=o.x();m_y-=o.y();
		return *this;}
	Coordinate& print(char const* padl=NULL,char const* padr=NULL) {
		const_cast<Coordinate const*>(this)->print(padl,padr);
		return *this;
	}
	Coordinate const& print(char const* padl=NULL,char const* padr=NULL) const {
		printf("%s(%.3f,%.3f)%s",padl?padl:"",(double)m_x,(double)m_y,padr?padr:"");
		return *this;
	}
	Distance x() const { return m_x; }
	Distance y() const { return m_y; }

	// polar notation
	Angle angle() const {
		return Angle(atan2_(y(),x()));
	}
	Distance abs() const {
		return abs(Coordinate());
	}
	Distance abs(Coordinate const& o) const {
		Distance dx=m_x-o.x(),dy=m_y-o.y();
		return sqrt_(dx*dx+dy*dy);
	}
};

class Samples {
	Sample m_antenna[BF_ARRAY_SIZE];
public:
	int length;

	Samples() : m_antenna(), length(BF_ARRAY_SIZE) {}
	Samples(Sample const* a,int n) : m_antenna(), length(n) {
		for(int i=0;i<n;i++)
			m_antenna[i]=a[i];
	}
	Sample& operator [](int i){return m_antenna[i];}
	Sample operator [](int i) const {return m_antenna[i];}
	Samples& operator+=(Samples const& o){
		for(int i=0;i<BF_ARRAY_SIZE;i++)
			m_antenna[i]+=o[i];
		return *this;
	}
	Samples& print(char const* padl=NULL,char const* padr=NULL) {
		const_cast<Samples const*>(this)->print(padl,padr);
		return *this;
	}
	Samples const& print(char const* padl=NULL,char const* padr=NULL) const {
		if(padl)printf("%s",padl);
		for(int i=0;i<BF_ARRAY_SIZE;i++)
			printf(" %7.3f",(double)m_antenna[i]);
		if(padr)printf("%s",padr);
		return *this;
	}
};


////////////////////////////////////////////////
// Beamformer types
////////////////////////////////////////////////

class Source;
class Environment;

class Array {
	Angle m_dir;
	Angle m_steering;
	int m_antennas;
	int m_taps;

	Distance c_antenna_distance[BF_ARRAY_SIZE][BF_MAX_SOURCES];
public:
	Array() : m_dir((Angle)0), m_steering((Angle)0), m_antennas(min(5,BF_ARRAY_SIZE)), m_taps(min(11,BF_FILTER_TAPS)|1), c_antenna_distance() {}

	int antennas() const {mc_flush(m_antennas); return m_antennas; }
	void addAntenna() {
		if(m_antennas<BF_ARRAY_SIZE){
			m_antennas++;
			UpdateAntennaDistances();
		}
	}
	void dropAntenna() {
		if(m_antennas>1){
			m_antennas--;
			UpdateAntennaDistances();
		}
	}

	int taps() const { mc_flush(m_taps); return m_taps; }
	void addTaps() { if(m_taps+2<=BF_FILTER_TAPS)m_taps+=2; }
	void dropTaps() { if(m_taps>1)m_taps-=2; }

	static Distance AntennaSpacing() { return BF_ANTENNA_DIST; }
	Coordinate AntennaPosition(int index) const {
		return Coordinate(Angle(turned()+M_PI_2),(Distance(antennas()-1)/2.0-Distance(index))*AntennaSpacing(),position());
	}
	Distance AntennaDistance(int index,int source_id) const {
		Distance const& d=c_antenna_distance[index][source_id];
		mc_flush(d);
		return d;
	}
	Distance AntennaDistance(int index,Coordinate source) const {
		// distance from source to the center of the array
		Distance source2center = source.abs(position());
		// angle of source relative to the array; 0 means that source is exactly in front of/perpendicular to the array
		Angle source2array = source.angle()-turned();
		// absolute position of the requested antenna
		Coordinate a=AntennaPosition(index);
		// difference in path length from source to array center and source to antenna
		Distance pathlendiff = sin_lut(source2array)*a.abs(position());
		if(index>=antennas()/2)
			return source2center+pathlendiff;
		else
			return source2center-pathlendiff;
	}
	Timestamp AntennaDelay(int index) const {
		return AntennaDelay(index,steered());
	}
	Timestamp AntennaDelay(int index,Angle steering) const {
		// absolute position of the requested antenna
		Coordinate a=AntennaPosition(index);
		// difference in path length from source to array center and source to antenna
		Distance pathlendiff = sin_lut(steering)*a.abs(position());
		// time difference by path length
		Timestamp t=pathlendiff/BF_WAVE_SPEED;
		if(index>=antennas()/2)
			return t;
		else
			return -t;
	}
	Array& print(){
		const_cast<Array const*>(this)->print();
		return *this;
	}
	Array const& print(char const* padr=NULL,char const* padl=NULL) const {
		printf("%sdir=%.3f, array={ ",padr?padr:"",(double)dir());
		for(int i=0;i<antennas();i++)
			AntennaPosition(i).print(""," ");
		printf("}%s",padl?padl:"\n");
		return *this;
	}
	Array& turnleft(Angle a=BF_ROTATE_STEP){
		return turn(m_dir+a);
	}
	Array& turnright(Angle a=BF_ROTATE_STEP){
		return turnleft(-a);
	}
	Array& turn(Angle a){
		m_dir=fmod_((Angle::type)a,(Angle::type)(M_PI*2.0));
		UpdateAntennaDistances();
		return *this;
	}
	Array& steerleft(Angle a=BF_ROTATE_STEP){
		return steer(m_steering+a);
	}
	Array& steerright(Angle a=BF_ROTATE_STEP){
		return steerleft(-a);
	}
	Array& steer(Angle a){
		m_steering=fmod_((Angle::type)a,(Angle::type)(M_PI*2.0));
		return *this;
	}
	operator Angle() const { return dir();}
	Angle dir() const { return turned()+steered();}
	Angle turned() const { mc_flush(m_dir); return m_dir;}
	Angle steered() const { mc_flush(m_steering); return m_steering;}

	// Array is by default at positioned at coordinate (0,0)
	operator Coordinate() const {return position();}
	Coordinate position() const {return Coordinate();}

	Sample pattern(Angle a,Frequency f) const {
		Sample realSum = 0;
		Sample imagSum = 0;

		Timestamp delay1 __attribute__((unused)) =
			(sin_lut(a)+sin_lut(steered()))*(Timestamp::type)BF_ANTENNA_DIST/BF_WAVE_SPEED;
		for(int as=antennas(),i=0;i<as;i++){
#ifdef __MICROBLAZE__
			// optimized version
			Timestamp delay = delay1*(Timestamp::type)i;
#else
			// actual/perfect version
			Timestamp delay = AntennaDelay(i,a)+AntennaDelay(i);
#endif

			// add simulation wave
			realSum += cos_lut((float)(2.0 * M_PI) * f * delay);
			imagSum += sin_lut((float)(2.0 * M_PI) * f * delay);
		}

		Sample output = sqrt_<Sample::type>(realSum*realSum+imagSum*imagSum)/antennas();
		Sample logOutput = 20 * log10_(output);
		if(logOutput < -50)
			logOutput = -50;
		return logOutput;
	}
protected:
	void UpdateAntennaDistances();
	friend class Environment;
};

// the array
static Array array_ ATTR_DATA_ALIGNED =Array();
static Array const& array=const_cast<Array const&>(array_);

// Sources generate plane waves, which losslessly travel towards array
class Source {
	static int num_sources;
	Coordinate m_pos;
	Frequency m_freq;
	int const m_id;
public:
	Source(Coordinate const& pos,Frequency freq) : m_pos(pos), m_freq(freq), m_id(num_sources++) {}
	Coordinate const& position() const { return m_pos;}
	Frequency frequency() const { return m_freq; }
	Sample gen(Timestamp t) const {
		return sin_lut(t*frequency()*2.0*M_PI);
	}
	Sample sample(Array const& a,int antenna,Timestamp t) const {
		Distance d=a.AntennaDistance(antenna,id());
		return gen(t-d/BF_WAVE_SPEED);
	}
	Samples samples(Array const& a,Timestamp t) const {
		Samples s;
		s.length=a.antennas();
		for(int i=0;i<array.antennas();i++)
			s[i]=sample(a,i,t);
		return s;
	}
	void gnuplot(int index=-1,char const* prefix="plot",FILE* f=stdout) const {
		if(index==-1)index=id();
		fprintf(f,"%s sin(2*pi*x*%f) title \"source %d, %.0f Hz\" with lines lc %d",prefix,(double)m_freq,index,(double)m_freq,1+index);
	}
	int id() const { return m_id; }
};

int Source::num_sources=0;

// the sources
#define BF_SOURCE_FREQ(f)	(Frequency((BF_FREQ_CAP-BF_FREQ_MIN)*f+BF_FREQ_MIN))
static Source const sources[] ATTR_DATA_ALIGNED ={
	Source(Coordinate((Angle)(2.0*M_PI/360.0*35.0),BF_SOURCE_DIST),BF_SOURCE_FREQ(0.93)),
	Source(Coordinate((Angle)(2.0*M_PI/360.0*20.0),BF_SOURCE_DIST),BF_SOURCE_FREQ(0.5)),
	Source(Coordinate((Angle)(2.0*M_PI/360.0*100.0),BF_SOURCE_DIST),BF_SOURCE_FREQ(0.25)),
	Source(Coordinate((Angle)(2.0*M_PI/360.0*60.0),BF_SOURCE_DIST),BF_SOURCE_FREQ(0.1)),
	Source(Coordinate((Angle)(2.0*M_PI/360.0*135.0),BF_SOURCE_DIST),BF_SOURCE_FREQ(0.8)),
	Source(Coordinate((Angle)(2.0*M_PI/360.0*10.0),BF_SOURCE_DIST),BF_SOURCE_FREQ(0.6))
};
static int const BF_SOURCES = sizeof(sources)/sizeof(Source);
static char max_source_check[BF_MAX_SOURCES-BF_SOURCES] __attribute__((unused)) = {};
	
class Environment {
	int m_sources;
	Source const * m_source;
public:
	Environment() : m_sources(BF_SOURCES/2), m_source(::sources) {
		array_.UpdateAntennaDistances();
	}
	int sources() const { mc_flush(m_sources); return m_sources; }
	Source const& source(int i) const { return m_source[i]; }
	void addSource() {
		if(m_sources<BF_SOURCES){
			m_sources++;
			array_.UpdateAntennaDistances();
		}
	}
	void dropSource() {
		if(m_sources>1){
			m_sources--;
			array_.UpdateAntennaDistances();
		}
	}
};

// the environment
static Environment env_ ATTR_DATA_ALIGNED =Environment();
static Environment const& env=const_cast<Environment const&>(env_);

void Array::UpdateAntennaDistances(){
	for(int s=0;s<env.sources();s++)
		for(int a=0;a<antennas();a++)
			c_antenna_distance[a][s]=AntennaDistance(a,env.source(s).position());
}


////////////////////////////////////////////////
// Application I/O
////////////////////////////////////////////////

class Timer {
public:
	typedef uint32_t interval_t;
private:
	struct timeval m_start;
	interval_t m_last;
	bool m_running;
	char const* const m_desc;
public:
	Timer(char const* desc=NULL) : m_start(), m_running(false), m_desc(desc) {
		start();
	}
	~Timer(){
		if(m_running)
			stop().print();
	}
	void start(){
		m_running=true;
		gettimeofday(&m_start,NULL);
	}
	Timer& stop(){
		struct timeval end;
		gettimeofday(&end,NULL);

		if(!m_running)
			return *this;
		m_running=false;

		m_last=(end.tv_sec-m_start.tv_sec)*1000+(end.tv_usec-m_start.tv_usec)/1000;
		return *this;
	}
	interval_t get() const {
		return m_last;
	}
	operator interval_t() const {return get();}
	void print(char const* padr="\n") const {
		print(get(),m_desc,padr);
	}
	static void print(uint32_t ms,char const* desc=NULL,char const* padr="\n"){
		printf("%s%s%.3f s%s",desc?desc:"",desc?": ":"",(double)ms/1000.0,padr?padr:"");
	}
};

class Output {
public:
	Output() {}
	virtual ~Output() {}
	virtual void start(){
		array.print("# ");
		printf("# FFT output:\n");
	}
	virtual void FFTdata(Frequency f,Sample fft){
		if(fft>0.5)
			printf("%f %f\n",(double)f,(double)fft);
	}
	virtual void finish(bool pause=true){
		printf("# done\n");
	}
	virtual void flush(){}
	virtual void stats(){}
};

#ifdef HAVE_GNUPLOT
class Gnuplot : public Output {
	FILE* m_f;
	bool m_interactive;
public:
	Gnuplot(char const* file="bf.gp",bool interactive=false) : Output(), m_f(), m_interactive(interactive) {
		m_f=fopen(file,"w");
		if(!m_f)
			fprintf(stderr,"Cannot open gnuplot file: %s\n",strerror(errno));
	}
	virtual ~Gnuplot(){
		if(m_f){
			if(m_interactive)
				fprintf(m_f,"quit\n");
			fclose(m_f);
		}
	}
	virtual void start(){
		if(!m_f)return;
		
		fprintf(m_f,"reset\n");
		fprintf(m_f,"set multiplot layout 2,2\n");
		fprintf(m_f,"set angles radians\n");

		// plot map
		fprintf(m_f,"set polar\n");
		fprintf(m_f,"set grid polar\n");
		fprintf(m_f,"set size square\n");
		fprintf(m_f,"set xrange[-%f:%f]\n",1.1*BF_SOURCE_DIST,1.1*BF_SOURCE_DIST);
		fprintf(m_f,"set yrange[-%f:%f]\n",1.1*BF_SOURCE_DIST,1.1*BF_SOURCE_DIST);
		fprintf(m_f,"plot ");
		for(int i=0;i<env.sources();i++)
			fprintf(m_f,"\"-\" notitle with points lc %d, ",1+i);
		fprintf(m_f,"\"-\" notitle with points lc 0\n");
		for(int i=0;i<env.sources();i++){
			Coordinate c=sources[i].position();
			fprintf(m_f,"%f %f\ne\n",(double)c.angle(),(double)c.abs());
		}
		for(int i=0;i<array.antennas();i++){
			Coordinate c=array.AntennaPosition(i);
			fprintf(m_f,"%f %f\n",(double)c.angle(),(double)c.abs());
		}
		Coordinate adir=Coordinate(array.dir(),0.5*BF_SOURCE_DIST,array.position());
		fprintf(m_f,"%f %f\n",(double)adir.angle(),(double)adir.abs());
		fprintf(m_f,"e\n");

		fprintf(m_f,"unset polar\n");
		fprintf(m_f,"set size noratio\n");
		
		// plot beam pattern per source frequency
		fprintf(m_f,"set yrange[-50:0]\n");
		fprintf(m_f,"set xrange[-90:90]\n");
		fprintf(m_f,"set xlabel \"angle (degrees)\"\n");
		fprintf(m_f,"set ylabel \"gain (dB)\"\n");
		fprintf(m_f,"plot ");
		for(int s=0;s<env.sources();s++)
			fprintf(m_f,"	\"-\" using 1:2 title \"beam pattern %.0f Hz\" with lines lc %d%s",(double)sources[s].frequency(),1+s,s==env.sources()-1?"\n":", ");
		for(int s=0;s<env.sources();s++){
			for(Angle a=-M_PI_2;a<=M_PI_2;a+=M_PI/180.0/2.0){
				Sample gain=array.pattern(a,sources[s].frequency());
				fprintf(m_f,"%f %f\n",a/M_PI*180.0,(double)gain);
			}
			fprintf(m_f,"e\n");
		}

		// plot the different sources
		fprintf(m_f,"set xlabel \"time\"\n");
		fprintf(m_f,"set ylabel \"amplitude\"\n");
		fprintf(m_f,"set samples 1000\n");
		fprintf(m_f,"set yrange[-2:2]\n");
		fprintf(m_f,"set xrange[0:%f]\n",2.0/BF_FREQ_MIN);
		fprintf(m_f,"plot ");
		for(int i=0;i<env.sources();i++)
			sources[i].gnuplot(i,i==0?"":",",m_f);
		fprintf(m_f,"\n");
	
		// plot FFT output
		fprintf(m_f,"set xlabel \"frequency (Hz)\"\n");
		fprintf(m_f,"set ylabel \"FFT\"\n");
		fprintf(m_f,"set yrange [0:1]\n");
		fprintf(m_f,"set xrange [0:*]\n");
		fprintf(m_f,"plot \"-\" using 1:2 notitle with lines lc 0\n");
	}
	virtual void FFTdata(Frequency f,Sample fft){
		if(!m_f)return;
		fprintf(m_f,"%f %f\n",(double)f,(double)fft);
	}
	virtual void finish(bool pause){
		if(!m_f)return;
		fprintf(m_f,"e\n");
		fprintf(m_f,"unset multiplot\n");
		if(!m_interactive)
			fprintf(m_f,"pause 0.02\n");
		fflush(m_f);
		if(m_interactive&&pause)
			usleep(10000);
	}
};
#endif // HAVE_GNUPLOT

#ifdef __MICROBLAZE__
static const color_t c_sources[]={
	PIX_CONST(0xff,0x00,0x00),
	PIX_CONST(0x00,0xff,0x80),
	PIX_CONST(0x80,0x40,0xff),
	PIX_CONST(0xff,0xff,0x00),
	PIX_CONST(0xff,0x80,0x00),
	PIX_CONST(0xff,0x00,0xff),
	PIX_CONST(0xff,0x80,0xff),
	PIX_CONST(0x80,0xff,0x80)
};

class HelixRender : public Output {
	se_t<1> m_se_draw ATTR_DATA_ALIGNED;
	se_t<1> m_se_flip ATTR_DATA_ALIGNED;
	void* m_framemem;
	dvi_frame* m_frame;
	int m_antennas ATTR_DATA_ALIGNED;
	Timer m_fps ATTR_DATA_ALIGNED;
public:
#define c_background	PIX_CONST(0,0x20,0x40)
#define c_border		PIX_CONST(0xb0,0xb0,0xb0)
#define c_fftbar		PIX_CONST(0x80,0x80,0x80)
#define c_source(i)		(c_sources[(i)%(sizeof(c_sources)/sizeof(c_sources[0]))])
#define c_antenna		PIX_CONST(0,0xff,0)
#define c_pattern		PIX_CONST(0,0xff,0xff)
#define c_text			PIX_CONST(0xff,0xff,0xff)
#define c_node			PIX_CONST(0x40,0xe0,0xb0)
#define c_nodetext		PIX_CONST(0x00,0x30,0x10)
#define c_connect		PIX_CONST(0x30,0xc0,0x90)
#define c_box			PIX_CONST(0x60,0x60,0x60)

	HelixRender() : Output(), m_framemem(), m_frame(), m_antennas(-1), m_fps() {
		render_init(1);
		if(!(m_framemem=render_alloc_buf(2,&m_frame)))
			printf("Cannot allocate framebuffer\n");

		// init both buffers
		for(int i=0;i<2;i++){
			// background
			fillrect(0,0,DVI_WIDTH,DVI_HEIGHT,c_background);
			drawMap(true);
			drawPattern(true);
			drawTaskgraph(true);
			drawFFTCanvas(true);
			render_flip_buffer();
		}
		mc_flush(*this);

		// start situation renderer
		pthread_t p;
		VERIFY(pthread_create(&p,NULL,(void*(*)(void*))drawProcess,this),0);
		se_create_pt(m_se_draw,p);
		se_create_pt(m_se_flip,pthread_self());

		prf_mem_snapshot();
	}
	virtual ~HelixRender() {
		if(m_framemem)
			sfree(m_framemem);
	}
	virtual void start(){
		se_hit(m_se_draw);
	}
	virtual void FFTdata(Frequency f,Sample fft){
//		printf("%f %f\n",(double)f,(double)fft);
		drawFFTBar(f,fft);
	}
	virtual void finish(bool pause){
		se_wait(m_se_flip);
		render_flip_buffer();
#ifndef BF_TIME_TASKS
		outbyte('.');
#endif
	}
	virtual void flush(){render_flush();}
	virtual void stats(){
		prf_mem_dump();
	}
protected:
	static void* drawProcess(HelixRender* that) __attribute__((noreturn)) {
		pthread_detach(pthread_self());
		while(true){
			pthread_yield();
			se_wait(that->m_se_draw);
#ifdef BF_TIME_TASKS
			Timer t=Timer("Draw situation");
#endif
			that->drawMap();
			that->drawTaskgraph();
			that->drawPattern();
			that->m_fps.stop();
			that->drawFPS(1.0f/((float)that->m_fps.get()*(1.0f/1000.0f)));
			se_hit(that->m_se_flip);
			that->m_fps.start();
			mc_flush(*that->m_frame);
		}
	}
	void drawMap(bool first=false,int x=20,int y=20,int w=200) {
		int cx=x+w/2,cy=y+w/2;
		fillrect(x+1,y+1,x+w-1,y+w-1,c_background);
//		if(first)
//			drawrect(x,y,x+w,y+w,c_border);
		float pxperdistunit=(float)w/2.0/BF_SOURCE_DIST;
		// sources
		for(int i=0;i<env.sources();i++){
			Source const& s=env.source(i);
			Coordinate p=s.position()*pxperdistunit;
			drawrect(cx+(int)p.x()-1,cy-(int)p.y()-1,cx+(int)p.x()+1,cy-(int)p.y()+1,c_source(i));
		}
		// array
		for(int i=0;i<array.antennas();i++){
			Coordinate p=array.AntennaPosition(i)*pxperdistunit*((Distance)BF_ARRAY_SIZE/array.antennas());
			drawrect(cx+(int)p.x()-1,cy-(int)p.y()-1,cx+(int)p.x()+1,cy-(int)p.y()+1,c_antenna);
		}
		Coordinate adir=Coordinate(array.dir(),0.5*BF_SOURCE_DIST,array.position())*pxperdistunit;
		Coordinate apos=array.position()*pxperdistunit;
		drawline(cx+apos.x(),cy-apos.y(),cx+adir.x(),cy-adir.y(),c_antenna);

		char str[64];
		snprintf(str,sizeof(str),"n=%d taps=%d dir=%d%+d deg.",array.antennas(),array.taps(),(int)(array.turned()/M_PI*180.0),(int)(array.steered()/M_PI*180.0));
		drawstring(x,y+w-20,str,c_text);
	}
	void drawPattern(bool first=false,int x=300,int y=35,int w=300,int h=180) {
		fillrect(x+1,y+1,x+w-1,y+h-1,c_background);
		if(first){
			drawrect(x,y,x+w,y+h,c_border);
			drawstring(x-25,y+4,		"  0",c_text);
			drawstring(x-25,y+4+(h/5),	"-10",c_text);
			drawstring(x-25,y+4+(h/5*2),"-20",c_text);
			drawstring(x-25,y+4+(h/5*3),"-30",c_text);
			drawstring(x-25,y+4+(h/5*4),"-40",c_text);
			drawstring(x-25,y+4+h,		"-50",c_text);
			drawstring(x-25,y-15,"gain (dB)",c_text);

			drawstring(x-10,		y+h+12, " 90",c_text);
			drawstring(x-10+(w/4),	y+h+12,	" 45",c_text);
			drawstring(x-10+(w/2),	y+h+12,	" 0 ",c_text);
			drawstring(x-10+(w/4*3),y+h+12,	"-45",c_text);
			drawstring(x-10+w,		y+h+12,	"-90",c_text);
			drawstring(x-40+(w/2),	y+h+25,	"angle (degrees)",c_text);
		}
#if 0
		// enable to draw the beampattern of all sources
		for(int src=0;src<env.sources();src++){
#else
		{
			int src=0;
#endif
			int sx,sy,px,py;
			for(int i=0;i<w-2;i++){
				Sample s=array.pattern((float)i/(float)(w-2)*(float)M_PI-(float)M_PI_2,env.source(src).frequency());
				sx=x+i+1;
				sy=y+s/-50.0f*(h-2)+1;
				if(i>0)drawline(sx,sy,px,py,c_source(src));
				px=sx;
				py=sy;
			}
			Angle srcpos=fmod_<Angle::type>((env.source(src).position().angle()-array.turned()),M_PI*2.0);
			if(srcpos<0)
				srcpos+=M_PI*2.0;
			if(srcpos>(float)(M_PI_2*3.0f))
				srcpos=srcpos-(Angle::type)(M_PI*2.0);
			else if(srcpos>(float)M_PI_2)
				srcpos=-srcpos+(Angle::type)(M_PI);

			int srcx=x+w/2-(srcpos/(float)M_PI_2*((w-2)/2));
			for(int srcy=1;srcy<h-2;srcy+=2)
				set_pixel_col(srcx,y+srcy,c_source(src));
//			drawline(srcx,y+1,srcx,y+h-2,c_source(src));
		}
	}
	void drawTaskgraph(bool first=false,int x= 10,int y=260,int w=250,int h=180){
		if(!first&&array.antennas()==m_antennas)
			return;
		int antennas=array.antennas();
		if(m_antennas==-antennas)
			// second buffer draw
			m_antennas=antennas;
		else
			// first buffer draw
			m_antennas=-antennas;

		fillrect(x,y,x+w,y+h,c_background);

		int const node_w=18;
		int const node_d=((w-4)-7*node_w)/6;
		int const node_s=(h-32-4*node_w)/3;
		int const pipe_y=(h-node_w)/2+y+6;
		int node_x=x+2;
		
		drawrect(x,y,x+node_w*2+node_d*3/2+1,y+h,c_box);
		drawstring(x+2,y+h+12,"generate",c_text);
		drawrect(x+node_w*2+node_d*3/2+3,y,x+node_w*5+node_d*9/2+1,y+h,c_box);
		drawstring(x+node_w*2+node_d*3/2+5,y+h+12,"beamformer",c_text);
		drawrect(x+node_w*5+node_d*9/2+3,y,x+node_w*7+node_d*6+4,y+h,c_box);
		drawstring(x+node_w*5+node_d*4+2,y+h+12,"visualization",c_text);

		struct { int x,y; } node_save1,node_save2,node_prev,node_last;
#define connectnode(a,b) drawline((a).x+node_w+1,(a).y+node_w/2,(b).x-1,(b).y+node_w/2,c_connect)
#define drawnode(nx,ny,c)														\
	do{																			\
		node_prev=node_last; node_last.x=(nx); node_last.y=(ny);				\
		fillrect((nx),(ny),(nx)+node_w,(ny)+node_w,c_node);						\
		char c_[8];																\
		snprintf(c_,sizeof(c_),"%d",(c));										\
		drawstring(node_last.x+node_w/2-((c)>9?7:3),node_last.y+node_w/2+4,c_,c_nodetext);\
	}while(0)
#define drawnode2(p,nx,ny,c)													\
	do{																			\
		drawnode(nx,ny,c);														\
		connectnode(p,node_last);												\
	}while(0)

		drawnode(node_x,pipe_y,0);
		node_save1=node_last;
		drawstring(node_x,y+12,"time",c_text);
		node_x+=node_w+node_d;
		
		drawnode(node_x+(node_w+node_d)*2,pipe_y,antennas+1);
		drawstring(node_x+(node_w+node_d)*2,y+12,"add",c_text);
		node_save2=node_last;

		int ant=1;
		switch(antennas){
		case 3:
			drawnode2(node_save1,node_x,pipe_y-node_w-node_s,ant);
			drawnode2(node_prev,node_x+node_w+node_d,pipe_y-node_w-node_s,ant);
			connectnode(node_last,node_save2);
			drawnode2(node_save1,node_x,pipe_y+node_w+node_s,ant+2);
			drawnode2(node_prev,node_x+node_w+node_d,pipe_y+node_w+node_s,ant+2);
			connectnode(node_last,node_save2);
			ant++;
		case 1:
			drawnode2(node_save1,node_x,pipe_y,ant);
			drawnode2(node_prev,node_x+node_w+node_d,pipe_y,ant);
			connectnode(node_last,node_save2);
			break;
		default:
		case 4:
			drawnode2(node_save1,node_x,pipe_y-(node_w+node_s)*3/2,ant);
			drawnode2(node_prev,node_x+node_w+node_d,pipe_y-(node_w+node_s)*3/2,ant);
			connectnode(node_last,node_save2);
			drawnode2(node_save1,node_x,pipe_y+(node_w+node_s)*3/2,antennas);
			drawnode2(node_prev,node_x+node_w+node_d,pipe_y+(node_w+node_s)*3/2,antennas);
			connectnode(node_last,node_save2);
			ant++;
		case 2:
			drawnode2(node_save1,node_x,pipe_y-(node_w+node_s)/2,ant);
			drawnode2(node_prev,node_x+node_w+node_d,pipe_y-(node_w+node_s)/2,ant);
			connectnode(node_last,node_save2);
			if(antennas>4){
				char str[16];
				snprintf(str,sizeof(str),"%d more...",antennas-3);
				drawstring(node_x,pipe_y+8+(node_w+node_s)/2,str,c_text);
			}else{
				drawnode2(node_save1,node_x,pipe_y+(node_w+node_s)/2,ant+1);
				drawnode2(node_prev,node_x+node_w+node_d,pipe_y+(node_w+node_s)/2,ant+1);
				connectnode(node_last,node_save2);
			}
		}
		ant=antennas+2;
		drawstring(node_x,y+12,"sin",c_text);
		drawstring(node_x+node_w+node_d,y+12,"FIR",c_text);
		node_x+=(node_w+node_d)*3;
		
		drawnode2(node_save2,node_x,pipe_y,ant);
		drawstring(node_x-8,y+12,"norm.",c_text);
		node_x+=node_w+node_d;
		ant++;
		
		drawnode2(node_prev,node_x,pipe_y,ant);
		drawstring(node_x,y+12,"FFT",c_text);
		node_x+=node_w+node_d;
		
		drawnode2(node_prev,node_x,pipe_y,ant);
		drawstring(node_x-9,y+12,"show",c_text);
#undef drawnode
#undef drawnode2
#undef connectnode
 
	}
	void drawFFTCanvas(bool first=false,int x=300,int y=260,int w=300,int h=180) {
		if(first){
			drawrect(x,y,x+w,y+h,c_border);
			drawstring(x-25,y+h/2,"FFT",c_text);
			drawstring(x+w/2-40,y+h+25,"frequency (Hz)",c_text);
			for(int s=0;s<BF_SOURCES;s++){
				Frequency f=env.source(s).frequency();
				char fstr[16];
				snprintf(fstr,sizeof(fstr),"%3d",(int)f);
				drawstring(x+(w-2)*((f-BF_FREQ_MIN)/BF_FREQ_BAND)-10,y+h+12,fstr,c_text);
			}
		}
	}
	void drawFFTBar(Frequency f,Sample fft,int x=300,int y=260,int w=300,int h=180) {
		if(f>BF_FREQ_MIN){
#if 1		// enable for log output
			fft=20*log10_(fft);
			if(fft<-50.0f)fft=-50.0f;
			fft=(fft+50.0f)*(1.0f/50.0f);
#endif

			float pixperhz=(float)(w-3)/BF_FREQ_BAND;
			float hzperbar=BF_FREQ_CAP/((float)BF_FFT_SIZE/2);
			int barw=hzperbar*pixperhz/(BF_FREQ_BAND/BF_FREQ_CAP);
			int barh=fft*h;
			color_t col=c_fftbar;
			for(int i=0;i<env.sources();i++){
				Frequency sf=env.source(i).frequency();
				if(sf-hzperbar*1.1<f && f<sf+hzperbar*1.1)
					col=c_source(i);
			}
			if(barh>h-2)barh=h-2;
			// clear bar
			int bx=(float)x+pixperhz*(f-BF_FREQ_MIN)+1;
			fillrect(bx,y+1,bx+barw,y+h-barh-1,c_background);
			// draw bar
			fillrect(bx,y+h-barh-1,bx+barw,y+h-1,col);
		}
	}
	void drawFPS(float fps,int x=2,int y=478){
		fillrect(x,y-10,x+100,y,c_background);
		char str[32];
		snprintf(str,sizeof(str),"%5.2f fps",fps);
		drawstring(x,y,str,c_text);
	}
};

#endif //__MICROBLAZE__

// the currently selected output method
static Output* output ATTR_DATA_ALIGNED;

class Input;
// the currently selected input method
static Input* input ATTR_DATA_ALIGNED;

class Input {
public:
	typedef enum {
		NOP, REDRAW,
		TURN_LEFT, TURN_RIGHT, TURN_ANIM,
		STEER_LEFT, STEER_RIGHT, STEER_ANIM,
		STEER_SOURCE0, STEER_SOURCE1, STEER_SOURCE2, STEER_SOURCE3, STEER_SOURCE4, STEER_SOURCE5, STEER_SOURCE6, STEER_SOURCE7, STEER_SOURCE8, STEER_SOURCE9,
		ROTATE_ANIM,
		ADD_SOURCE, DROP_SOURCE,
		ADD_ANTENNA, DROP_ANTENNA,
		ADD_TAPS, DROP_TAPS,
		STATS, QUIT } input_t;
private:
	input_t m_next;
	input_t m_default;
public:
	
	Input(input_t def=NOP) : m_next(REDRAW), m_default(def) {
		struct sigaction sa = {};
		sa.sa_handler=sighandler;
		sigaction(SIGINT,&sa,NULL);
		sigaction(SIGTERM,&sa,NULL);
		sigaction(SIGQUIT,&sa,NULL);
		sigaction(SIGUSR1,&sa,NULL);
		sigaction(SIGUSR2,&sa,NULL);
	}
	virtual ~Input() {}
	virtual input_t poll() {
		input_t res=m_next;
		m_next=m_default;
		return res;
	}
	virtual void print() const {
		float rot=BF_ROTATE_STEP/(M_PI)*180.0;
		fprintf(stderr,"\nKeys to control beamformer:\n");
		fprintf(stderr,"  right arrow    steer array %.1f degree clockwise\n",rot);
		fprintf(stderr,"  left arrow     steer array %.1f degree counterclockwise\n",rot);
		fprintf(stderr,"  down arrow     turn array %.1f degree clockwise\n",rot);
		fprintf(stderr,"  up arrow       turn array %.1f degree counterclockwise\n",rot);
		fprintf(stderr,"  insert         add source (max %d)\n",BF_MAX_SOURCES);
		fprintf(stderr,"  delete         remove source\n");
		fprintf(stderr,"  1 ... %d        steer towards source\n",(min(9,BF_MAX_SOURCES)));
		fprintf(stderr,"  s              automatically and incremently steer counterclockwise\n");
		fprintf(stderr,"  t              automatically and incremently turn counterclockwise\n");
		fprintf(stderr,"  r              automatically and incremently turn counterclockwise, keeping steered in same direction\n");
		fprintf(stderr,"  (numpad) +     add antenna (max %d)\n",BF_ARRAY_SIZE);
		fprintf(stderr,"  (numpad) -     remove antenna\n");
		fprintf(stderr,"  (numpad) *     increase FIR (antenna delay) filter taps (max %d)\n",BF_FILTER_TAPS);
		fprintf(stderr,"  (numpad) /     decrease FIR (antenna delay) filter taps\n");
		fprintf(stderr,"  space          redraw output\n");
	}
protected:
	virtual void signal(int sig){
		setInput(QUIT);
	}
	void setInput(input_t next,input_t def=NOP){m_next=next;m_default=def;}
private:
	static void sighandler(int sig){
		if(input)
			input->signal(sig);
	}
};

class AutoRotate : public Input {
	int m_count;
public:
	AutoRotate(int count=-1) : Input(STEER_ANIM), m_count(count) {}
	virtual input_t poll() {
		if(m_count!=-1)
			if(--m_count<=0)
				setInput(QUIT);
		return Input::poll();
	}
};

#ifdef HAVE_SDL
#include <SDL.h>
class SDLKeyboard : public Input {
	typedef Input base;
	SDL_Surface *m_screen;
public:
	SDLKeyboard() : base(), m_screen(NULL) {
		if(SDL_Init(SDL_INIT_VIDEO))
			fprintf(stderr,"Cannot init SDL: %s\n",SDL_GetError());
		else if(!(m_screen=SDL_SetVideoMode(100,100,0,SDL_ANYFORMAT)))
			fprintf(stderr,"Cannot set video mode: %s\n",SDL_GetError());
		print();
	}
	virtual ~SDLKeyboard(){
		if(m_screen)
			SDL_Quit();
	}
	virtual input_t poll() {
		SDL_Event event;
		if(SDL_PollEvent(&event)){
			switch(event.type){
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym){
				case SDLK_LEFT:			setInput(STEER_LEFT);				break;
				case SDLK_RIGHT:		setInput(STEER_RIGHT);				break;
				case SDLK_UP:			setInput(TURN_LEFT);				break;
				case SDLK_DOWN:			setInput(TURN_RIGHT);				break;
				case SDLK_s:			setInput(STEER_ANIM,STEER_ANIM);	break;
				case SDLK_t:			setInput(TURN_ANIM,TURN_ANIM);		break;
				case SDLK_r:			setInput(ROTATE_ANIM,ROTATE_ANIM);	break;
				case SDLK_INSERT:		setInput(ADD_SOURCE);				break;
				case SDLK_DELETE:		setInput(DROP_SOURCE);				break;
				case SDLK_KP_PLUS:		setInput(ADD_ANTENNA);				break;
				case SDLK_KP_MINUS:		setInput(DROP_ANTENNA);				break;
				case SDLK_KP_MULTIPLY:	setInput(ADD_TAPS);					break;
				case SDLK_KP_DIVIDE:	setInput(DROP_TAPS);				break;
				case SDLK_SPACE:		setInput(REDRAW);					break;
				case SDLK_ESCAPE:		return QUIT;						break;
				case SDLK_1:			setInput(STEER_SOURCE0);			break;
				case SDLK_2:			setInput(STEER_SOURCE1);			break;
				case SDLK_3:			setInput(STEER_SOURCE2);			break;
				case SDLK_4:			setInput(STEER_SOURCE3);			break;
				case SDLK_5:			setInput(STEER_SOURCE4);			break;
				case SDLK_6:			setInput(STEER_SOURCE5);			break;
				case SDLK_7:			setInput(STEER_SOURCE6);			break;
				case SDLK_8:			setInput(STEER_SOURCE7);			break;
				case SDLK_9:			setInput(STEER_SOURCE8);			break;
				case SDLK_p:			setInput(STATS);					break;
				case SDLK_F1:			print();							break;
				default:;
				}
				break;
			case SDL_QUIT:
				return QUIT;
			default:;
			}
		}
		return Input::poll();
	}
	virtual void print() const {
		base::print();
		fprintf(stderr,"  escape         quit application\n");
	}
};
#endif // HAVE_SDL

#ifdef __MICROBLAZE__
class UARTInput : public Input {
	typedef Input base;
	char m_keys[4];
public:
	UARTInput(input_t start) : Input(), m_keys() {
		setInput(start,start);
		print();
	}
	virtual input_t poll(){
		if(kbhit()){
			m_keys[0]=m_keys[1];
			m_keys[1]=m_keys[2];
			m_keys[2]=m_keys[3];
			m_keys[3]=inbyte();
//			printf("key: 0x%hhx\n",m_keys[3]);

#define is_key(b1,b2,b3,b4)			\
	(((b1)==0||m_keys[0]==(b1)) &&	\
	 ((b2)==0||m_keys[1]==(b2)) &&	\
	 ((b3)==0||m_keys[2]==(b3)) &&	\
	 ((b4)==0||m_keys[3]==(b4)))

			if     (is_key(0x00,0x1b,'[','D'))	setInput(STEER_LEFT);
			else if(is_key(0x00,0x1b,'[','C'))	setInput(STEER_RIGHT);
			else if(is_key(0x00,0x1b,'[','A'))	setInput(TURN_LEFT);
			else if(is_key(0x00,0x1b,'[','B'))	setInput(TURN_RIGHT);
			else if(is_key(0x00,0x00,0x00,'s'))	setInput(STEER_ANIM,STEER_ANIM);
			else if(is_key(0x00,0x00,0x00,'t'))	setInput(TURN_ANIM,TURN_ANIM);
			else if(is_key(0x1b,'[','2',0x7e))	setInput(ADD_SOURCE);
			else if(is_key(0x1b,'[','3',0x7e))	setInput(DROP_SOURCE);
			else if(is_key(0x00,0x00,0x00,'+'))	setInput(ADD_ANTENNA);
			else if(is_key(0x00,0x00,0x00,'-'))	setInput(DROP_ANTENNA);
			else if(is_key(0x00,0x00,0x00,'*'))	setInput(ADD_TAPS);
			else if(is_key(0x00,0x00,0x00,'/'))	setInput(DROP_TAPS);
			else if(is_key(0x00,0x00,0x00,' '))	setInput(REDRAW);
			else if(is_key(0x00,0x00,0x00,'1'))	setInput(STEER_SOURCE0);
			else if(!is_key(0x00,0x1b,'[','2')&&is_key(0x00,0x00,0x00,'2'))	setInput(STEER_SOURCE1);
			else if(!is_key(0x00,0x1b,'[','3')&&is_key(0x00,0x00,0x00,'3'))	setInput(STEER_SOURCE2);
			else if(is_key(0x00,0x00,0x00,'4'))	setInput(STEER_SOURCE3);
			else if(is_key(0x00,0x00,0x00,'5'))	setInput(STEER_SOURCE4);
			else if(is_key(0x00,0x00,0x00,'6'))	setInput(STEER_SOURCE5);
			else if(is_key(0x00,0x00,0x00,'7'))	setInput(STEER_SOURCE6);
			else if(is_key(0x00,0x00,0x00,'8'))	setInput(STEER_SOURCE7);
			else if(is_key(0x00,0x00,0x00,'9'))	setInput(STEER_SOURCE8);
			else if(is_key(0x00,0x00,0x00,'r'))	setInput(ROTATE_ANIM,ROTATE_ANIM);
			else if(is_key(0x00,0x00,0x00,'p'))	setInput(STATS);
			else if(is_key(0x00,0x1b,'O','P'))	print();
		}
		return Input::poll();
	}
	virtual void print() const {
		base::print();
		fprintf(stderr,"  p              print memory bandwidth statistics\n\n");
	}
};

class ButtonInput : public UARTInput {
	typedef UARTInput base;
protected:
	typedef enum { bn_left=4, bn_right=2, bn_up=1, bn_down=3, bn_center=0, bn_none=-1 } button_t;
	typedef enum { state_steer, state_config, state_source, state_anim } state_t;
private:

	state_t m_state;
	button_t m_last;
	int m_last_source;

protected:
	void printState(state_t state) const {
		switch(state){
		default:
		case state_steer:
			printf("\nState 1/4: 'steer':\n");
			printf("  left     steer left\n");
			printf("  right    steer right\n");
			printf("  up       turn left\n");
			printf("  down     turn right\n");
			printf("  center   go to state 2/4 'config'\n\n");
			posleds_value(1<<IO_POSLEDS_C);
			break;
		case state_config:
			printf("\nState 2/4: 'config':\n");
			printf("  left     add FIR taps\n");
			printf("  right    drop FIR taps\n");
			printf("  up       add antenna\n");
			printf("  down     drop antenna\n");
			printf("  center   go to state 3/4 'source'\n\n");
			posleds_value(1<<IO_POSLEDS_N);
			break;
		case state_source:
			printf("\nState 3/4: 'source':\n");
			printf("  left     add source\n");
			printf("  right    drop source\n");
			printf("  up       steer to next source\n");
			printf("  down     steer to previous source\n");
			printf("  center   go to state 4/4 'animate'\n\n");
			posleds_value(1<<IO_POSLEDS_S);
			break;
		case state_anim:
			printf("\nState 4/4: 'animate':\n");
			printf("  left     animate steer\n");
			printf("  right    animate turn\n");
			printf("  up       animate turn, but steer to same direction\n");
			printf("  down     redraw\n");
			printf("  center   go to state 1/4 'steer'\n\n");
			posleds_value(1<<IO_POSLEDS_W);
			break;
		}
	}
	void button(button_t b,bool repeat){
		switch(m_state){
		case state_config:
			if(repeat)
				break;
			switch(b){
			case bn_left:	setInput(ADD_TAPS); break;
			case bn_right:	setInput(DROP_TAPS); break;
			case bn_up:		setInput(ADD_ANTENNA); break;
			case bn_down:	setInput(DROP_ANTENNA); break;
			case bn_center:	
			default:
				m_state=state_source;
				printState(m_state);
			}
			break;
		case state_source:
			if(repeat)
				break;
			switch(b){
			case bn_left:	setInput(ADD_SOURCE); break;
			case bn_right:	setInput(DROP_SOURCE); break;
			case bn_up:
				m_last_source=(m_last_source+1)%env.sources();
				setInput((input_t)(STEER_SOURCE0+m_last_source));
				break;
			case bn_down:
				m_last_source=(m_last_source-1+env.sources())%env.sources();
				setInput((input_t)(STEER_SOURCE0+m_last_source));
				break;
			case bn_center:	
			default:
				m_state=state_anim;
				printState(m_state);
			}
			break;
		case state_anim:
			if(repeat)
				break;
			switch(b){
			case bn_left:	setInput(STEER_ANIM,STEER_ANIM); break;
			case bn_right:	setInput(TURN_ANIM,TURN_ANIM); break;
			case bn_up:		setInput(ROTATE_ANIM,ROTATE_ANIM); break;
			case bn_down:	setInput(REDRAW); break;
			case bn_center:	
			default:
				m_state=state_steer;
				printState(m_state);
			}
			break;
		case state_steer:
		default:
			switch(b){
			case bn_left:	setInput(STEER_LEFT); break;
			case bn_right:	setInput(STEER_RIGHT); break;
			case bn_up:		setInput(TURN_LEFT); break;
			case bn_down:	setInput(TURN_RIGHT); break;
			case bn_center:	
			default:
				if(!repeat){
					m_state=state_config;
					printState(m_state);
				}
			}
			break;
		}
	}
public:
	ButtonInput(input_t start) : base(start), m_state(state_steer), m_last(bn_none) {
		printState(m_state);
	}
	virtual input_t poll(){
		unsigned int bs;
		if((bs=buttons_event())){
			if(bs&(1<<bn_left)){		button(bn_left,m_last==bn_left);	m_last=bn_left;		}
			else if(bs&(1<<bn_right)){	button(bn_right,m_last==bn_right);	m_last=bn_right;	}
			else if(bs&(1<<bn_up)){		button(bn_up,m_last==bn_up);		m_last=bn_up;		}
			else if(bs&(1<<bn_down)){	button(bn_down,m_last==bn_down);	m_last=bn_down;		}
			else if(bs&(1<<bn_center)){	button(bn_center,m_last==bn_center);m_last=bn_center;	}
			else fprintf(stderr,"Unknown button event: 0x%x\n",bs);
			return Input::poll();
		}else{
			m_last=bn_none;
			return base::poll();
		}
	}
	virtual void print() const {
		base::print();
		printState(m_state);
	}
};
#endif //__MICROBLAZE__


////////////////////////////////////////////////
// Function composition types
////////////////////////////////////////////////

struct context_t {
	pthread_t thread;

	context_t() : thread(0) {}
	context_t(pthread_t t) : thread(t) {}
};

template <typename T>
class SimpleQueue {
	sync_type(T) m_val;
	sync_type(bool) m_full;
	context_t m_from;
	context_t m_to;
public:
	SimpleQueue() : m_full(false), m_from(), m_to() {
#ifdef __HELIX__
		if((uintptr_t)this&(DCACHE_LINESIZE-1)){
			fprintf(stderr,"Queue %p not properly aligned to cacheline\n",this);
			abort();
		}
#endif
	} 
	void setInput(context_t const& context){m_from=context;}
	void setOutput(context_t const& context){m_to=context;}
	void finalize(){
		if(!m_from.thread||!m_to.thread){
			fprintf(stderr,"Cannot finalized unconfigured queue %p\n",this);
			abort();
		}
#ifdef BF_DEBUG
		printf("# finalized queue %p: thread 0x%x @%d -> thread 0x%x @%d\n",
			this,
			(unsigned int)m_from.thread,
			coreid(m_from),
			(unsigned int)m_to.thread,
			coreid(m_to));
#endif
		mc_flush(*this);
	}
	void push(T const& v){
		while(m_full)
			poll_sleep();
		barrier();
		m_val=v;
		barrier();
		m_full=true;
	}
	T pop(){
		while(!m_full)
			poll_sleep();
		barrier();
		T v=m_val;
		barrier();
		m_full=false;
		return v;
	}
	operator T(){return pop();}
};

#ifdef __HELIX__
template <typename T>
class FifoQueue {
	CFifo<T,CFifo<>::w> *m_ffrom;
	CFifo<T,CFifo<>::r> *m_fto;
	CFifoPtr<T> m_fifo;
	context_t m_from;
	context_t m_to;
public:
	FifoQueue() : m_ffrom(NULL), m_fto(NULL), m_from(), m_to() {}
	void setInput(context_t const& context){m_from=context;}
	void setOutput(context_t const& context){m_to=context;}
	void finalize(){
		if(!m_from.thread||!m_to.thread){
			fprintf(stderr,"Cannot finalized unconfigured queue %p\n",this);
			abort();
		}
#ifdef BF_DEBUG
		printf("# finalized queue %p: thread 0x%x @%d -> thread 0x%x @%d\n",
			this,
			(unsigned int)m_from.thread,
			coreid(m_from),
			(unsigned int)m_to.thread,
			coreid(m_to));
#endif

		m_fifo=CFifo<T>::Create(coreid(m_from),m_ffrom,coreid(m_to),m_fto,4);
		if(!m_fifo.valid()){
			printf("Cannot initialize FIFO; OOM\n");
			abort();
		}

		mc_flush(*this);
	}
	void push(T const& v){
		(*m_ffrom)+=v;
	}
	T pop(){
		return (*m_fto)--;
	}
	operator T(){return pop();}
};

#  define Queue FifoQueue
#else
#  define Queue SimpleQueue
#endif

template <typename A=int,typename R=void,bool B=false> class Composition;

class TypelessFunctor {
	int m_index;
	context_t m_context;
protected:
	TypelessFunctor() : m_index(0) {}
	virtual ~TypelessFunctor() {}
	
	void finalize(context_t const& context,int index=0){
		m_context=context;
		m_index=index;
	}
	
public:
	context_t& context() { return m_context; }
	context_t const& context() const {return m_context; }

	int index() const {
		return m_index;
	}

#define printFunctor(f,depth,str,a...)									\
	do{																	\
		char _c[32]={};													\
		for(int i=0;i<(int)sizeof(_c)-1&&i<(depth);i++)_c[i]=' ';		\
		printf("# %s#%2d by 0x%08x at %2d:   " str "\n",_c,(f).index(),(unsigned int)(uintptr_t)(f).context().thread,coreid((f).context()), ##a);				\
	}while(0)

	virtual void print(int depth=0){
		printFunctor(*this,depth,"untyped functor %p",this);
	}
};

template <typename T> struct is_queue { enum { value = false }; typedef T type; };
template <typename T> struct is_queue<Queue<T> > { enum { value = true }; typedef T type; };
template <typename T> struct is_queue<Queue<T>&> { enum { value = true }; typedef T type; };

template <typename A,typename R_> class Process;

template <typename A,typename R>
class Functor : public TypelessFunctor {
	typedef TypelessFunctor base;
	
protected:
	Functor() : TypelessFunctor() {}
	virtual ~Functor(){}
public:
	virtual void cleanup(){delete this;}
	virtual R operator ()(A) =0;

	virtual void finalize(context_t& context,typename deref<R>::type* to=NULL,int index=0){
		base::finalize(context,index);
	}

	virtual Functor& duplicate()=0;
	
	template <typename R2>
	Functor<A,R2>& operator += (Functor<R,R2>& o) {
		return *new Compose<R2>(*this,o);
	}

#ifdef DISABLE_THREADS
	template <typename R2>
	Functor<A,R2>& operator |= (Functor<R,R2>& o) {
		return this->operator +=(o);
	}
#else
	template <typename R2>
	Functor<A,typename Process<A,R2>::R>& operator |= (Functor<R,R2>& o) {
		return *new Compose<typename Process<A,R2>::R>(*this,*new Process<R,R2>(o));
	}
	template <typename R2>
	Functor<A,typename Process<A,R2>::R>& operator |= (Functor<R,Queue<R2>&>& o) {
		return *new Compose<typename Process<A,R2>::R>(*this,*new Process<R,R2>(o));
	}
#endif
	
	virtual void print(int depth=0){
		printFunctor(*this,depth,"%s @ %p",type_string(*this),this);
	}

private:
	template <typename R2>
	class Compose : public Functor<A,R2> {
		Functor<A,R>& m_f1;
		Functor<R,R2>& m_f2;
	public:
		typedef Functor<A,R2> base;
		Compose(Functor<A,R>& f1,Functor<R,R2>& f2) : base(), m_f1(f1), m_f2(f2) {}
		virtual ~Compose(){
			m_f1.cleanup();
			m_f2.cleanup();
		}
		virtual R2 operator ()(A a) {
			return m_f2(m_f1(a));
		}
		virtual Functor<A,R2>& duplicate(){
			return *new Compose(m_f1.duplicate(),m_f2.duplicate());
		}
		virtual void finalize(context_t& context,typename deref<R2>::type* to=NULL,int index=0){
			base::finalize(context,to,index);
			m_f1.finalize(context,NULL,index);
			m_f2.finalize(context,to,index);
		}
		virtual void print(int depth=0){
			m_f1.print(depth);
			m_f2.print(depth);
		}
	};

public:
	template <typename A1,typename R1,bool B> friend class Composition;
	template <typename A1,typename R1> friend class Functor;
};

template <typename A,typename R_>
class Process : public Functor<A,Queue<R_>&> {
	Functor<A,R_>* const m_f;
	Functor<A,Queue<R_>&>* const m_p;
	pthread_t m_pid;
	Queue<A>& m_in;
	Queue<R_>* m_out;
	sync_type(bool) m_ready;

public:
	typedef Queue<R_>& R;
private:
	typedef Functor<A,R> base;

public:
	Process(Functor<A,R_>& f) : base(), m_f(&f), m_p(), m_pid(), m_in(*new Queue<A>()), m_out(), m_ready(false) {
		setup();
	}
	
	Process(Functor<A,Queue<R_>&>& f) : base(), m_f(), m_p(&f), m_pid(), m_in(*new Queue<A>()), m_out(), m_ready(false) {
		setup();
	}

protected:
	virtual ~Process(){
		if(m_f)m_f->cleanup();
		if(m_p)m_p->cleanup();
		delete &m_in;
	}

	void setup(){
		mc_flush(*this);
		int res;
		if((res=pthread_create(&m_pid,NULL,(void*(*)(void*))Process::start,this))){
			fprintf(stderr,"Cannot create process: %s\n",strerror(res));
			abort();
		}
//		fprintf(stderr,"thread 0x%x created for Process %p\n",(unsigned int)m_pid,this);
		pthread_yield();
	}

	static void start(Process* that) __attribute__((noreturn)) {
//		fprintf(stderr,"Starting thread 0x%x...\n",(unsigned int)pthread_self());
		pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
		while(!that->m_ready)
			poll_sleep();
		mc_flush_all();
		that->run();
	}
	void run() __attribute__((noreturn)) {
		while(true){
			A in=m_in.pop();
//			printFunctor(*this,0,"process %p got data",this);
			if(m_f)
				m_out->push((*m_f)(in));
			else
				(*m_p)(in);
		}
	}
public:
	virtual void cleanup(){
		if(m_pid){
			pthread_cancel(m_pid);
			if(pthread_join(m_pid,NULL))
				fprintf(stderr,"Joining thread 0x%x failed, ignored\n",(unsigned int)m_pid);
//			fprintf(stdout,"Killed thread 0x%x...\n",(unsigned int)m_pid);
		}
		base::cleanup();
	}

	Queue<A>& queueIn() { return m_in; }

	virtual R operator ()(A a){
		m_in.push(a);
		return *m_out;
	}

	virtual void finalize(context_t& context,typename deref<R>::type* to=NULL,int index=0){
//		printf("finalize thread\n");
		base::finalize(context,NULL,index);
		if(to==NULL){
			fprintf(stderr,"Cannot initialize process %p, output queue unknown\n",this);
			abort();
		}

		m_in.setInput(context);
		context.thread=m_pid;
		m_in.setOutput(context);
		m_in.finalize();
		m_out=to;
		to->setInput(context);

		if(m_f)m_f->finalize(context,NULL,index);
		if(m_p)m_p->finalize(context,to,index);
		
		mc_flush_all();
		barrier();
		m_ready=true;
	}
	
	virtual void print(int depth=0){
		printFunctor(*this,depth,"process 0x%x @ %p, queue %p -> %p =",(unsigned int)m_pid,this,&m_in,m_out);
		if(m_f)m_f->print(depth+1);
		if(m_p)m_p->print(depth+1);
	}

	virtual Functor<A,R>& duplicate(){
		if(m_f)
			return *new Process(m_f->duplicate());
		else
			return *new Process(m_p->duplicate());
	}

	template <typename A1,typename R1,bool B> friend class Composition;
};
	
template <typename A,typename R>
class WrapFunctor : public Functor<A,R> {
	R (*const m_f)(int,A);
public:
	WrapFunctor(R (*f)(int,A)) : Functor<A,R>(), m_f(f) {}
	virtual R operator ()(A a) {
		return m_f(this->index(),a);
	}

	virtual Functor<A,R>& duplicate(){
		return *new WrapFunctor(m_f);
	}

	virtual void print(int depth=0){
		printFunctor(*this,depth,"wrap %p=%s @ %p",m_f,type_string(m_f),this);
	}
};

template <typename A,typename R>
class StaticWrapFunctor : public WrapFunctor<A,R> {
public:
	StaticWrapFunctor(R (*f)(int,A)) : WrapFunctor<A,R>(f) {}
	virtual void cleanup(){}
};

template <typename A,typename R,typename S >
class StateFunctor : public Functor<A,R> {
	typedef Functor<A,R> base;
	R (*const m_f)(int,S&,A);
	S m_s;
public:
	StateFunctor(R (*f)(int,S&,A)) : base(), m_f(f), m_s() {}
	StateFunctor(R (*f)(int,S&,A),S const& s) : base(), m_f(f), m_s(s) {}
	S& state(){return m_s;}
	virtual R operator ()(A a) {
		return m_f(this->index(),m_s,a);
	}
	virtual Functor<A,R>& duplicate(){
		return *new StateFunctor<A,R,S>(m_f,m_s);
	}
	virtual void print(int depth=0){
		printFunctor(*this,depth,"wrap+state %p=%s @ %p",m_f,type_string(m_f),this);
	}
};

template <typename A,typename Q,int N> class ParMap;

template <typename A,typename R_,int N>
class Map : public Functor<A,R_> {
public:
	typedef typename is_queue<R_>::type R;
	typedef typeof((A())[0]) A1;
	typedef typename is_queue<typeof((R())[0])>::type R1;
	typedef Functor<A1,R1> F;
	typedef void (*J)(R&,R1 const&);

private:
	typedef Functor<A,R> base;

protected:
	
	class Split {
		A const& a;
	public:
		static const int length=N;

		Split(A const& a) : a(a) {}
		A1 const& operator[](int i) const { return a[i];}
		A1 operator[](int i) { return a[i];}
	};

	class Join {
		J const m_f;
		R m_state;
	public:
		Join(J f,R const& start) : m_f(f), m_state(start) {}
		Join& operator+=(R const& v){
			m_f(m_state,v);
			return *this;
		}
		operator R&(){return m_state;}
		operator R const() const{return m_state;}
	};

private:
	F* m_F[N];
	J const m_join;
	R const m_start;

public:
	Map(F& f,J join,R const& start) : base(), m_F(), m_join(join), m_start(start) {
		m_F[0]=&f;
		for(int i=1;i<N;i++)
			m_F[i]=&f.duplicate();
	}
	virtual ~Map(){
		for(int i=0;i<N;i++)
			m_F[i]->cleanup();
	}
	virtual Functor<A,R_>& duplicate(){
		return *new Map(m_F[0]->duplicate(),m_join,m_start);
	}
	virtual R operator()(A a){
		Split as(a);
		Join rs(m_join,m_start);
		for(int i=0;i<as.length;i++)
			rs+=(*m_F[i])(as[i]);
		return rs;
	}
	virtual void finalize(context_t& context,typename deref<R>::type* to=NULL,int index=0){
		base::finalize(context,to,index);
		for(int i=0;i<N;i++)
			m_F[i]->finalize(context,to,this->index()*N+i);
	}
	virtual void print(int depth=0){
		printFunctor(*this,depth,"map @ %p",this);
		for(int i=0;i<N;i++)
			m_F[i]->print(depth+1);
		printFunctor(*this,depth+1,"join using %p=%s",m_join,type_string(J));
	}

	template <typename PA,typename PR,int PN> friend class ParMap;
};

template <typename A,typename Q,int N>
class ParMap : public Functor<A,Q> {
public:
	typedef typename is_queue<Q>::type R;
	typedef typeof((A())[0]) A1;
	typedef typename is_queue<typeof((R())[0])>::type R1;
	typedef Queue<R1> Q1;
	typedef Functor<A1,Q1&> F;
	typedef Functor<A1,R1> F1;
	typedef void (*J)(R&,R1 const&);

private:
	typedef Functor<A,Q> base;

protected:
	
	typedef typename Map<A,R,N>::Split Split;
	
	class Join : public Functor<void*,R> {
		typedef Functor<void*,R> base;
		J const m_f;
		R const m_start;
		Queue<R1> m_q[N];
	public:
		Join(J f,R const& start) : base(), m_f(f), m_start(start) {}
		R1 operator ()(void* _){
			R r=m_start;
			for(int i=0;i<N;i++)
				m_f(r,queue(i).pop());
			return r;
		}
		Queue<R1>& queue(int i){ return m_q[i]; }
		J f() const { return m_f; }
		R start() const { return m_start; }
		Functor<void*,R>& duplicate(){
			return *new Join(m_f,m_start);
		}
		virtual void print(int depth=0){
			printFunctor(*this,depth,"join using %p=%s @ %p",m_f,type_string(J),this);
		}
	};

private:
	F* m_F[N];
	Join& m_join;
	Process<void*,R>& m_proc;

	void setup(F& f){
		m_F[0]=&f;
		for(int i=1;i<N;i++)
			m_F[i]=&f.duplicate();
	}
public:
	ParMap(F& f,J join,R const& start) : base(), m_F(), m_join(*new Join(join,start)), m_proc(*new Process<void*,R>(m_join)) {
		setup(*(F*)new Process<A1,R1>(f));
	}
	ParMap(F1& f,J join,R const& start) : base(), m_F(), m_join(*new Join(join,start)), m_proc(*new Process<void*,R>(m_join)) {
		setup(*(F*)new Process<A1,R1>(f));
	}
	virtual ~ParMap(){
		for(int i=0;i<N;i++)
			m_F[i]->cleanup();
		m_proc.cleanup(); // this will clean m_join
	}
	virtual Functor<A,Q>& duplicate(){
		return *new ParMap(m_F[0]->duplicate(),m_join.f(),m_join.start());
	}
	virtual Q operator()(A a){
		Split as(a);
		for(int i=0;i<as.length;i++)
			(*m_F[i])(as[i]);
		return m_proc(NULL); // dummy data, make join start next iteration
	}
	virtual void finalize(context_t& context,typename deref<Q>::type* to=NULL,int index=0){
		base::finalize(context,to,index);
		for(int i=0;i<N;i++){
			context_t c=context;
			m_F[i]->finalize(c,&m_join.queue(i),this->index()*N+i);
			m_join.queue(i).setInput(c);
		}
		m_proc.finalize(context,to,this->index());
		for(int i=0;i<N;i++){
			m_join.queue(i).setOutput(context);
			m_join.queue(i).finalize();
		}
		m_proc(NULL); // dummy data, make join start polling queues
	}
	virtual void print(int depth=0){
		printFunctor(*this,depth,"parmap @ %p",this);
		for(int i=0;i<N;i++)
			m_F[i]->print(depth+1);
		m_proc.print(depth+1);
	}

	template <typename R2>
	Functor<A,typename Process<A,R2>::R>& operator += (Functor<R,R2>& o) {
		return this->operator |=(o);
	}
	template <typename R2>
	Functor<A,typename Process<A,R2>::R>& operator |= (Functor<R,R2>& o) {
		return *new Compose<R2>(*this,*new Process<R,R2>(o));
	}
	template <typename R2>
	Functor<A,typename Process<A,R2>::R>& operator |= (Functor<R,Queue<R2>&>& o) {
		return *new Compose<R2>(*this,*new Process<R,R2>(o));
	}

	Join& joiner(){return m_join;}
	
private:
	template <typename R2>
	class Compose : public Functor<A,Queue<R2>&> {
		typedef Queue<R2>& Q2;
		typedef Functor<A,Q2> base;
		ParMap& m_f1;
		Process<R,R2>& m_f2;
		typename deref<Q2>::type* m_to;
	public:
		Compose(ParMap& f1,Process<R,R2>& f2) : base(), m_f1(f1), m_f2(f2), m_to() {}
		virtual ~Compose(){
			m_f1.cleanup();
			m_f2.cleanup();
		}
		virtual Functor<A,Q2>& duplicate(){
			return *new Compose(static_cast<ParMap&>(m_f1.duplicate()),static_cast<Process<R,R2>&>(m_f2.duplicate()));
		}
		virtual void finalize(context_t& context,typename deref<Q2>::type* to=NULL,int index=0){
			m_to=to;
			base::finalize(context,to,index);
			m_f1.finalize(context,&m_f2.queueIn(),index);
			m_f2.finalize(context,to,index);
		}
		virtual Q2 operator ()(A a){
			m_f1(a);
			return *m_to;
		}
		virtual void print(int depth=0){
			m_f1.print(depth);
			m_f2.print(depth);
		}
	};
};

template <typename T> void fold_last(T& r,T const& r1){ r=r1; }
template <typename T,typename T1> void fold_sum(T& r,T1 const& r1){ r+=r1; }

template <typename A,int N,typename R,typename Map<A,R,N>::J J>
Functor<A,R>& map(typename Map<A,R,N>::F& f){
	return *new Map<A,R,N>(f,J,(R()));
}

#ifdef DISABLE_THREADS
#  define parmap map
#else
template <typename A,int N,typename R,typename ParMap<A,R,N>::J J>
ParMap<A,Queue<R>&,N>& parmap(typename ParMap<A,Queue<R>&,N>::F& f){
	return *new ParMap<A,Queue<R>&,N>(f,J,(R()));
}

template <typename A,int N,typename R,typename ParMap<A,R,N>::J J>
ParMap<A,Queue<R>&,N>& parmap(typename ParMap<A,Queue<R>&,N>::F1& f){
	return *new ParMap<A,Queue<R>&,N>(f,J,(R()));
}
#endif

template <typename A,typename R,bool Q>
class Composition : public Functor<A,R> {
	typedef Functor<A,R> base;
	Functor<A,R>& m_f;
	int const m_bulk;
public:
	template <typename A1,typename R1> static Composition<A1,R1,false> typecheck(Functor<A1,R1>& c);
	template <typename A1,typename R1> static Composition<A1,R1,true> typecheck(Functor<A1,Queue<R1>&>& c);

	Composition(Functor<A,R>& f,int bulk=BF_TIME_STEPS) : base(), m_f(f), m_bulk(bulk) {
		context_t context(pthread_self());
		finalize(context);
#ifdef BF_DEBUG
		this->print();
#endif
	}
	virtual ~Composition(){m_f.cleanup();}
	virtual R operator()(A a){
		for(int i=0;i<m_bulk;i++)
			m_f(a);
		return (R());
	}
	
	virtual void print(int depth=0){
		printFunctor(*this,depth,"function composition @ %p",this);
		m_f.print(depth+1);
	}
	
	virtual void cleanup(){}	
	virtual Functor<A,R>& duplicate(){
		return *this;
	}
	
	virtual void finalize(context_t& context,typename deref<R>::type* to=NULL,int index=0){
		base::finalize(context,to,index);
		m_f.finalize(context,to,index);
	}
};

template <typename A,typename R>
class Composition<A,R,true> : public Functor<A,R> {
	typedef Functor<A,R> base;
	typedef Queue<R> Q;
	Functor<A,Q&>& m_f;
	Q m_q;
	Queue<bool> m_qdone;
	pthread_t m_pid;
	int const m_bulk;
	sync_type(bool) m_ready;
public:
	Composition(Functor<A,Q&>& f,int bulk=BF_TIME_STEPS) : base(), m_f(f), m_bulk(bulk), m_ready(false) {
		context_t context(pthread_self());
		finalize(context);
#ifdef BF_DEBUG
		this->print();
#endif
		m_ready=true;
	}
	virtual ~Composition(){
		pthread_cancel(m_pid);
		if(pthread_join(m_pid,NULL))
			fprintf(stderr,"Joining terminator thread 0x%x failed, ignored\n",(unsigned int)m_pid);

		m_f.cleanup();
	}
	virtual R operator()(A a){
		for(int i=0;i<m_bulk;i++)
			m_f(a);
		m_qdone.pop();
		return (R());
	}
	
	virtual void print(int depth=0){
		printFunctor(*this,depth,"process composition @ %p, queue -> %p",this,&m_q);
		m_f.print(depth+1);
	}
	
	virtual void cleanup(){}	
	virtual Functor<A,R>& duplicate(){
		return *this;
	}
	virtual void finalize(context_t& context,typename deref<R>::type* to=NULL,int index=0){
		base::finalize(context,to,index);
		mc_flush(*this);
		int res;
		if((res=pthread_create(&m_pid,NULL,(void*(*)(void*))terminator,this))){
			fprintf(stderr,"Cannot create terminator process: %s\n",strerror(res));
			abort();
		}
		
		m_qdone.setOutput(context);
		context_t c=context;
		m_f.finalize(c,&m_q);

		c.thread=m_pid;
		m_q.setOutput(c);
		m_q.finalize();
		
		m_qdone.setInput(c);
		m_qdone.finalize();
	}

protected:
	static void terminator(Composition* that) __attribute__((noreturn)) {
		while(!that->m_ready)
			poll_sleep();
		that->terminator2();
	}
	void terminator2() __attribute__((noreturn)) {
		pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
		while(true){
			for(int i=0;i<m_bulk;i++)
				m_q.pop();
			m_qdone.push(true);
		}
	}
};


#define FUNCTOR(fun,A,R)													\
	static R fun##_fun(int,A) __attribute__((unused));						\
	static StaticWrapFunctor<A,R> fun __attribute__((unused)) ATTR_DATA_ALIGNED =fun##_fun;	\
	static R fun##_fun(int antenna,A arg)

#define STATE_FUNCTOR(fun,A,R,S)									\
	static R fun##_fun(int,S&,A) __attribute__((unused));			\
	class fun##_t : public StateFunctor<A,R,S> {					\
		typedef StateFunctor<A,R,S> base;							\
	public:															\
		fun##_t() : base(fun##_fun) {}								\
		fun##_t(S const& s) : base(fun##_fun,s) {}					\
	};																\
	class fun##_td : public StateFunctor<A,R,S> {					\
		typedef StateFunctor<A,R,S> base;							\
	public:															\
		fun##_td() : base(fun##_fun) {}								\
		fun##_td(S const& s) : base(fun##_fun,s) {}					\
	};																\
	static Functor<A,R>& fun(S const& s=S()) __attribute__((unused)); \
	static Functor<A,R>& fun(S const& s){							\
		return *new fun##_td(s);									\
	}																\
	static R fun##_fun(int antenna,S& state, A arg)

#define compose(var,p...)											\
	typeof(Composition<>::typecheck(p)) var(p)


////////////////////////////////////////////////
// Functional parts of the application
////////////////////////////////////////////////

FUNCTOR(test,Timestamp,Timestamp){
	printf("(%d) now: %f\n",antenna,(double)arg);
	usleep(500000);
	return arg;
}

FUNCTOR(print_time,Timestamp,Timestamp){
	printf("now: %f ",(double)arg);
	return arg;
}

FUNCTOR(gen_samples,Timestamp,Samples){
	Samples s=Samples();
//	printf("# Generating samples at timestamp %f of %d sources...\n",(double)arg,BF_SOURCES);
	for(int i=0;i<env.sources();i++)
		s+=sources[i].samples(array,arg);
	return s;
}

FUNCTOR(gen_sample,Timestamp,Sample){
	if(antenna>=array.antennas())
		return 0;

	Sample s=Sample();
//	printf("# Generating sample for antenna %d at timestamp %f of %d sources...\n",antenna,(double)arg,env.sources());
	for(int i=0;i<env.sources();i++)
		s+=sources[i].sample(array,antenna,arg);
	return s;
}

FUNCTOR(print_samples,Samples,Samples){
	arg.print(""," ");
	return arg;
}

FUNCTOR(print_sample,Sample,Sample){
	arg.print("","\n");
	return arg;
}

class SystemTimestamp {
	int m_iteration;
	int m_step;

public:
	SystemTimestamp() : m_iteration(), m_step() {}
	Timestamp step(Timestamp offset=0){
		if(++m_step==BF_TIME_STEPS){
			m_step=0;
			m_iteration++;
		}
		return m_step*BF_TIME_STEP;
	}
	operator Timestamp(){
		return (m_iteration*BF_TIME_STEPS+m_step)*BF_TIME_STEP;
	}
};

STATE_FUNCTOR(gen_time,Timestamp,Timestamp,SystemTimestamp){
	return state.step(arg);
}

template <int N=BF_FILTER_TAPS>
struct FIRFilter {
	int taps;
	Sample coeff[N];
	Sample samples[N];

	Angle config_steered;
	int config_antennas;

	FIRFilter() : taps(N), coeff(), samples(), config_steered(), config_antennas() {
		delay(0);
	}

	void print(char const* padl=NULL,char const* padr=NULL){
		printf("%sFIR %p: c={ ",padl?padl:"",this);
		for(int i=0;i<taps;i++)
			coeff[i].print(""," ");
		printf("}, samples={ ");
		for(int i=0;i<taps;i++)
			samples[i].print(""," ");
		printf("}%s",padr?padr:"\n");
	}

	void delay(Timestamp t){
		if(taps>N)taps=N;

		Timestamp offset=t/(1.0/BF_SAMPLE_FREQ);
		for(int i=0;i<taps;i++){
			Sample x = (Sample)i-offset;
			coeff[i]=
				sinc_<Sample::type>(x-(Sample)(taps/2))*
				window_<Sample::type>(x,taps);
		}
	}
};

STATE_FUNCTOR(fir_delay,Sample,Sample,FIRFilter<>){
	if(antenna>=array.antennas())
		return 0;
	
	// update coefficients
	if(	array.steered()!=state.config_steered ||
		array.antennas()!=state.config_antennas ||
		array.taps()!=state.taps){

		state.config_steered=array.steered();
		state.config_antennas=array.antennas();
		state.taps=array.taps();

		state.delay(-array.AntennaDelay(antenna));
	}

	// shift samples
	for(int i=state.taps-1;i>=1;i--)
		state.samples[i]=state.samples[i-1];
	state.samples[0]=arg;

	// multadd
	Sample out=Sample(0);
	for(int i=0;i<state.taps;i++)
		out+=state.samples[i]*state.coeff[i];

	return out;
}

FUNCTOR(sum,Samples,Sample){
	Sample out=Sample(0);
	
	// sum samples
	for(int i=array.antennas()-1;i>=0;i--)
		out+=arg[i];
	
	// normalize
	out/=array.antennas();

	return out;
}

FUNCTOR(normalize,Sample,Sample){
	return arg/array.antennas();
}

template <int N=BF_FFT_SIZE>
struct FFTBlock {
	static const int size=N;
	int logsize;
	complex_t sample[N];
	int count;

	FFTBlock() : logsize(log2(N)), count(0) {}
protected:
	static int log2(int n){
		int res=0;
		for(;n>1;n/=2,res++);
		return res;
	}
};

STATE_FUNCTOR(fft,Sample,FFTBlock<>*,FFTBlock<>){
	state.sample[state.count]=arg;
	if(++state.count>=state.size){
//		fprintf(stderr,"FFT size %d, depth %d\n",state.size,state.logsize);
		// do fft
#ifdef BF_TIME_TASKS
		Timer t=Timer("FFT");
#endif
		cfft2(state.sample,state.size,state.logsize,0);
		// scale output
		for(int i=0;i<state.size;i++)
			state.sample[i]/=state.size/2;
		state.count=0;
		return &state;
	}else 
		return NULL;
}

FUNCTOR(print_fft,FFTBlock<>*,bool){
	if(arg){
#ifdef BF_TIME_TASKS
		Timer t=Timer("Visualize");
#endif
		output->flush();
		FFTBlock<>& s=*arg;
		for(int i=0;i<s.size/2;i++)
			output->FFTdata(
				(float)(i)/(float)s.size*(float)BF_FREQ_CAP*2.0,
				sqrt_(s.sample[i].r*s.sample[i].r+s.sample[i].i*s.sample[i].i));
		return true;
	}else
		return false;
}

struct BlockInterval {
	int samples;
	Timer timer;

	BlockInterval(char const* desc=NULL) : samples(), timer(desc) {}
	~BlockInterval(){timer.stop();}
	void count(){
		if(samples++==0)
			timer.start();
		else if(samples==BF_TIME_STEPS){
			uint32_t ms=timer.stop();
			timer.print("");
			printf(" per %d samples (%.3f s per sample)\n",
				BF_TIME_STEPS,(double)ms/BF_TIME_STEPS/1000.0);
			samples=0;
		}
	}
};

STATE_FUNCTOR(time_sample,Sample,Sample,BlockInterval){
	state.count();
	return arg;
}

template <typename R,bool B>
void run(Composition<Timestamp,R,B>& f){
	mc_flush(array);
	mc_flush(env);
	f(0);
}


////////////////////////////////////////////////
// The application
////////////////////////////////////////////////

// main accesses the non-const variables by default
#define array array_
#define env env_

extern "C" int main(int argc,char* argv[]){
	
	array.turn(M_PI_2);
	array.steer(-M_PI_4);

#ifdef __MICROBLAZE__
	output=new HelixRender();
	input=new ButtonInput(Input::STEER_ANIM);
#else
#  ifdef BF_PROFILE
	output=new Output();
	input=new AutoRotate(M_PI*2.0/BF_ROTATE_ANIM_STEP);
#  else
#    ifdef BF_TERMINAL_OUTPUT
	output=new Output();
	input=new AutoRotate(10);
#    else
#      ifdef HAVE_GNUPLOT
	output=new Gnuplot(argc>1?argv[1]:"bf.gp",argc>1);
#      else
	output=new Output();
#      endif
#      ifdef HAVE_SDL
	input=new SDLKeyboard();
#      else
	input=new AutoRotate(M_PI*2.0/BF_ROTATE_ANIM_STEP*4.0);
#      endif
#    endif
#  endif
#endif
	
//	output=new Output();
//	input=new Input(Input::QUIT);
//	input=new AutoRotate(M_PI*2.0/BF_ROTATE_ANIM_STEP);

	mc_flush(input);
	mc_flush(output);

	compose(pipe,
			gen_time()
		+=	parmap<Timestamp,BF_MAX_CORES,Sample,fold_sum>(
				map<Timestamp,BF_ANTENNAS_PER_CORE,Sample,fold_sum>(
						gen_sample
					+=	fir_delay()
//					+=	print_sample
				)
			)
		|= normalize
//		+= print_sample
#ifdef BF_TIME_TASKS
		+= time_sample()
#endif
		|= fft()
		+= print_fft
	);

	bool anim=false;
	bool steer_anim_left=true;
	while(1){
		anim=false;
		switch(input->poll()){
		case Input::STEER_RIGHT:	array.steerright();	break;
		case Input::STEER_LEFT:		array.steerleft();	break;
		case Input::STEER_ANIM:
			if(steer_anim_left){
				array.steerleft(BF_ROTATE_ANIM_STEP);
				steer_anim_left=array.steered()+BF_ROTATE_ANIM_STEP*2<M_PI_2;
			}else{
				array.steerright(BF_ROTATE_ANIM_STEP);
				steer_anim_left=array.steered()-BF_ROTATE_ANIM_STEP*2<-M_PI_2;
			}
			anim=true;
			break;
		case Input::STEER_SOURCE0:	if(env.sources()>0)array.steer(env.source(0).position().angle()-array.turned()); break;
		case Input::STEER_SOURCE1:	if(env.sources()>1)array.steer(env.source(1).position().angle()-array.turned()); break;
		case Input::STEER_SOURCE2:	if(env.sources()>2)array.steer(env.source(2).position().angle()-array.turned()); break;
		case Input::STEER_SOURCE3:	if(env.sources()>3)array.steer(env.source(3).position().angle()-array.turned()); break;
		case Input::STEER_SOURCE4:	if(env.sources()>4)array.steer(env.source(4).position().angle()-array.turned()); break;
		case Input::STEER_SOURCE5:	if(env.sources()>5)array.steer(env.source(5).position().angle()-array.turned()); break;
		case Input::STEER_SOURCE6:	if(env.sources()>6)array.steer(env.source(6).position().angle()-array.turned()); break;
		case Input::STEER_SOURCE7:	if(env.sources()>7)array.steer(env.source(7).position().angle()-array.turned()); break;
		case Input::STEER_SOURCE8:	if(env.sources()>8)array.steer(env.source(8).position().angle()-array.turned()); break;
		case Input::STEER_SOURCE9:	if(env.sources()>9)array.steer(env.source(9).position().angle()-array.turned()); break;
		case Input::TURN_RIGHT:		array.turnright();	break;
		case Input::TURN_LEFT:		array.turnleft();	break;
		case Input::TURN_ANIM:		array.turnleft(BF_ROTATE_STEP/5.0); anim=true;	break;
		case Input::ROTATE_ANIM:	array.turnleft(BF_ROTATE_STEP/5.0);array.steerright(BF_ROTATE_STEP/5.0); anim=true;	break;
		case Input::ADD_SOURCE:		env.addSource();	break;
		case Input::DROP_SOURCE:	env.dropSource();	break;
		case Input::ADD_ANTENNA:	array.addAntenna();	break;
		case Input::DROP_ANTENNA:	array.dropAntenna();break;
		case Input::ADD_TAPS:		array.addTaps();	break;
		case Input::DROP_TAPS:		array.dropTaps();	break;
		case Input::STATS:			output->stats();	break;
		case Input::QUIT:			goto stop;
		case Input::REDRAW:			break;
		case Input::NOP:
		default:;
			usleep(100000);
			continue;
		}
		
		output->start();
		run(pipe);
		output->finish(!anim);
	}

stop:
	delete output;
	delete input;

	return 0;
}

