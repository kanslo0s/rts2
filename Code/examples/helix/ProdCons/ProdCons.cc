#include <helix.h>

void* ProdProcess(void* arg){
	WFifo<int> f((Fifo*)OpenChannel(0));
	unsigned int total=0;

	for(int i=0;i<100;i++){
//		printf("producing %d...\n",i);
		total+=i;
		f.push(i);
	}

	printf("waiting for consumer...\n");
	unsigned int r;
	r=MailRead((SimpleMail*)OpenChannel(1));
	if(r==total)
		printf("consumer has received everything, really, I checked: %d\n",r);
	else
		printf("Uh oh, consumer has received %d items, instead of %d!\n",r,total);

	printf("reset vector: %p %p\n",*(void**)0,*(void**)4);

	return (void*)1;
}

void* ConsProcess(void* arg){
	RFifo<int> f((Fifo*)OpenChannel(0));
	unsigned int total=0;

	for(int i=0;i<100;i++){
		int v;
		v=*f.front();
		f.pop();
		total+=v;
//		printf("consuming %d...\n",v);
	}

	printf("Sending 'I Love You' mail to producer...\n");
	MailWrite((SimpleMail*)OpenChannel(1),total);
	printf("consumer done\n");

	return (void*)total;
}

void* ProdProcess2(void* arg){
/*	FILE* f=fdopen(0,"w");
	unsigned int total=0;

	for(int i=0;i<100;i++){
//		printf("producing %d...\n",i);
		total+=i;
		fwrite(&i,sizeof(int),1,f);
	}
	fclose(0);

	return (void*)total;*/
	return NULL;
}

void* ConsProcess2(void* arg){
/*	FILE* f=fdopen(0,"r");
	unsigned int total=0;

	for(int i=0;i<100;i++){
		int v;
		fread(&v,sizeof(int),1,f);
		total+=v;
//		printf("consuming %d...\n",v);
	}
	fclose(0);

	return (void*)total;*/
	return NULL;
}

void* ProdProcess3(void* arg){
	pthread_mutex_t* mutex=(pthread_mutex_t*)arg;
	volatile unsigned int* s=(volatile unsigned int*)OpenChannel(0);
	printf("producer mutex: %p, share: %p\n",mutex,s);

	int ret;
	for(int i=0;i<1000;i++){
		if((ret=pthread_mutex_lock(mutex)))
			printf("p lock failed: %d\n",ret);
		mc_entry_rwu(*s);
		(*s)++;
		mc_exit_rwu(*s);
		pthread_mutex_unlock(mutex);
		printf("p: %d\n",i);
	}
	printf("p done\n");
	return NULL;
}

void* ConsProcess3(void* arg){
	pthread_mutex_t* mutex=(pthread_mutex_t*)arg;
	volatile unsigned int* s=(volatile unsigned int*)OpenChannel(0);
	printf("consumer mutex: %p, share: %p\n",mutex,s);

	int ret;
	for(int i=0;i<1000;i++){
		if((ret=pthread_mutex_lock(mutex)))
			printf("c lock failed: %d\n",ret);
		mc_entry_rwu(*s);
		(*s)--;
		mc_exit_rwu(*s);
		pthread_mutex_unlock(mutex);
		printf("c: %d = %d\n",i,*s);
		mc_flush(*s);
	}
	return NULL;
}

void InitHardware(void* arg){
}

int main(int argc,char** argv){
	pid_t prod,cons;
	int err;
	prf_mem_snapshot();
	prf_mb_snapshot();
	printf("Application started\n");

	if((err=CreateProcess(prod,&ProdProcess,NULL,5,PROC_DEFAULT_STACK)))
		printf("prod creation failed: %d\n",err);
	if((err=SetProcessFlags(prod,PROC_FLAG_JOINABLE)))
		printf("flags error: %d\n",err);
	if((err=CreateProcess(cons,&ConsProcess,NULL,1,PROC_DEFAULT_STACK)))
		printf("cons creation failed: %d\n",err);
	if((err=SetProcessFlags(cons,PROC_FLAG_JOINABLE)))
		printf("flags error: %d\n",err);
	
#define PD_FIFO_SIZE 16
#define PD_FIFO_TYPE int
#define PD_FIFO_FLAGS (FIFO_LOCAL|FIFO_ENDPOINT_BOTH)
	printf("Allocating memory for fifo...");
	FifoEP* ep=AllocFifoEndPoint(PD_FIFO_SIZE,sizeof(PD_FIFO_TYPE),PD_FIFO_FLAGS);
	printf(" at %p\n",ep);
	printf("Initializing fifo...");
	printf(" ret %d\n",InitFifo(ep,ep,PD_FIFO_SIZE,PD_FIFO_FLAGS));
	printf("Allocating channels...");
	printf(" ret %d",AllocChannels(prod,2));
	printf(" %d\n",AllocChannels(cons,2));
	printf("Assigning channels for fifo...");
	printf(" ret %d",AssignChannel(prod,0,&ep->fifo));
	printf("  %d\n",AssignChannel(cons,0,&ep->fifo));

	printf("Allocating memory for mail...");
	SimpleMail* m=AllocSimpleMail();
	InitMail(m);
	printf(" at %p\n",m);
	printf("Assigning channels for mail...");
	printf(" ret %d",AssignChannel(prod,1,(void*)m));
	printf(" %d\n",AssignChannel(cons,1,(void*)m));

	printf("Starting processes...\n");
	if((err=StartProcess(prod)))
		printf("prod start failed: %d\n",err);
	if((err=StartProcess(cons)))
		printf("cons start failed: %d\n",err);

	printf("Wait for both processes...\n");
	void* ret;
	err=WaitProcess(prod,&ret);
	printf("join prod: %d %p\n",err,ret);
	err=WaitProcess(cons,&ret);
	printf("join cons: %d %p\n",err,ret);

	printf("Redoing test using pthreads...\n");
	pthread_attr_t a;
	pthread_attr_init(&a);
	pthread_t thprod,thcons;
	printf("Creating producer...");
	printf(" ret %d\n",pthread_create(&thprod,&a,&ProdProcess2,NULL));
	printf("Creating consumer...");
	printf(" ret %d\n",pthread_create(&thcons,&a,&ConsProcess2,NULL));
	pthread_attr_destroy(&a);
	printf("Allocating channels...");
	printf(" ret %d",lAllocChannels(thprod,1));
	printf(" %d\n",lAllocChannels(thcons,1));
	printf("Assigning channels for fifo...");
	printf(" ret %d",lAssignChannel(thprod,0,&ep->fifo));
	printf("  %d\n",lAssignChannel(thcons,0,&ep->fifo));
	void *ret_prod,*ret_cons;
	printf("Waiting for producer thread...\n");
	printf("Producer finished: %d\n",pthread_join(thprod,&ret_prod));
	printf("Waiting for consumer thread...\n");
	printf("Consumer finished: %d\n",pthread_join(thcons,&ret_cons));
	if((int)ret_prod==(int)ret_cons)
		printf("Test successful: %d\n",(int)ret_prod);
	else
		printf("Bytes lost in transfer: p: %d c: %d\n",(int)ret_prod,(int)ret_cons);

	printf("Redoing on two cores\n");

	printf("Creating producer...");
	err=CreateProcess(prod,&ProdProcess,NULL,10,PROC_DEFAULT_STACK,1);
	printf(" ret: %d, pid: %d\n",err,prod);

	// sending commands to TD
	printf("Starting producer...\n");
	VERIFY(StartProcess(prod,1),0);
	
	printf("Creating consumer locally...\n");
	if((err=CreateProcess(cons,&ConsProcess,NULL,1,PROC_DEFAULT_STACK,0)))
		printf("cons creation failed: %d\n",err);

	printf("Allocing producer fifo endpoint...\n");
	FifoEP* prodep=AllocFifoEndPoint(16,sizeof(int),FIFO_ENDPOINT_WRITE|FIFO_SPLIT,1);
	printf("Producer EP? %p\n",prodep);

	printf("Allocing consumer fifo endpoint...\n");
	FifoEP* consep=AllocFifoEndPoint(16,sizeof(int),FIFO_ENDPOINT_READ|FIFO_SPLIT,0);
	printf("Allocing producer channels... ret %d\n",AllocChannels(prod,2,1));
	printf("Allocing consumer channels... ret %d\n",AllocChannels(cons,2,0));
	printf("Allocing mailbox @ producer... ret %p\n",(m=AllocSimpleMail(1)));
	InitMail((SimpleMail*)m);

	printf("Initializing prodcons fifo...\n");
	InitFifo(prodep,addr_global(consep),16,FIFO_SPLIT);
//	printf("w: %p %p %p %p\n",prodep,prodep->fifo.local,prodep->fifo.remote,prodep->fifo.fifo);
//	printf("r: %p %p %p %p\n",consep,consep->fifo.local,consep->fifo.remote,consep->fifo.fifo);

	// finish setup
	printf("Assigning channels to producer...\n");
	printf("%d\n",AssignChannel(prod,0,addr_local(&prodep->fifo),0,1));
	printf("%d\n",AssignChannel(prod,1,(void*)addr_local(m),0,1));
	printf("Assigning channels to consumer...\n");
	printf("%d\n",AssignChannel(cons,0,&consep->fifo,0,0));
	printf("%d\n",AssignChannel(cons,1,(void*)m,0,0));
	if((err=StartProcess(cons,0)))
		printf("cons start failed: %d\n",err);


	sleep(10);

	// continue in parallel with next test: mutex stress test
	pthread_mutex_t* mutex;
	volatile unsigned int* share;
	HEAP* v=NULL;
	printf("Shared heap: %p %p %p 0x%x\n",&_sheap,_sheap,v,SHARED_HEAP_SIZE);
	malloc_init(&v,_sheap,SHARED_HEAP_SIZE);
	printf("Shared heap: %p %p\n",_sheap,v);
	printf("Allocating mutex: %p\n",(mutex=(pthread_mutex_t*)smalloc(sizeof(pthread_mutex_t))));
	printf("Allocating shared memory: %p\n",(share=(unsigned int*)smalloc(sizeof(unsigned int))));
	VERIFY(pthread_mutex_init(mutex,NULL),0);
	mc_entry_wo(*share);
	*share=0xcafebabe;
	mc_exit_wo(*share);
	printf("Creating producer...");
	err=CreateProcess(prod,&ProdProcess3,mutex,10,PROC_DEFAULT_STACK,1);
	printf(" ret: %d, pid: %d\n",err,prod);
	SetProcessFlags(prod,PTHREAD_CREATE_JOINABLE,1);
	printf("Creating consumer locally...\n");
	if((err=CreateProcess(cons,&ConsProcess3,mutex,5,PROC_DEFAULT_STACK,0)))
		printf("cons creation failed: %d\n",err);
	SetProcessFlags(cons,PTHREAD_CREATE_JOINABLE,0);
	printf("Assigning channels...\n");
	VERIFY(AllocChannels(prod,1,1),0);
	AssignChannel(prod,0,(void*)share,0,1);
	VERIFY(AllocChannels(cons,1,0),0);
	AssignChannel(cons,0,(void*)share,0,0);
	printf("Starting processes...\n");
	VERIFY(StartProcess(prod,1),0);
	if((err=StartProcess(cons,0)))
		printf("cons start failed: %d\n",err);

	// wait for consumer
	printf("join cons: %d\n",pthread_join(cons,NULL));
	printf("join prod: %d\n",WaitProcess(prod,NULL,1));
	usleep(100000);
	mc_entry_ro(*share);
	printf("mutex stress test done: 0x%x\n",*share);
	mc_exit_ro(*share);

	printf("All done\n");
	prf_mb_t* mb_p=prf_mb_snapshot();
	prf_mem_dump();
	prf_mb_dump(mb_p);

	return 0;
}

