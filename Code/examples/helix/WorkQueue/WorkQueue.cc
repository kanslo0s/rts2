#include <helix.h>

#define NUM_SLAVES (NUM_PROCESSORS-1)
#define WORK_BY_GEN

WorkQueue<int>* wq;

pthread_barrier_t bar;

typedef struct { int gen,work; } w_t;
w_t worked[NUM_PROCESSORS] ATTR_DATA_ALIGNED;

void* slave(void* arg){
	int work_generated=0,work_worked=0;

	unsigned int seed=getgpid();
	for(int i=rand_r(&seed)%10;i>0;i--){
#ifdef WORK_BY_GEN
		if(wq->IsFull())
			break;
#endif
		int w=rand_r(&seed)%100;
		work_generated+=w;
		wq->Push(w);
	}

	int work;
	while(wq->Pop(work)){
		printf("slave %2d: work %d\n",GetProcID(),work);
		usleep(work*10000);
		work_worked+=work;
	}
	printf("exiting slave %d\n",pthread_self());
	typeof(&worked[0]) w_=&worked[GetProcID()];
	mc_entry_wou(*w_);
	w_->gen=work_generated;
	w_->work=work_worked;
	mc_exit_wou(*w_);
	int res __attribute__((unused))=pthread_barrier_wait(&bar);
	ASSERT(res==0||res==PTHREAD_BARRIER_SERIAL_THREAD,"Barrier broken");
	return NULL;
}

int gen_work ATTR_DATA_ALIGNED;

bool work_gen(int& w,bool reset,void* arg){
#ifdef WORK_BY_GEN
	static int i=100;
	w=i;
	if(i<500){
		gen_work+=i++;
		outbyte('.');
		return true;
	}else return false;
#else
	return false;
#endif
}

int main(int argc,char** argv){
	wq=new WorkQueue<int>(8,work_gen);
	mc_flush(wq);
	VERIFY(pthread_barrier_init(&bar,NULL,NUM_SLAVES+1),0);
	int res=wq->StartSlaves(NUM_SLAVES,slave);
	if(res)
		printf("Cannot start slaves: %d\n",res);

	int bres __attribute__((unused))=pthread_barrier_wait(&bar);
	ASSERT(res==0||res==PTHREAD_BARRIER_SERIAL_THREAD,"Barrier broken");
	int allwork=0,allwork_gen=0;
	mc_entry_ro(worked);
	for(int i=0;i<NUM_PROCESSORS;i++){
		allwork_gen+=worked[i].gen;
		allwork+=worked[i].work;
		printf("slave %2d: generated=%6d worked=%6d\n",i,worked[i].gen,worked[i].work);
	}
	mc_exit_ro(worked);
	if(allwork-allwork_gen-gen_work==0)
		printf("work ok\n");
	else
		printf("ERROR: work diff %d (gen %d)\n",allwork-allwork_gen-gen_work,gen_work);
	return 0;
}

