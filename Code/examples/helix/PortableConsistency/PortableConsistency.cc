#include <helix.h>

#define WORKERS		NUM_PROCESSORS
#define WORKERS_SC	(WORKERS>2?2:WORKERS)
#define TWO_PRODS	1
#define ITERATIONS	1000

#define SHOW_PROGRESS() outbyte('A'+GetProcID())

#ifdef PMC_ENABLE_DEBUG
pthread_mutex_t print_lck=PTHREAD_MUTEX_INITIALIZER;
#  define DEBUG(a...) do{auto_ret<int,0>(pthread_mutex_lock(&print_lck));printf(a);auto_ret<int,0>(pthread_mutex_unlock(&print_lck));}while(0)
#  define outbyte(...)
#else
#  define DEBUG(...)
//#  define outbyte(...)
#endif

#define ELEM_TYPE		long long
#define FIFO_SIZE_BITS	4

mfifo_t<ELEM_TYPE,WORKERS_SC,FIFO_SIZE_BITS,::PMC_NS::sc>	mfifo_sc	PMC_VAR_ATTR_SC;
mfifo_t<ELEM_TYPE,WORKERS,FIFO_SIZE_BITS,::PMC_NS::cache>	mfifo_cache PMC_VAR_ATTR_CACHE;
mfifo_t<ELEM_TYPE,WORKERS,FIFO_SIZE_BITS,::PMC_NS::ring>	mfifo_ring	PMC_VAR_ATTR_RING;
mfifo_t<ELEM_TYPE,WORKERS,FIFO_SIZE_BITS,::PMC_NS::spm>		mfifo_spm	PMC_VAR_ATTR_SPM;

#define START_LOW	0
#define START_HIGH	(ITERATIONS*2)

template <typename F>
void fifo_prod(bool high,F& fifo){
	for(int i=0;i<ITERATIONS;i++){
		DEBUG("%d@%d: producing %d\n",high?1:0,GetProcID(),i+(high?START_HIGH:START_LOW));
		outbyte(high?'!':'.');
		fifo.push(i+(high?START_HIGH:START_LOW));
	}
}

void fifo_prod_sc(bool high){	 fifo_prod<typeof(mfifo_sc)>(high,mfifo_sc); }
void fifo_prod_cache(bool high){ fifo_prod<typeof(mfifo_cache)>(high,mfifo_cache); }
void fifo_prod_ring(bool high){	 fifo_prod<typeof(mfifo_ring)>(high,mfifo_ring); }
void fifo_prod_spm(bool high){	 fifo_prod<typeof(mfifo_spm)>(high,mfifo_spm); }

template <typename F>
void fifo_cons(int me,F& fifo){
	int last_high=START_HIGH-1,last_low=START_LOW-1;
	while(true){
		typeof(fifo.data_t()) i;
		fifo.pop(i,me);
		if(i>=START_HIGH){
			ASSERT(last_high+1==i,"Consumed high out of order: %d -> %d",last_high,(int)i);
			last_high=i;
			if(me<26)outbyte('A'+me);
		}else{
			ASSERT(last_low+1==i,"Consumed low out of order: %d -> %d",last_low,(int)i);
			last_low=i;
			if(me<26)outbyte('a'+me);
		}
		DEBUG("%d@%d: consuming %d\n",me,GetProcID(),(int)i);
		if( 
#if TWO_PRODS==1
			last_high==START_HIGH+ITERATIONS-1&&
#endif
			last_low==START_LOW+ITERATIONS-1)
			break;
	}
}

void fifo_cons_sc(int me){		fifo_cons<typeof(mfifo_sc)>(me,mfifo_sc); }
void fifo_cons_cache(int me){	fifo_cons<typeof(mfifo_cache)>(me,mfifo_cache); }
void fifo_cons_ring(int me){	fifo_cons<typeof(mfifo_ring)>(me,mfifo_ring); }
void fifo_cons_spm(int me){		fifo_cons<typeof(mfifo_spm)>(me,mfifo_spm); }

#define fifo_test_variant(var)									\
void fifo_test_##var(){											\
	printf("mfifo_" STRINGIFY(var) " test %p, %d readers\n",	\
		&mfifo_##var,WORKERS);									\
	pthread_t pl,ph __attribute__((unused));					\
	par(fifo_prod_##var,false,&pl);								\
	if(TWO_PRODS)par(fifo_prod_##var,true,&ph);					\
	for(int i=0;i<WORKERS;i++)									\
		par(fifo_cons_##var,i);									\
	auto_ret<int,0>(pthread_join(pl,NULL));						\
	if(TWO_PRODS)auto_ret<int,0>(pthread_join(ph,NULL));		\
	sleep(1);													\
	printf("\n\nfifo done\n");									\
	sleep(1);													\
}

void fifo_test_sc(){
	printf("mfifo_sc test %p, %d readers\n",&mfifo_sc,WORKERS_SC);
	pthread_t pl,ph __attribute__((unused));
	par(fifo_prod_sc,false,&pl,1);
	if(TWO_PRODS)par(fifo_prod_sc,true,&ph,1);
	for(int i=0;i<WORKERS_SC;i++)
		par(fifo_cons_sc,i,NULL,1);
	auto_ret<int,0>(pthread_join(pl,NULL));
	if(TWO_PRODS)auto_ret<int,0>(pthread_join(ph,NULL));
	sleep(1);
	printf("\n\nfifo done\n");
	sleep(1);
}

fifo_test_variant(cache)
fifo_test_variant(ring)
fifo_test_variant(spm)

int main(int argc,char** argv){
	fifo_test_sc();
	fifo_test_cache();
	fifo_test_ring();
	fifo_test_spm();
	printf("\n\nall done\n");
	return 0;
}

