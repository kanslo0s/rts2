//#define CFIFO_DEBUG
#include <helix.h>
#include <cfifo.h>
#include <stdio.h>
#include <unistd.h>

#define ITERATIONS	1000
#define FROM_CORE	3
#define TO_CORE		7

void* prod(CFifo<int,CFifo<>::w> &f){ // mind the reference
	int sum=0;
	for(int i=0;i<ITERATIONS;i++){
		printf("prod: %d\n",i);
		sum+=i;
		f+=i; // identical to f.push(i)
	}
	sleep(1);
	printf("prod exit: %d\n",sum);
	f+=-1;
	return NULL;
}

void* cons(CFifo<int,CFifo<>::r> &f){ // mind the reference
	int sum=0;
	while(f!=-1){ // cast of f to int is identical to f.front()
		printf("cons: %d\n",(int)f);
		sum+=f--; // f-- first reads f.front(), then f.pop() and then returning the previous front
	}
	printf("cons exit: %d\n",sum);
	return NULL;
}


int main(int argc,char** argv){
	int res=0;
	usleep(100000);

	{
		CFifo<int,CFifo<>::w> *ffrom;
		CFifo<int,CFifo<>::r> *fto;
		CFifoPtr<int> a=CFifo<int>::Create(FROM_CORE,ffrom,TO_CORE,fto,160);

		pid_t p,c;

		if(!a.valid())
			printf("ERROR: Create: OOM\n");
		else if(
			(res=CreateProcess(p,(void*(*)(void*))prod,(void*)ffrom,PROC_DEFAULT_TIMESLICE,PROC_DEFAULT_STACK,FROM_CORE))||
			(res=SetProcessFlags(p,PROC_FLAG_JOINABLE,FROM_CORE))||
			(res=StartProcess(p,FROM_CORE))||
			(res=CreateProcess(c,(void*(*)(void*))cons,(void*)fto,PROC_DEFAULT_TIMESLICE,PROC_DEFAULT_STACK,TO_CORE))||
			(res=SetProcessFlags(c,PROC_FLAG_JOINABLE,TO_CORE))||
			(res=StartProcess(c,TO_CORE))||
			(res=WaitProcess(p,NULL,FROM_CORE))||
			(res=WaitProcess(c,NULL,TO_CORE)))
			printf("ERROR: processes: %d\n",res);
	}
	// fifo should be out of scope here, expect destructors
	
	printf("done\n");
	return 0;
}

