#include <helix.h>

#define ITERATIONS	100

#define print_obj(o) printf("shared object: %p (0x%lx)\n",&(o),sizeof(o))
#define check(o,val)														\
	do{																		\
		mc_entry_x(o);														\
		int _tmp=(int)o;													\
		mc_exit_x(o);														\
		if(unlikely(_tmp!=(val))){											\
			usleep(50000*getgpid());										\
			printf("TEST FAILED %d@%d: expected %d, got %d\n",getpid(),GetProcID(),(val),_tmp);		\
			abort();														\
		}																	\
	}while(0)

pthread_barrier_t b=PTHREAD_BARRIER_INITIALIZER;

static void bar(){
	int res=pthread_barrier_wait(&b);
	if(unlikely(res!=0&&res!=PTHREAD_BARRIER_SERIAL_THREAD)){
		usleep(50000*getgpid());
		printf("barrier %d@%d failed: %d\n",getpid(),GetProcID(),res);
		abort();
	}
}

static void StartSlaves(int num,void*(*f)(void*),void* arg=NULL){
	int res=0;
	if((res=pthread_barrier_destroy(&b)))
		goto abort;
	if((res=pthread_barrier_init(&b,NULL,num+1)))
		goto abort;
	for(int i=0;i<num;i++)
		if((res=pthread_create(NULL,NULL,f,arg)))
			goto abort;

	// wait for start
	bar();
	// wait for end
	bar();
	// wait for check
	bar();

	return;

abort:
	printf("Cannot start slaves: error %d\n",res);
	abort();
}

#define EXCLUSIVE_TEST_SLAVES 10
int mc_test ATTR_DATA_ALIGNED;

static void* ExclusiveTestSlave(void* arg){
	bar();

	for(int i=0;i<ITERATIONS;i++){
		mc_entry_x(mc_test);
		printf("mc_test: %d\n",mc_test);
		mc_test++;
		mc_exit_x(mc_test);
	}
	
	bar();
	check(mc_test,EXCLUSIVE_TEST_SLAVES*ITERATIONS+1);
	bar();
	return NULL;
}

static void ExclusiveTest(){
	printf("Exclusive test\n");
	print_obj(mc_test);
	mc_entry_wou(mc_test);
	mc_test=1;
	mc_exit_wou(mc_test);

	StartSlaves(EXCLUSIVE_TEST_SLAVES,ExclusiveTestSlave);
}

#define WRAPPER_TEST_SLAVES 10
MC_TYPE_PRIM(int) mc_test2;
pthread_mutex_t mc_test2_m = PTHREAD_MUTEX_INITIALIZER;

static void* WrapperTestSlave(void* arg){
	bar();

	for(int i=0;i<ITERATIONS;i++){
		VERIFY(pthread_mutex_lock(&mc_test2_m),0);
		printf("mc_test2: %d\n",(int)mc_test2);
		mc_test2++;
		pthread_mutex_unlock(&mc_test2_m);
	}
	
	bar();
	check(mc_test2,WRAPPER_TEST_SLAVES*ITERATIONS+1);
	bar();
	return NULL;
}

static void WrapperTest(){
	printf("Wrapper test\n");
	print_obj(mc_test2);
	mc_test2=1;

	StartSlaves(WRAPPER_TEST_SLAVES,WrapperTestSlave);
}

int main(int argc,char** argv){
#if defined(USE_MC)
	printf("MC: full operation\n");
#elif defined(USE_SW_CACHE_COHERENCY)
	printf("MC: software cache coherency without full MC\n");
#elif defined(ALL_DATA_CACHED)
	printf("MC: no software cache coherency, but all memory cached\n");
#else
	printf("MC: uncached\n");
#endif

	ExclusiveTest();
	WrapperTest();

	sleep(1);
	printf("all done\n");
}

