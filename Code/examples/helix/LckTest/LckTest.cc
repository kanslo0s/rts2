#include <helix.h>

#ifdef USE_DISTRIBUTED_LOCK

#define LCK1	(void*)1
#define LCK2	(void*)2
#define LCK3	(void*)3
#define LCK_END	(void*)1000

#define PROCS			45
#define ITERATIONS		100
//#define SLEEP			(1<<15)

#define DEBUGL(s...)	//printf(s)
#define DEBUG_SLEEP		//usleep(100000)
#define DEBUGS(a)		//outbyte(a)

#define dolock(l) \
	do{ \
		int r=lck_lock((void*)(l)); \
		ASSERT(r<=0,"lock of %p failed: %d",l,r); \
		DEBUGS('A'+GetProcID()); \
		DEBUGL("* Lock    @%03d %p: %d\n",getgpid(),(void*)(l),r); \
	}while(0)
#define dounlock(l) \
	do{ \
		DEBUGS('a'+GetProcID()); \
		int r=lck_unlock((void*)(l)); \
		ASSERT(r<=0,"unlock of %p failed: %d",l,r); \
		DEBUG_SLEEP; \
		DEBUGL("* Unlock  @%03d %p: %d\n",getgpid(),(void*)(l),r); \
	}while(0)
#define dodestroy(l) \
	do{ \
		int r=lck_destroy((void*)(l)); \
		ASSERT(r<=0,"destroy failed: %d",r); \
		DEBUG_SLEEP; \
		DEBUGS('!'); \
		DEBUGL("* Destroy @%03d %p: %d\n",getgpid(),(void*)(l),r); \
	}while(0)

volatile int test=0;

void* p1(void* arg){
	dolock(LCK2);
	dolock(LCK1);
	usleep(100000);
	dounlock(LCK2);
	return NULL;
}

volatile int done[PROCS];
volatile int done_cnt=0;

void* p2(void* arg){
	void* l;
	for(int i=0;i<ITERATIONS;i++){
		for(int j=0;j<100;j++){
			dolock((l=(void*)(rand()%100+10+(int)LCK_END)));
			dounlock(l);
		}
//		for(volatile int z=0;z<1000000;z++);
		dolock(LCK3);
		int t=test+1;
		outbyte('.');
		test=t;
		mc_flush(test);
		dounlock(LCK3);
#ifdef SLEEP
		int s=rand()%SLEEP;
		if(s>SLEEP/4*3)
			usleep(s);
#endif
	}
	dolock(LCK_END);
	done[GetProcID()%PROCS]++;
	done_cnt++;
	mc_flush(done);
	mc_flush(done_cnt);
	return NULL;
}

void* p3(void* arg){
	void* l;
	for(int j=0;j<100;j++){
		dolock((l=(void*)(j+10+(int)LCK_END)));
		dounlock(l);
	}
	for(volatile int z=100000000;z>0;z--);
	return p2(NULL);
}


int main(){

	// simple tests
	printf("* Lock %p: %d\n",LCK1,lck_lock(LCK1));
	printf("* Lock %p: %d\n",LCK2,lck_lock(LCK2));
	printf("* Unlock %p: %d\n",LCK1,lck_unlock(LCK1));
	printf("* Lock %p: %d\n",LCK1,lck_lock(LCK1));
	printf("* Destroy %p: %d\n",LCK1,lck_destroy(LCK1));
	printf("* Lock %p: %d\n",LCK1,lck_lock(LCK1));
	printf("* Unlock %p: %d\n",LCK2,lck_unlock(LCK2));

/*	int res=0;
	// two processes
	printf("* Lock %p: %d\n",LCK1,lck_lock(LCK1));
	for(int p=0;p<PROCS;p++){
		pthread_create(NULL,NULL,p1,NULL);
		usleep(50000);
	}
	usleep(500000);
	res=lck_unlock(LCK1);
	usleep(100000);
	printf("* Unlock %p: %d\n",LCK1,res);
	printf("* Lock %p: %d\n",LCK2,lck_lock(LCK2));
*/	
	for(int p=0;p<PROCS;p++)
		VERIFY(pthread_create(NULL,NULL,p2,NULL),0);
	usleep(100000);

	while(done_cnt<PROCS){
		usleep(10000000);
		for(int p=0;p<PROCS;p++)
			printf("%d=%d ",p,done[p]);
		printf("\n");
		mc_flush(done);
		mc_flush(done_cnt);
	}
	mc_flush(test);
	printf("\n\ntest=%d\n",test);
	ASSERT(test==PROCS*ITERATIONS,"test error");

	printf("* Done\n");
	return 0;
}

#else

int main(){
	printf("Kernel not configured with distributed lock, aborting...\n");
	return -1;
}

#endif

