#include <helix.h>

int f1(int i){
	sleep(1);
	printf("%d@%d: %d\n",getpid(),GetProcID(),i);
	return 1;
}

void f2(double d,float f){
	sleep(2);
	printf("%d@%d: %f/%f\n",getpid(),GetProcID(),d,f);
}

int main(int argc,char** argv){

	// Syntax: par(function,arg1,...,argn,pthread (optional),core id (optional),stack (optional),timeslice (optional));
	// return value is errno indicating whether the process has been started successfully

	f1(10);
	par(f1,11);
	par(f1,21,NULL,6);
	par(f1,54,NULL,7,PROC_DEFAULT_STACK*2,PROC_DEFAULT_TIMESLICE*2);

	pthread_t pt;
	par(f2,4.0,6.0f,&pt);
	VERIFY(pthread_join(pt,NULL),0);
	printf("done\n");

	return 0;
}

