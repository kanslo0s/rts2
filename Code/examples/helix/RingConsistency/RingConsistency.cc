#include <helix.h>

#ifndef RING_CONSISTENCY
#  pragma message ("Shouldn't RING_CONSISTENCY be set for this example?")
#endif

#define WORKERS		NUM_PROCESSORS
#define ITERATIONS	1000

#define SHOW_PROGRESS() outbyte('A'+GetProcID())

#ifdef RC_ENABLE_DEBUG
pthread_mutex_t print_lck=PTHREAD_MUTEX_INITIALIZER;
#  define DEBUG(a...) do{auto_ret<int,0>(pthread_mutex_lock(&print_lck));printf(a);auto_ret<int,0>(pthread_mutex_unlock(&print_lck));}while(0)
#  define outbyte(...)
#else
#  define DEBUG(...)
#endif

volatile char rc_dummy ATTR_RC;
volatile int rc_test[4] ATTR_RC;

#define dump_array(a)												\
	do{																\
		typeof(*a)* a_=a;											\
		printf("\n" STRINGIFY(a) " @ %p = {",a_);					\
		for(unsigned int i=0;i<sizeof(a)/sizeof(a[0])-1;i++,a_++)	\
			printf(" %4d,",(int)(*a_));								\
		printf(" %4d}\n\n",(int)(*a_));								\
	}while(0)
	

void test1(){
	for(int i=0;i<ITERATIONS;i++){
		rc_entry_x(rc_test[0]);
		rc_test[0]++;
		rc_exit_x(rc_test[0]);
	}
//	SHOW_PROGRESS();
}

void test2(){
	for(int i=0;i<ITERATIONS;i++){
		rc_entry_x(rc_test[0]);
		rc_test[0]++;
		rc_exit_x(rc_test[0]);

		rc_entry_x(rc_test[1]);
		rc_test[1]+=3;
		SHOW_PROGRESS();
		rc_exit_x(rc_test[1]);
	}
}

#define wait_until(poll,value)			\
	do{									\
		typeof(poll) prev=0,next=0;		\
		while((next=(poll))<(value)){	\
			ASSERT(next>=prev,"updates observed out of order: %d %d",(int)prev,(int)next);	\
			prev=next;					\
		}								\
	}while(0)

void simple_test(){
	dump_array(rc_test);

	// test 1
	for(int i=0;i<WORKERS;i++)
		par(test1);
	wait_until(rc_test[0],WORKERS*ITERATIONS);
	dump_array(rc_test);
	ASSERT(rc_test[0]==WORKERS*ITERATIONS,"test 1 failed");
	sleep(1);

	// test 2
	rc_entry_x(rc_test);
	rc_test[0]=0;
	rc_test[1]=1;
	rc_flush(rc_test);
	rc_exit_x(rc_test);
	for(int i=0;i<WORKERS;i++){
		par(test1);
		par(test2);
	}
	wait_until(rc_test[0],WORKERS*ITERATIONS*2);
	dump_array(rc_test);
	rc_entry_x(rc_test[1]);
	ASSERT(rc_test[0]==WORKERS*ITERATIONS*2&&rc_test[1]==WORKERS*ITERATIONS*3+1,"test 2 failed");
	rc_exit_x(rc_test[1]);
	sleep(1);

	printf("All ok\n");
}

int main(int argc,char** argv){
	simple_test();
	return 0;
}
