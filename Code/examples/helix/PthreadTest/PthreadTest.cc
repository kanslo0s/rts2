#include <helix.h>

#define TEST_NR_CORES	7
#define TEST_NR_PROC	(TEST_NR_CORES*(MAX_PROCESSES-4)-1)

#define wait_for(var,val) do{mc_entry_ro(var);while(var!=val){mc_exit_ro(var);outbyte('!');Yield();mc_entry_ro(var);}mc_exit_ro(var);}while(0)

typedef struct {
	pthread_mutex_t m;
	volatile int i;
	volatile int done;
} mutex_test_t;

void* MutexTestSlave(void* arg){
	mutex_test_t *t=(mutex_test_t*)arg;
	int n;

	for(int i=0;i<1000;i++){
		VERIFY(pthread_mutex_lock(&t->m),0);
		mc_flush(t->i);
		n=t->i+i;
		outbyte('>');
		t->i=n;
		mc_flush(t->i);
		pthread_mutex_unlock(&t->m);

		VERIFY(pthread_mutex_lock(&t->m),0);
		mc_flush(t->i);
		n=t->i-i;
		outbyte('<');
		t->i=n;
		mc_flush(t->i);
		pthread_mutex_unlock(&t->m);
	}

	VERIFY(pthread_mutex_lock(&t->m),0);
	mc_flush(t->done);
	t->done++;
	outbyte('.');
	mc_flush(t->done);
	pthread_mutex_unlock(&t->m);
	return NULL;
}

void MutexTest(){
	int ret;
	printf("Starting mutex test...\n");

	mutex_test_t _mt;
	mutex_test_t* mt=&_mt;
//	mutex_test_t* mt=(mutex_test_t*)smalloc(sizeof(mutex_test_t));

	if(!mt){
		printf("Cannot allocate memory for test structure\n");
		return;
	}

	mc_entry_wou(*mt);

	pthread_mutexattr_t attr=PTHREAD_MUTEXATTR_INITIALIZER;
	if(	(ret=pthread_mutexattr_init(&attr))||
		(ret=pthread_mutexattr_protect(&attr,(void*)&mt->i,sizeof(*mt)-sizeof(mt->m)))){
		printf("Cannot initialize mutex attribute: %d\n",ret);
		return;
	}
	if((ret=pthread_mutex_init(&mt->m,&attr))){
		printf("Cannot initialize mutex: %d\n",ret);
		return;
	}

	prf_mem_snapshot();
	prf_mb_snapshot();

	mt->i=0;
	mt->done=0;
	mc_exit_wou(*mt);

	for(int i=0;i<TEST_NR_PROC;i++){
		pthread_t t;
		if((ret=pthread_create(&t,NULL,&MutexTestSlave,mt))){
			printf("Cannot start process %d: %d\n",i,ret);
			abort();
		}else if((ret=pthread_detach(t))){
			printf("Cannot detach process %d: %d\n",t,ret);
			abort();
		}
	}

	wait_for(mt->done,TEST_NR_PROC);

	usleep(100000);
	mc_flush(*mt);
	printf("\nMutex test result: %s (%d)\n",mt->i==0?"PASSED":"FAILED",mt->i);
	pthread_mutex_destroy(&mt->m);
//	sfree(mt);

	prf_mem_dump();
	prf_mb_dump();
}

typedef struct {
	pthread_mutex_t m;
	pthread_cond_t c;
	int i;
} cond_test_t;

void* CondTestSlave(void* arg){
	cond_test_t* t=(cond_test_t*)arg;
	VERIFY(pthread_mutex_lock(&t->m),0);
	t->i+=getgpid();
	int id=getgpid()%26;

	for(int i=0;i<30;i++){
		VERIFY(pthread_cond_wait(&t->c,&t->m),0);
		int v=t->i;
		outbyte('A'+id);
		t->i=v+1;
		pthread_mutex_unlock(&t->m);
		Yield();
		outbyte('a'+id);
		VERIFY(pthread_mutex_lock(&t->m),0);
	}

	t->i-=30+getgpid();
	outbyte('.');
	pthread_mutex_unlock(&t->m);

	return NULL;
}

void CondTest(){
	int ret;
	printf("Starting condition test...\n");

	cond_test_t* ct=(cond_test_t*)smalloc(sizeof(cond_test_t));
	if(!ct){
		printf("Cannot allocate memory for test structure\n");
		return;
	}

	mc_entry_wou(*ct);

	pthread_mutexattr_t attr=PTHREAD_MUTEXATTR_INITIALIZER;
	if(	(ret=pthread_mutexattr_init(&attr))||
		(ret=pthread_mutexattr_protect(&attr,(void*)&ct->i,sizeof(ct->i)))){
		printf("Cannot initialize mutex attribute: %d\n",ret);
		return;
	}
	if((ret=pthread_mutex_init(&ct->m,&attr))){
		printf("Cannot initialize mutex: %d\n",ret);
		return;
	}
	
	if((ret=pthread_cond_init(&ct->c,NULL))){
		printf("Cannot initialize condition: %d\n",ret);
		return;
	}

	prf_mem_snapshot();
	prf_mb_snapshot();

	ct->i=0;
	mc_exit_wou(*ct);

	for(int i=0;i<TEST_NR_PROC;i++){
		pthread_t t;
		if((ret=pthread_create(&t,NULL,&CondTestSlave,ct)))
			printf("Cannot start process %d: %d\n",i,ret);
		else if((ret=pthread_detach(t)))
			printf("Cannot detach process %d: %d\n",t,ret);
	}

	usleep(1000000);
	for(int i=0;i<10*TEST_NR_PROC;i++){
		usleep(50000);
		outbyte('!');
		if((ret=pthread_cond_signal(&ct->c)))
			printf("Signal failed: %d\n",ret);
	}

	for(int i=0;i<10;i++){
		usleep(1000000);
		outbyte('?');
		if((pthread_cond_broadcast(&ct->c)))
			printf("Broadcast failed: %d\n",ret);
	}
	usleep(1000000);

	for(int i=0;i<10*TEST_NR_PROC;i++){
		usleep(50000);
		outbyte('!');
		if((ret=pthread_cond_signal(&ct->c)))
			printf("Signal failed: %d\n",ret);
	}

	usleep(1000000);
	VERIFY(pthread_mutex_lock(&ct->m),0);
	printf("\nCondition test result: %s (%d)\n",ct->i==0?"PASSED":"FAILED",ct->i);
	pthread_mutex_unlock(&ct->m);
	pthread_mutex_destroy(&ct->m);
	pthread_cond_destroy(&ct->c);
	sfree(ct);

	prf_mem_dump();
	prf_mb_dump();
}

void* ThreadTestSlave(void* arg){
	int id=getgpid()%26;
	outbyte('A'+id);
//	usleep(rand()%500000);
	void* m=calloc(10,4);
	if(!m)outbyte('0');
	free(realloc(m,50));
	outbyte('a'+id);
	printf("+++++++++++++++++++++%d\n",getgpid());
	return NULL;
}

void ThreadTest(){
	int ret;
	pthread_t p[TEST_NR_PROC];
	for(int l=0;l<10;l++){
		GarbageCollect();
		for(int i=0;i<TEST_NR_PROC;i++)
			if((ret=pthread_create(&p[i],NULL,&ThreadTestSlave,NULL))){
				printf("Cannot start process %d: %d\n",i,ret);
				p[i]=0;
			}
		for(int i=0;i<TEST_NR_PROC;i++)
			if(p[i]!=0)
				if((ret=pthread_join(p[i],NULL)))
					printf("Cannot join process %d: %d\n",i,ret);
	}
	usleep(500000);
	printf("Thread test done\n");
}

pthread_barrier_t bar ATTR_DATA_ALIGNED;
volatile bool bar_done ATTR_DATA_ALIGNED;

void* BarrierTestSlave(void* arg){
	int res;
	for(int i=0;i<10000;i++){
		if((res=pthread_barrier_wait(&bar))&&res!=PTHREAD_BARRIER_SERIAL_THREAD){
			printf("Slave %d@%d: Barrier wait error: %d, %s\n",getpid(),GetProcID(),res,strerror(res));
			return (void*)res;
		}
		outbyte('a'+(i%26));
	}
	mc_entry_wou(bar_done);
	bar_done=true;
	mc_exit_wou(bar_done);
	return NULL;
}

void BarrierTest(){
	sleep(1);
	printf("Starting barrier test...\n");
	mc_entry_wou(bar_done);
	bar_done=false;
	mc_exit_wou(bar_done);

	int ret;
	if((ret=pthread_barrier_init(&bar,NULL,TEST_NR_PROC))){
		printf("Cannot create barrier: %d\n",ret);
		return;
	}

	GarbageCollect();
	for(int i=0;i<TEST_NR_PROC;i++)
		if((ret=pthread_create(NULL,NULL,&BarrierTestSlave,NULL))){
			printf("Cannot start process %d: %d\n",i,ret);
			abort();
		}

	sleep(10);
	mc_entry_ro(bar_done);
	while(!bar_done){
		mc_exit_ro(bar_done);
		Yield();
		mc_entry_ro(bar_done);
	}
	mc_exit_ro(bar_done);
}

int main(int argc,char** argv){

	MutexTest();
	Yield();
	CondTest();
	Yield();
	ThreadTest();
	Yield();
	BarrierTest();

	sleep(1);
	printf("\nTests finished\n");
	return 0;
}

