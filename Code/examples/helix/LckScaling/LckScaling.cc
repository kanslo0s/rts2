#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>

#ifdef X86BUILD
#define smalloc(a...) malloc(a)
#else
#include <helix.h>
#ifndef RESERVE_DAEMON_CORE
#error Define RESERVE_DAEMON_CORE
#endif
#endif

float contention_target;
#define STARTUP_ERROR		0.01f
#define CONTENTION_WINDOW	1000
#define CONTENTION_PID_P	200000.0f
#define CONTENTION_PID_I	10000.0f
#define CONTENTION_PID_D	1000.0f
#define STEADY_WORK			0
#define STEADY_NON_WORK		10000
#define MAX_NON_WORK		(20*STEADY_NON_WORK)
#define MEASURE_COUNT		1000
#define MEASURE_STEP		0.025f

#define DEBUG(msg,a...)		printf("%d: " msg "\n",(int)pthread_self(),##a)
#define abs(f)				((f)<0.0f?-(f):(f))
//#define max(a,b)			((a)<(b)?(b):(a))

#define PHASE_START			0
#define PHASE_UNSTABLE		1
#define	PHASE_ALL_STABLE	2
#define PHASE_DONE			3
int phase;

typedef long long int work_t;

typedef struct {
	float prev_err;
	float i;
	int steady;
} work_pid_t;

work_pid_t pid;

static work_t work_pid(float contention,work_pid_t& pid){
	float err_old=pid.prev_err;
	float err=pid.prev_err=contention_target-contention;
	float p=CONTENTION_PID_P*err;
	float i=CONTENTION_PID_I*(pid.i+=err);
	float d=CONTENTION_PID_D*(err-err_old);
	work_t res=(work_t)(p+i+d);
//	DEBUG("pid(%f) -> p=%f,i=%f,d=%f -> %lld",(double)contention,(double)p,(double)i,(double)d,(long long int)res);
	return res;
}

work_t current_work;
pthread_mutex_t m=PTHREAD_MUTEX_INITIALIZER,m2=PTHREAD_MUTEX_INITIALIZER;
unsigned long long int m_free,m_locks;
unsigned int last_proc;
volatile bool busy;
unsigned long long int stable_locks,stable_time;

static void update_pid(){
	float contention=1.0f-(float)m_free/(float)m_locks;
	current_work=work_pid(contention,pid);
	if(abs(contention-contention_target)<STARTUP_ERROR){
		if(pid.steady++==CONTENTION_WINDOW)
			phase=PHASE_ALL_STABLE;
	}else{
		pid.steady=0;
		phase=PHASE_UNSTABLE;
	}
}

void work(work_t w){
	if(w>0){
//		usleep(w);
		for(volatile unsigned int i=w*4;i>0;i--);
//			if(!(i&0xfff))Yield();
	}
}

#define LOCK_FREE	0
#define LOCK_RE		1
#define LOCK_BUSY	2
#define timediff(tv1,tv2)	((tv2.tv_sec-tv1.tv_sec)*1000000+(tv2.tv_usec-tv1.tv_usec))
static int do_lock(){
	struct timeval tv1,tv2;
	// pull code to i-cache
	pthread_mutex_lock(&m2);
	pthread_mutex_unlock(&m2);
	gettimeofday(&tv1,NULL);

	gettimeofday(&tv1,NULL);
	pthread_mutex_lock(&m);
	gettimeofday(&tv2,NULL);

	unsigned int lock_us=timediff(tv1,tv2);
//	DEBUG("lock took %u us",lock_us);

	int res;
	if(last_proc==GetProcID())
		res=LOCK_RE;
	else if(busy)
		res=LOCK_BUSY;
	else
		res=LOCK_FREE;
	last_proc=GetProcID();
	barrier();
	busy=true;

	if(phase==PHASE_ALL_STABLE){
		stable_locks++;
		stable_time+=lock_us;
	}

	return res;
}

static void do_unlock(bool adjust){
	if(adjust)
		update_pid();
	pthread_mutex_unlock(&m);
	barrier();
	busy=false;
}

static void do_contention(work_t w){
	switch(do_lock()){
	case LOCK_BUSY:
		DEBUG("skip busy lock");
		do_unlock(false);
		return;
	case LOCK_RE:
		m_free++;
	case LOCK_FREE:
		m_locks++;
		break;
	default:
		exit(2);
	}

	if(m_locks>CONTENTION_WINDOW){
		m_locks/=2;
		m_free/=2;
	}
	
	work(w);
	do_unlock(true);
}


static void steady_work_loop(){
	DEBUG("Steady work");
	while(phase!=PHASE_DONE){
		do_contention(STEADY_WORK);
		work(STEADY_NON_WORK);
	}
	DEBUG("Done");
}

static void work_loop(){
	DEBUG("Variable work");
	while(phase!=PHASE_DONE){
		do_contention(0);
		work(max(STEADY_NON_WORK,MAX_NON_WORK-current_work));
	}
	DEBUG("Done");
}

static void run_target(float target){
//	DEBUG("running for %f",(double)target);
	contention_target=target;
	phase=PHASE_START;

	pthread_mutex_lock(&m);
	current_work=(work_t)((float)(STEADY_WORK+STEADY_NON_WORK)/contention_target);
	stable_locks=0;
	stable_time=0;
	pid.steady=0;
	pid.prev_err=0.0f;
	pid.i=0.0f;
	pthread_mutex_unlock(&m);
	
	while(stable_locks<MEASURE_COUNT){
		sleep(1);
		float c=abs((1.0f-(float)m_free/(float)m_locks)-contention_target);
		printf("\r%0.3f: ",target);
		fflush(stdout);
		for(float f=c;f>0.0;f-=STARTUP_ERROR/2)
			outbyte('.');
		printf(" %llu                          \r",stable_locks);
		fflush(stdout);
	}
	
	pthread_mutex_lock(&m);
	printf("\r%0.3f: contention=1-re/free=%f  avg=%f\n",(double)target,(double)(1.0f-(double)m_free/(double)m_locks),(double)stable_time/(double)stable_locks);
	pthread_mutex_unlock(&m);
}

int main(int argc,char** argv){
	printf("Dist mutex characterization\n");
	phase=PHASE_START;

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setschedparam(&attr,DAEMON_DEFAULT_TIMESLICE);
	pthread_t slave;
	pthread_create(&slave,&attr,(void*(*)(void*))steady_work_loop,NULL);
	pthread_create(&slave,&attr,(void*(*)(void*))work_loop,NULL);

	for(float f=MEASURE_STEP;f<1.0;f+=MEASURE_STEP)
		run_target(f);

	phase=PHASE_DONE;

	sleep(1);
	DEBUG("Exit");
}


