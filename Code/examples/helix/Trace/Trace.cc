#include <helix.h>
#include <stdio.h>

#define PHASE1 (TRACE_PHASE_RESERVED+0)
#define PHASE2 (TRACE_PHASE_RESERVED+1)
#define PHASE3 (TRACE_PHASE_RESERVED+2)
#define PHASE4 (TRACE_PHASE_RESERVED+3)

static void foo(){
	// set new phase
	int old=TracePhaseChange(PHASE4);

	TraceLog("foo");

	// restore old phase
	TracePhaseChange(old);
}

void fun(){
	TracePhaseChange(PHASE3);
	foo();
}

void fun2(){
	foo();
}

int main(int argc,char** argv){
	printf("Trace example");

	// set phase
	TracePhaseChange(PHASE1);
	// do something
	printf("Phase 1: %d\n",PHASE1);
	usleep(20000);

	// set another phase
	TracePhaseChange(PHASE2);
	// write text to log
	TraceLog("Entering next phase: %d",PHASE2);
	// do something
	printf("Phase 2: %d\n",PHASE1);
	VERIFY(par(fun),0);

	// repeatedly create and destroy process to show effect on VCD
	for(int i=0;i<3;i++){
		pthread_t p;
		VERIFY(par(fun,&p),0);
		foo();
		VERIFY(pthread_join(p,NULL),0);
	}

	VERIFY(par(fun2),0);

	// log end of test
	TraceLog("Done");
	printf("End of example\n");

#ifndef TRACING_DUMP_DEFAULT
	TraceDumpAll();
#endif
	// done
	return 0;
}
