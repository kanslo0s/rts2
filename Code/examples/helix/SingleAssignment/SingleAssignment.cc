//  UNFINISHED!!

#include <helix.h>

using namespace PMC_NS;

class DebugCallback : public SACallback {
public:
	template <typename B,typename T,typename BT> static void readBeforeWrite(B& backend,Var<T,BT>& var) throw() {
		pprintf("Stalling on %p...\n",&var);
		usleep(100000);
		SACallback::readBeforeWrite(backend,var);
	}
	template <typename B,typename T,typename BT> static void justWritten(B& backend,Var<T,BT>& var) throw() {
		pprintf("Wrote %p...\n",&var);
		SACallback::justWritten(backend,var);
	}
};

PMC_SA_SPM(int,x,DebugCallback)

void proc1(){
	usleep(500000);
	pprintf("%d\n",(int)x);
}

void proc2(){
	sleep(1);
	x=10;
}

typedef PMC_SA_SPM_T(int,DebugCallback) fifo_e_t;
FifoMemPool<fifo_e_t,8,spm> fifo PMC_SA_ATTR_SPM;

void prod(){
	for(int i=100;i>=0;i--){
		fifo_e_t *e=new(fifo) fifo_e_t();
//		usleep(100000);
		pprintf("producing %d\n",i);
		*e=i;
	}
}

void cons(){
	sleep(2);
	int i=0;
	do{
		const fifo_e_t &e=fifo.front();
		i=(int)e;
		pprintf("consumed %d\n",i);
		pdelete(fifo) &e;
	}while(i);
}

int main(int argc,char** argv){
/*	par(proc1);
	par(proc1);
	par(proc2);
	sleep(3);*/

	par(prod);
	cons();
	return 0;
}

