#include <helix.h>
#include <par.h>

// Process definitions
void t1() {};
void t2() {};

int main(int argc, char** argv) {
	pthread_t pt1, pt2;
	// Create and start threads
	par(t1, &pt1);	// todo: use verify
	par(t2, &pt2);

	// Wait for threads to terminate
	VERIFY(pthread_join(pt1, NULL),0);
	VERIFY(pthread_join(pt2, NULL),0);
	return 0;
}
