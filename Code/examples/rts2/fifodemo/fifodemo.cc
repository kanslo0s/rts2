#include <helix.h>

CFifo<bool,CFifo<>::w> *wr;	// Write end point of a FIFO
CFifo<bool,CFifo<>::r> *rd;	// Read end point of a FIFO

#define ERREXIT(str) {fprintf(stderr, "Error: " str "\n"); exit(1);}
#define ERREXIT2(str, ...) {fprintf(stderr, "Error: " str "\n", __VA_ARGS__); exit(1);}

void *ping(void *arg) {
	wr->validate();
	while(1) {
		printf("Ping\n");
		wr->push(true);
		sleep(1);
	}
	return NULL;
}

void *pong(void *arg) {
	rd->validate();
	while(1) {
		rd->pop();
		printf("\tPong\n");
	}
	return NULL;
}

int main(int argc, char **argv) {
	pid_t pid0, pid1;

	CFifoPtr<bool> fifo = CFifo<bool>::Create(1, wr, 2, rd, 2);
	if(!fifo.valid()) ERREXIT("Error creating buffer");

	if(int e=CreateProcess(pid0, ping, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 1))
		ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid1, pong, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 2))
			ERREXIT2("Process creation failed: %i", e);
	if(int e=SetProcessFlags(pid0, PROC_FLAG_JOINABLE, 1))
		ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid1, PROC_FLAG_JOINABLE, 2))
			ERREXIT2("While setting process flags: %i", e);
	if(int e=StartProcess(pid0, 1)) ERREXIT2("Could not start ping: %i", e);
	if(int e=StartProcess(pid1, 2)) ERREXIT2("Could not start pong: %i", e);

	// FIFOs are destroyed when the pointers goes out of scope
	if(int e=WaitProcess(pid0, NULL, 1)) ERREXIT2("Waiting on ping %i@%i: %i\n", pid0, 1, e);
	if(int e=WaitProcess(pid1, NULL, 2)) ERREXIT2("Waiting on pong %i@%i: %i\n", pid1, 2, e);
	return 0;
}
