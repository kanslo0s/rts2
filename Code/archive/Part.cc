CFifo<double,CFifo<>::w> *wFIR; // Write end point of a FIFO
CFifo<double,CFifo<>::r> *rFIR; // Read end point of a FIFO
CFifo<double,CFifo<>::w> *wSample;
CFifo<double,CFifo<>::r> *rSample;

void *proc1(void *arg) {
	wFIR->validate();
    rFIR->validate();
    rSample->validate();
    double fir=0;
    double sample=0;
	while(1) {
        sample = rSample->front;
        fir = rFIR->value;
        fir = fir + sample;
		wFIR->push(fir);
		rFIR->pop();
        rSample->pop();
	}
	return NULL;
}