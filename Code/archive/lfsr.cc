#include <stdio.h>
#include <stdint.h>
#include <string.h>

int main(void)
{
	uint16_t startstate = 0xA30Fu;
	uint16_t lfsr       = startstate;
	uint16_t bit;
  unsigned period = 0;

	do
		{
			// ^ = xor, >> n = shift n bits to the right
			bit = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5)) & 1;
      lfsr = (lfsr >> 1) | (bit << 15);
			printf("lfsr state:, %d\n",lfsr);
			printf("period: %d\n", period);
			++period;
	} while (lfsr != startstate);

	return 0;
}
