//https://www.youtube.com/watch?v=fdCAsZkjpSg   <<  ring buffer
//https://sestevenson.wordpress.com/implementation-of-fir-filtering-in-c-part-1/
//#include <helix.h>
//#include <render/render.h>

#include <stdio.h>
#include <stdint.h>
#include <string.h>

CFifo<double,CFifo<>::w> *wFIR; // Write end point of a FIFO
CFifo<double,CFifo<>::r> *rFIR; // Read end point of a FIFO
CFifo<double,CFifo<>::w> *wSample;
CFifo<double,CFifo<>::r> *rSample;
 //////////////////////////////////////////////////////////////
 //Processor Code Definitions
//////////////////////////////////////////////////////////////
 #define ERREXIT(s) { perror(s); exit(1); }


//////////////////////////////////////////////////////////////
//  Filter Code Definitions
//////////////////////////////////////////////////////////////
 
// maximum number of inputs that can be handled
// in one function call
#define MAX_INPUT_LEN   80
// maximum length of filter than can be handled
#define MAX_FLT_LEN     63
// buffer to hold all of the input samples
#define BUFFER_LEN      (MAX_FLT_LEN - 1 + MAX_INPUT_LEN)
 
 // bandpass filter centred around 1000 Hz
// sampling rate = 8000 Hz
 
#define FILTER_LEN  63
double coeffs[ FILTER_LEN ] =
{
  -0.0448093,  0.0322875,   0.0181163,   0.0087615,   0.0056797,
   0.0086685,  0.0148049,   0.0187190,   0.0151019,   0.0027594,
  -0.0132676, -0.0232561,  -0.0187804,   0.0006382,   0.0250536,
   0.0387214,  0.0299817,   0.0002609,  -0.0345546,  -0.0525282,
  -0.0395620,  0.0000246,   0.0440998,   0.0651867,   0.0479110,
   0.0000135, -0.0508558,  -0.0736313,  -0.0529380,  -0.0000709,
   0.0540186,  0.0766746,   0.0540186,  -0.0000709,  -0.0529380,
  -0.0736313, -0.0508558,   0.0000135,   0.0479110,   0.0651867,
   0.0440998,  0.0000246,  -0.0395620,  -0.0525282,  -0.0345546,
   0.0002609,  0.0299817,   0.0387214,   0.0250536,   0.0006382,
  -0.0187804, -0.0232561,  -0.0132676,   0.0027594,   0.0151019,
   0.0187190,  0.0148049,   0.0086685,   0.0056797,   0.0087615,
   0.0181163,  0.0322875,  -0.0448093
};
 
 //Processor functions
 void *proc1(void *arg) {
	wFIR->validate(1);
    double coeffs1 = -0.0448093;
    double coeffs2 = -0.0448093;
    double coeffs3 = -0.0448093;
    double coeffs4 = -0.0448093;
    double sample = arg;
	while(1) {
		printf("Ping1\n");
        coeffs1 * sample
		wFIR->push(true);
		sleep(1);
		rFIR->pop();
	}
	return NULL;
}

void *proc2(void *arg) {
	wFIR->validate(2);
    double coeffs1 = -0.0448093;
    double coeffs2 = -0.0448093;
    double coeffs3 = -0.0448093;
    double coeffs4 = -0.0448093;
    double sample = arg;
	while(1) {
		printf("\tPing2\n");
		wFIR->push(true);
		sleep(1);
		rFIR->pop();
	}
	return NULL;
}

void *proc3(void *arg) {
	wFIR->validate(3);
    double coeffs1 = -0.0448093;
    double coeffs2 = -0.0448093;
    double coeffs3 = -0.0448093;
    double coeffs4 = -0.0448093;
    double sample = arg;
	while(1) {
		printf("\t\tPing3\n");
		wFIR->push(true);
		sleep(1);
		rFIR->pop();
	}
	return NULL;
}

void *proc4(void *arg) {
	wFIR->validate(4);
    double coeffs1 = -0.0448093;
    double coeffs2 = -0.0448093;
    double coeffs3 = -0.0448093;
    double coeffs4 = -0.0448093;
    double sample = arg;
	while(1) {
		printf("\t\t\tPing4\n");
		wFIR->push(true);
		sleep(1);
		rFIR->pop();
	}
	return NULL;
}
 void *proc5(void *arg) {
	wFIR->validate(5);
    double coeffs1 = -0.0448093;
    double coeffs2 = -0.0448093;
    double coeffs3 = -0.0448093;
    double coeffs4 = -0.0448093;
    double sample = arg;
	while(1) {
		printf("Ping1\n");
		wFIR->push(true);
		sleep(1);
		rFIR->pop();
	}
	return NULL;
}

void *proc6(void *arg) {
	wFIR->validate(6);
    double coeffs1 = -0.0448093;
    double coeffs2 = -0.0448093;
    double coeffs3 = -0.0448093;
    double coeffs4 = -0.0448093;
    double sample = arg;
	while(1) {
		printf("\tPing2\n");
		wFIR->push(true);
		sleep(1);
		rFIR->pop();
	}
	return NULL;
}

void *proc7(void *arg) {
	wFIR->validate(7);
    double coeffs1 = -0.0448093;
    double coeffs2 = -0.0448093;
    double coeffs3 = -0.0448093;
    double coeffs4 = -0.0448093;
    double sample = arg;
	while(1) {
		printf("\t\tPing3\n");
		wFIR->push(true);
		sleep(1);
		rFIR->pop();
	}
	return NULL;
}

void *proc8(void *arg) {
	wFIR->validate(8);
    double coeffs1 = -0.0448093;
    double coeffs2 = -0.0448093;
    double coeffs3 = -0.0448093;
    double coeffs4 = -0.0448093;
    double sample = arg;
	while(1) {
		printf("\t\t\tPing4\n");
		wFIR->push(true);
		sleep(1);
		rFIR->pop();
	}
	return NULL;
} 
void *proc9(void *arg) {
	wFIR->validate(9);
    double coeffs1 = -0.0448093;
    double coeffs2 = -0.0448093;
    double coeffs3 = -0.0448093;
    double coeffs4 = -0.0448093;
    double sample = arg;
	while(1) {
		printf("\t\t\tPing4\n");
		wFIR->push(true);
		sleep(1);
		rFIR->pop();
	}
	return NULL;
}  

 
 
 
// array to hold input samples
double insamp[ BUFFER_LEN ];
 
// FIR init
void firFloatInit( void )
{
    memset( insamp, 0, sizeof( insamp ) );
}
 
// the FIR filter function
void firFloat( double *coeffs, double *input, double *output,
       int length, int filterLength )
{
    double acc;     // accumulator for MACs
    double *coeffp; // pointer to coefficients
    double *inputp; // pointer to input samples
    int n;
    int k;
 
    // put the new samples at the high end of the buffer
    memcpy( &insamp[filterLength - 1], input,
            length * sizeof(double) );
 
    // apply the filter to each input sample
    for ( n = 0; n < length; n++ ) {
        // calculate output n
        coeffp = coeffs;
        inputp = &insamp[filterLength - 1 + n];
        acc = 0;
        for ( k = 0; k < filterLength; k++ ) {
            acc += (*coeffp++) * (*inputp--);
        }
        output[n] = acc;
    }
    // shift input samples back in time for next time
    memmove( &insamp[0], &insamp[length],
            (filterLength - 1) * sizeof(double) );
 
}
 

 
void intToFloat( int16_t *input, double *output, int length )
{
    int i;
 
    for ( i = 0; i < length; i++ ) {
        output[i] = (double)input[i];
    }
}
 
void floatToInt( double *input, int16_t *output, int length )
{
    int i;
 
    for ( i = 0; i < length; i++ ) {
        if ( input[i] > 32767.0 ) {
            input[i] = 32767.0;
        } else if ( input[i] < -32768.0 ) {
            input[i] = -32768.0;
        }
        // convert
        output[i] = (int16_t)input[i];
    }
}
 
// number of samples to read per loop
#define SAMPLES   80

/*int main(int argc, char **argv ){ */
void main()
{
    int size;
    int16_t input[SAMPLES];
    int16_t output[SAMPLES];
    double floatInput[SAMPLES];
    double floatOutput[SAMPLES];
    FILE   *in_fid;
    FILE   *out_fid;
    
    //processor part
    pid_t pid1, pid2, pid3, pid4, pid5, pid6, pid7, pid8, pid9, pidS1;

	CFifoPtr<double> fifo1 = CFifo<double>::Create(1, wFIR, 2, rFIR, 9);
	CFifoPtr<double> fifo2 = CFifo<double>::Create(2, wFIR, 3, rFIR, 1);
	CFifoPtr<double> fifo3 = CFifo<double>::Create(3, wFIR, 4, rFIR, 2);
	CFifoPtr<double> fifo4 = CFifo<double>::Create(4, wFIR, 5, rFIR, 3);
    CFifoPtr<double> fifo5 = CFifo<double>::Create(5, wFIR, 6, rFIR, 4);
	CFifoPtr<double> fifo6 = CFifo<double>::Create(6, wFIR, 7, rFIR, 5);
	CFifoPtr<double> fifo7 = CFifo<double>::Create(7, wFIR, 8, rFIR, 6);
	CFifoPtr<double> fifo8 = CFifo<double>::Create(8, wFIR, 9, rFIR, 7);
    CFifoPtr<double> fifo9 = CFifo<double>::Create(9, wFIR, 1, rFIR, 8);

    CFifoPtr<double> fifoS1 = CFifo<double>::Create(1, wSample, 2, rSample, 2);
   
	if(!fifo1.valid()) ERREXIT("Error creating buffer");
	if(!fifo2.valid()) ERREXIT("Error creating buffer");
	if(!fifo3.valid()) ERREXIT("Error creating buffer");
	if(!fifo4.valid()) ERREXIT("Error creating buffer");
    if(!fifo5.valid()) ERREXIT("Error creating buffer");
	if(!fifo6.valid()) ERREXIT("Error creating buffer");
	if(!fifo7.valid()) ERREXIT("Error creating buffer");
	if(!fifo8.valid()) ERREXIT("Error creating buffer");
    if(!fifo9.valid()) ERREXIT("Error creating buffer");
    //NULL vervangen door sample
	if(int e=CreateProcess(pid1, proc1, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 1)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid2, proc2, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 2)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid3, proc3, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 3)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid4, proc4, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 4)) ERREXIT2("Process creation failed: %i", e);
    if(int e=CreateProcess(pid5, proc5, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 5)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid6, proc6, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 6)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid7, proc7, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 7)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid8, proc8, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 8)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid9, proc9, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 9)) ERREXIT2("Process creation failed: %i", e);

	if(int e=SetProcessFlags(pid1, PROC_FLAG_JOINABLE, 1)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid2, PROC_FLAG_JOINABLE, 2)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid3, PROC_FLAG_JOINABLE, 3)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid4, PROC_FLAG_JOINABLE, 4)) ERREXIT2("While setting process flags: %i", e);
    if(int e=SetProcessFlags(pid5, PROC_FLAG_JOINABLE, 5)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid6, PROC_FLAG_JOINABLE, 6)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid7, PROC_FLAG_JOINABLE, 7)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid8, PROC_FLAG_JOINABLE, 8)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid9, PROC_FLAG_JOINABLE, 9)) ERREXIT2("While setting process flags: %i", e);


	if(int e=StartProcess(pid1, 1)) ERREXIT2("Could not start ping: %i", e);
	if(int e=StartProcess(pid2, 2)) ERREXIT2("Could not start pong: %i", e);
	if(int e=StartProcess(pid3, 3)) ERREXIT2("Could not start pong: %i", e);
	if(int e=StartProcess(pid4, 4)) ERREXIT2("Could not start pong: %i", e);
	if(int e=StartProcess(pid5, 5)) ERREXIT2("Could not start ping: %i", e);
	if(int e=StartProcess(pid6, 6)) ERREXIT2("Could not start pong: %i", e);
	if(int e=StartProcess(pid7, 7)) ERREXIT2("Could not start pong: %i", e);
	if(int e=StartProcess(pid8, 8)) ERREXIT2("Could not start pong: %i", e);
    if(int e=StartProcess(pid9, 9)) ERREXIT2("Could not start pong: %i", e);

        
	// FIFOs are destroyed when the pointers goes out of scope
	if(int e=WaitProcess(pid1, NULL, 1)) ERREXIT2("Waiting on ping %i@%i: %i\n", pid1, 1, e);
	if(int e=WaitProcess(pid2, NULL, 2)) ERREXIT2("Waiting on ping %i@%i: %i\n", pid2, 2, e);
	if(int e=WaitProcess(pid3, NULL, 3)) ERREXIT2("Waiting on pong %i@%i: %i\n", pid3, 3, e);
	if(int e=WaitProcess(pid4, NULL, 4)) ERREXIT2("Waiting on pong %i@%i: %i\n", pid4, 4, e);
	if(int e=WaitProcess(pid5, NULL, 5)) ERREXIT2("Waiting on ping %i@%i: %i\n", pid5, 5, e);
	if(int e=WaitProcess(pid6, NULL, 6)) ERREXIT2("Waiting on ping %i@%i: %i\n", pid6, 6, e);
	if(int e=WaitProcess(pid7, NULL, 7)) ERREXIT2("Waiting on pong %i@%i: %i\n", pid7, 7, e);
	if(int e=WaitProcess(pid8, NULL, 8)) ERREXIT2("Waiting on pong %i@%i: %i\n", pid8, 8, e);
	if(int e=WaitProcess(pid9, NULL, 9)) ERREXIT2("Waiting on pong %i@%i: %i\n", pid9, 9, e);    
    
    
    // open the input waveform file
    in_fid = fopen( "input.raw", "rb" );
    if ( in_fid == 0 ) {
        printf("couldn't open input.pcm");
        return;
    }
 
    // open the output waveform file
    out_fid = fopen( "outputFloat.raw", "wb" );
    if ( out_fid == 0 ) {
        printf("couldn't open outputFloat.pcm");
        return;
    }
 
    // initialize the filter
    firFloatInit();
 
    // process all of the samples
    do {
        // read samples from file
        size = fread( input, sizeof(int16_t), SAMPLES, in_fid );
        // convert to doubles
        intToFloat( input, floatInput, size );
        // perform the filtering
        firFloat( coeffs, floatInput, floatOutput, size,
               FILTER_LEN );
        // convert to ints
        floatToInt( floatOutput, output, size );
        // write samples to file
        fwrite( output, sizeof(int16_t), size, out_fid );
    } while ( size != 0 );
 
    fclose( in_fid );
    fclose( out_fid );
 
   // return 0;
}
