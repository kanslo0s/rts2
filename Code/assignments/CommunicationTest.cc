#include <helix.h>

CFifo<bool,CFifo<>::w> *wr1; // Write end point of a FIFO
CFifo<bool,CFifo<>::r> *rd1; // Read end point of a FIFO
CFifo<bool,CFifo<>::w> *wr2; // Write end point of a FIFO
CFifo<bool,CFifo<>::r> *rd2; // Read end point of a FIFO
CFifo<bool,CFifo<>::w> *wr3; // Write end point of a FIFO
CFifo<bool,CFifo<>::r> *rd3; // Read end point of a FIFO
#define ERREXIT(str) {fprintf(stderr, "Error: " str "\n"); exit(1);}
#define ERREXIT2(str, ...) {fprintf(stderr, "Error: " str "\n",__VA_ARGS__); exit(1);}

void *ping(void *arg) {
	wr1->validate();
    bool x = false;
    //double foo[2];
	while(1) {
        //foo[0] = 4.5;
        //foo[1] = 3.23;
		printf("\t\t\tPing1 pushing\n");
		wr1->push(true);
        //wr1->push(foo);
		sleep(1);
	}
	return NULL;
}

void *ping2(void *arg) {
	wr2->validate();
    rd1->validate();
    bool x = false;
    //double foo[2];
	while(1) {
        //foo = rd1->front();
        x = rd1->front();
		//printf("\t\t\tPing2: %lf and %lf\n", foo[0],foo[2]);
		printf("\t\t\tPing2: %d\n", x);
        //foo = {1,2};
        //wr2->push(foo);
		wr2->push(false);
		sleep(1);
		rd1->pop();
	}
	return NULL;
}

void *ping3(void *arg) {
	wr3->validate();
    rd2->validate();
    bool x = false;
    double foo[2];
	while(1) {
        x = rd2->front();
        //foo = rd2->front();
		//printf("\t\t\tPing2: %lf and %lf\n", foo[0],foo[2]);
        printf("\t\t\tPing3: %d\n", x);
		wr3->push(true);
        //foo = {4,5};
        //wr3->push(foo);
		sleep(1);
		rd2->pop();
	}
	return NULL;
}

void *ping4(void *arg) {
    rd3->validate();
    bool x = false;
    //double foo[2];
	while(1) {
        //foo = rd3->front();
        x = rd3->front;
		//printf("\t\t\tPing2: %lf and %lf\n", foo[0],foo[2]);
		printf("\t\t\tPing4: %d\n", x);
		sleep(1);
		rd3->pop();
	}
	return NULL;
}

int main(int argc, char **argv) {
	pid_t pid0, pid1, pid2, pid3;

	CFifoPtr<bool> fifo1 = CFifo<bool>::Create(1, wr1, 2, rd1, 5);
	CFifoPtr<bool> fifo2 = CFifo<bool>::Create(2, wr2, 3, rd2, 5);
	CFifoPtr<bool> fifo3 = CFifo<bool>::Create(3, wr3, 4, rd3, 5);

	if(!fifo1.valid()) ERREXIT("Error creating buffer");
	if(!fifo2.valid()) ERREXIT("Error creating buffer");
	if(!fifo3.valid()) ERREXIT("Error creating buffer");

	if(int e=CreateProcess(pid0, ping, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 1)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid1, ping2, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 2)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid2, ping3, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 3)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid3, ping4, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 4)) ERREXIT2("Process creation failed: %i", e);


	if(int e=SetProcessFlags(pid0, PROC_FLAG_JOINABLE, 1)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid1, PROC_FLAG_JOINABLE, 2)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid2, PROC_FLAG_JOINABLE, 3)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid3, PROC_FLAG_JOINABLE, 4)) ERREXIT2("While setting process flags: %i", e);


	if(int e=StartProcess(pid0, 1)) ERREXIT2("Could not start ping: %i", e);
	if(int e=StartProcess(pid1, 2)) ERREXIT2("Could not start pong: %i", e);
	if(int e=StartProcess(pid2, 3)) ERREXIT2("Could not start pong: %i", e);
	if(int e=StartProcess(pid3, 4)) ERREXIT2("Could not start pong: %i", e);

	// FIFOs are destroyed when the pointers goes out of scope
	if(int e=WaitProcess(pid0, NULL, 1)) ERREXIT2("Waiting on ping %i@%i: %i\n", pid0, 1, e);

	if(int e=WaitProcess(pid1, NULL, 2)) ERREXIT2("Waiting on ping %i@%i: %i\n", pid1, 2, e);

	if(int e=WaitProcess(pid2, NULL, 3)) ERREXIT2("Waiting on pong %i@%i: %i\n", pid2, 3, e);

	if(int e=WaitProcess(pid3, NULL, 4)) ERREXIT2("Waiting on pong %i@%i: %i\n", pid3, 4, e);

	return 0;
}

