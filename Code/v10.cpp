#include <helix.h>
#include <math.h>
#include <render/render.h>
#include <time.h>

struct firVstruct{                                                                                  //struct to communicate between processors for voice (V)
	double sample;
	double outputL;
    double output2;
    double output3;
    double output4;
    double output5;
    double output6;
    double output7;
    double outputH;
} firV1, firV2, firV3, firV4;
struct firOstruct{                                                                                  //struct to communicate between processors for overlapping (O) sound
	double sample;
	double outputL;
    double output2;
    double output3;
    double output4;
    double output5;
    double output6;
    double output7;
    double outputH;
} firO1, firO2, firO3, firO4;
struct outpStruct{                                                                                  //struct to communicate between the last fir processors, evelope follower(env) and VCA.
    double outputL;
    double output2;
    double output3;
    double output4;
    double output5;
    double output6;
    double output7;
    double outputH;
} firVenv, envInternal, envInternalTemp, firOVCA, VCAfirInternal, VCAenvInternal;
CFifo<double,CFifo<>::w> *wSampleV;                                                                 //write end point of a CFIFO between sampler voice and FIR1 voice
CFifo<double,CFifo<>::r> *rSampleV;                                                                 //Read end point of a CFIFO between sampler voice and FIR1 voice
CFifo<firVstruct,CFifo<>::w> *wFIRV1;                                                               //Write end point of a CFIFO between FIR1 voice and FIR2 voice
CFifo<firVstruct,CFifo<>::r> *rFIRV1;                                                               //Read end point of a CFIFO between FIR1 voice and FIR2 voice
CFifo<firVstruct,CFifo<>::w> *wFIRV2;                                                               //Write end point of a CFIFO between FIR2 voice and FIR3 voice
CFifo<firVstruct,CFifo<>::r> *rFIRV2;                                                               //Read end point of a CFIFO between FIR2 voice and FIR3 voice
CFifo<firVstruct,CFifo<>::w> *wFIRV3;                                                               //Write end point of a CFIFO between FIR3 voice and FIR4 voice
CFifo<firVstruct,CFifo<>::r> *rFIRV3;                                                               //Read end point of a CFIFO between FIR3 voice and FIR4 voice
CFifo<double,CFifo<>::w> *wSampleO;                                                                 //write end point of a CFIFO between sampler overlapping sound and FIR1 overlapping sound   
CFifo<double,CFifo<>::r> *rSampleO;                                                                 //Read end point of a CFIFO between sampler overlapping sound and FIR1 overlapping sound
CFifo<firOstruct,CFifo<>::w> *wFIRO1;                                                               //Write end point of a CFIFO between FIR1 overlapping sound and FIR2 overlapping sound
CFifo<firOstruct,CFifo<>::r> *rFIRO1;                                                               //Read end point of a CFIFO between FIR1 overlapping sound and FIR2 overlapping sound
CFifo<firOstruct,CFifo<>::w> *wFIRO2;                                                               //Write end point of a CFIFO between FIR2 overlapping sound and FIR3 overlapping sound
CFifo<firOstruct,CFifo<>::r> *rFIRO2;                                                               //Read end point of a CFIFO between FIR2 overlapping sound and FIR3 overlapping sound
CFifo<firOstruct,CFifo<>::w> *wFIRO3;                                                               //Write end point of a CFIFO between FIR3 overlapping sound and FIR4 overlapping sound
CFifo<firOstruct,CFifo<>::r> *rFIRO3;                                                               //Read end point of a CFIFO between FIR3 overlapping sound and FIR4 overlapping sound
CFifo<outpStruct,CFifo<>::w> *wEnv;                                                                 //write end point of a CFIFO between FIR voice and envelope follower
CFifo<outpStruct,CFifo<>::r> *rEnv;                                                                 //Read end point of a CFIFO between FIR voice and envelope follower
CFifo<outpStruct,CFifo<>::w> *wVCAFIR;                                                              //write end point of a CFIFO between VCA and FIR overlapping sound                                 
CFifo<outpStruct,CFifo<>::r> *rVCAFIR;                                                              //Read end point of a CFIFO between VCA and FIR overlapping sound          
CFifo<outpStruct,CFifo<>::w> *wVCAEnv;                                                              //write end point of a CFIFO between VCA and envelope follower
CFifo<outpStruct,CFifo<>::r> *rVCAEnv;                                                              //Read end point of a CFIFO between VCA and envelope follower
#define ERREXIT(str) {fprintf(stderr, "Error: " str "\n"); exit(1);}                                //pre-defined exits when an error occurs during initializing/starting/validating of processors and buffers
#define ERREXIT2(str, ...) {fprintf(stderr, "Error: " str "\n",__VA_ARGS__); exit(1);}
#define NUMBER_OF_COEFFICIENTS_VOICE	16                                                          //each FIR processor (voice) contains atleast this amount of coefficients        
#define NUMBER_OF_COEFFICIENTS_OVERLAP	16                                                          //each FIR processor (overlapping sound) contains atleast this amount of coefficients
#define PIXEL_SIZE						200                                                         //the size of an array, which stores the output of FIRO and sumFIR that are afterwards drawn on the screen
#define SAMPLE_FREQUENCY				63                                                   	    //time that the sampler should sleep; 62.5us = 16kHz.
#define X_AXIS_SCALING			        3		                                                    //scale the width of the x axis between points on the screen
#define FREQ_HIGH1						10                                                          //this case we provice a sqaure wave, so the number of samples that are high every period
#define FREQ_TOT1						20                                                          //number of samples in one period    
#define FREQ_HIGH2						80                                                          //this case we provice a sqaure wave, so the number of samples that are high every period
#define FREQ_TOT2						160                                                         //number of samples in one period
#define TICKS_PER_RENDER				20                                                          //number of iterations to wait before drawing on screen (soft real-time)
#define SCALE_OUTPUT					1                                                           //scale the output that will be drawn on the screen                                  
//Processor functions for voice
void *procSamplerV(void *arg) {                                                                     //the sampler is a replacement for the ADC in order to simulate a working vocoder
    wSampleV->validate();                                                                           //validate the buffer
	double sample = 10;                                                                             //amplitude of the sample
	int tick = 0;                                                                                   //count how many iterations have passed; to create a sqaure wave
	int timeElapsed = 0;                                                                            //count how many iterations have passed; changing to a different frequency 
	int freq_high = FREQ_HIGH2;                                                                     //set its first frequency to 100 Hz  (SAMPLE_FREQUENCY/FREQ_HIGH = 160kHz / 160 = 100Hz)
	int freq_tot = FREQ_TOT2;
	int j = 1;                                                                                      //possitive or negative in order to create a sqaure wave
	while(true){
		sample = 10 * j;                                    
		wSampleV->push(sample);                                                                     //push the sample to FIRV1 (the first FIR filter)
        if(tick >= freq_high)                                                                       //creation of the sqaure wave
        {
            j = -1;
            if(tick >= freq_tot)
            {
                j = 1;
                tick = 0;
            }
        }
        if(timeElapsed >= 500)                                                                      //changing to another sample frequency, for demonstration purpose only
        {   
            freq_tot = FREQ_TOT1;
            freq_high = FREQ_HIGH1;
            timeElapsed = 0;
        }
        tick++;                     
        timeElapsed++;

		usleep(SAMPLE_FREQUENCY);                                                                   //to simulate the ADC processing speed 
	}
	                                          

	return NULL;
}

void *procFirV1(void *arg) {                                                                        //The first FIR filter, filtering the signal of voice        
	rSampleV->validate();                                                                           //validate the buffers
	wFIRV1->validate();
                                                                                                    //the first 16 coefficients of 8 different FIR filters, filtering different frequencies:
	double coeffL[NUMBER_OF_COEFFICIENTS_VOICE] = {0.002009797714883, 0.001589776213765, 0.001129629658942, 0.000542108161679, -0.000266129923033, -0.001383076815682, -0.002879439797339, -0.004797301628876, -0.007140468915944, -0.009867507627148, -0.012888282408866, -0.016064556546795, -0.019214891797955, -0.022123736645131, -0.024554236028881, -0.026263964657108};
	double coeff2[NUMBER_OF_COEFFICIENTS_VOICE] = {-0.001594342843359, -0.000694934711938, 0.000352455259332, 0.001646123177970, 0.003249707196761, 0.005159343632905, 0.007280084861265, 0.009416827453657, 0.011283005449624, 0.012527687586138, 0.012778841064371, 0.011697802698854, 0.009037840152308, 0.004698435913559, -0.001233186790668, -0.008460677878210};
	double coeff3[NUMBER_OF_COEFFICIENTS_VOICE] = {0.001681702677228, 0.000841140019805, -0.000258387944813, -0.001666490516444, -0.003359831600897, -0.005181857429666, -0.006813806540355, -0.007796102179399, -0.007606934274455, -0.005788695810532, -0.002096999075264, 0.003364339448142, 0.010062071439526, 0.017038832776553, 0.023021373030660, 0.026625216891559};
	double coeff4[NUMBER_OF_COEFFICIENTS_VOICE] = {-0.001904979513933, -0.002143695820440, -0.001915461910994, -0.001053741878834, 0.000547301109234, 0.002783340573114, 0.005207876669386, 0.007014909392857, 0.007206128780592, 0.004939048090860, -0.000040915964557, -0.007054094417358, -0.014408586506634, -0.019707329678306, -0.020524352683802, -0.015278641133925};
	double coeff5[NUMBER_OF_COEFFICIENTS_VOICE] = {-0.000749265947793, -0.000056022594752, 0.000985790736354, 0.002051302525254, 0.002556138503085, 0.001816248082992, -0.000530418575543, -0.004012538631502, -0.007106170011957, -0.007668951547568, -0.004055096680685, 0.003565916374712, 0.012514992391961, 0.018247379073870, 0.016401804083837, 0.005427433474339};
	double coeff6[NUMBER_OF_COEFFICIENTS_VOICE] = {0.000032766768318, 0.000934960898828, 0.001411739621265, 0.000589942226046, -0.001456357651484, -0.003137985152701, -0.002341935061096, 0.001239654476244, 0.004852564849775, 0.004775799239451, 0.000417907441575, -0.004274608590728, -0.004765271639935, -0.001405715398890, 0.000610299053879, -0.001841050120118};
	double coeff7[NUMBER_OF_COEFFICIENTS_VOICE] = {0.000581384922188, 0.000998863216021, -0.000330683686573, -0.001942678450245, -0.000773965280287, 0.002618520697641, 0.002896541982954, -0.001971789323416, -0.005111219515949, -0.000512840852606, 0.005483895551463, 0.003320178791202, -0.003043302441441, -0.003181632702084, 0.000166228510538, -0.001839669502584};
	double coeffH[NUMBER_OF_COEFFICIENTS_VOICE] = {0.001222154680657, -0.000864228597482, -0.001248028815229, 0.001799246285214, 0.000274891598705, -0.001230351661134, 0.000059447854077, -0.001148499469490, 0.003043796655958, 0.001730068159807, -0.008157230243151, 0.002643416033895, 0.008234658656892, -0.006062256870050, -0.001777441649843, -0.001765070724298};
                                                                                                    //16 samples needed to do FIR calculations:
    double samples[NUMBER_OF_COEFFICIENTS_VOICE]= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	firV1.outputL = 0;                                                                              //reset output
	firV1.output2 = 0;
	firV1.output3 = 0;
	firV1.output4 = 0;
	firV1.output5 = 0;
	firV1.output6 = 0;
	firV1.output7 = 0;
	firV1.outputH = 0;
	/*static struct timespec prevTime;                                                              //COMPUTATION TIME MEASUREMENT ONLY
	struct timespec currentTime;
    double diff = 0;*/
	while(1) {
		firV1.sample = samples[NUMBER_OF_COEFFICIENTS_VOICE-1];		                                //the last sample before shifting, used as first sample for next processor
        memmove(samples + 1, samples, (NUMBER_OF_COEFFICIENTS_VOICE - 1)*sizeof(double));           //shift samples to the right
		samples[0] = rSampleV->front();                                                             //read sample from buffer and place in first element.
		//if( clock_gettime( CLOCK_REALTIME, &prevTime) == -1 ) { perror( "clock gettime" ); }      //COMPUTATION TIME MEASUREMENT ONLY
        for(int i = 0;i<NUMBER_OF_COEFFICIENTS_VOICE;i++)                                           //FIR calculations
		{
			firV1.outputL += samples[i]*coeffL[i];		
			firV1.output2 += samples[i]*coeff2[i];		
			firV1.output3 += samples[i]*coeff3[i];		
			firV1.output4 += samples[i]*coeff4[i];		
			firV1.output5 += samples[i]*coeff5[i];		
			firV1.output6 += samples[i]*coeff6[i];		
			firV1.output7 += samples[i]*coeff7[i];		
			firV1.outputH += samples[i]*coeffH[i];		
		}
        //if( clock_gettime( CLOCK_REALTIME, &currentTime) == -1 ) { perror( "clock gettime" );}    //COMPUTATION TIME MEASUREMENT ONLY
		wFIRV1->push(firV1);						                                                //send results to the next processor
		rSampleV->pop();                                                                            //removes the previous read value (sample) from the buffer
		firV1.outputL = 0;                                                                          //reset output
        firV1.output2 = 0;
        firV1.output3 = 0;
        firV1.output4 = 0;
        firV1.output5 = 0;
        firV1.output6 = 0;
        firV1.output7 = 0;
        firV1.outputH = 0;
		//diff = (long)( currentTime.tv_nsec - prevTime.tv_nsec );                                  //COMPUTATION TIME MEASUREMENT ONLY
    }
	return NULL;
}

void *procFirV2(void *arg) {                                                                        //the second FIR filter, filtering the signal of voice
	wFIRV2->validate();                                                                             //validate the buffers
    rFIRV1->validate();
                                                                                                    //second 16 FIR coefficients
	double coeffL[NUMBER_OF_COEFFICIENTS_VOICE] = {-0.027022507667696, -0.026629611173050, -0.024932519751067, -0.021841119566456, -0.017339617334486, -0.011493700736024, -0.004452430590061, 0.003555512963873, 0.012231196686886, 0.021219960056116, 0.030130253394294, 0.038555602481433, 0.046098258350861, 0.052392939538794, 0.057129039644912, 0.060069759596744};
	double coeff2[NUMBER_OF_COEFFICIENTS_VOICE] = {-0.016482476737109, -0.024623108387785, -0.032090370494862, -0.038053341835886, -0.041733035635186, -0.042495328209234, -0.039934847799431, -0.033938987160995, -0.024723098802707, -0.012831047823733, 0.000900728490742, 0.015413157473536, 0.029521791405358, 0.042031248800494, 0.051854384476944, 0.058122898881404};
	double coeff3[NUMBER_OF_COEFFICIENTS_VOICE] = {0.026624940033079, 0.022239208172513, 0.013369270025653, 0.000732157242514, -0.014153916507945, -0.029147034768953, -0.041803649079250, -0.049794644352933, -0.051340763563091, -0.045582807993428, -0.032809203092594, -0.014487249855673, 0.006919528810690, 0.028324132615340, 0.046516833855282, 0.058704763992838};
	double coeff4[NUMBER_OF_COEFFICIENTS_VOICE] = {-0.004015900041785, 0.011217003706662, 0.026627017567914, 0.037587208757029, 0.039998680470660, 0.031700684658135, 0.013453310140714, -0.010896071399416, -0.035217317438108, -0.052715175469964, -0.057914614332365, -0.048408173926062, -0.025791862929218, 0.004557836959637, 0.034859323481132, 0.057078400453065};
	double coeff5[NUMBER_OF_COEFFICIENTS_VOICE] = {-0.011651762869775, -0.027637221911951, -0.034042074646718, -0.025386320172461, -0.002773816647987, 0.025383005032015, 0.046403772174674, 0.049122104899377, 0.029734263935740, -0.005354958846325, -0.041526714122110, -0.062264741181699, -0.056972193155566, -0.026462590312242, 0.016773221078528, 0.053933919032289};
	double coeff6[NUMBER_OF_COEFFICIENTS_VOICE] = {-0.004098857968139, 0.001806130991557, 0.015127068099604, 0.021508862937617, 0.005746650639745, -0.027467488868565, -0.049041850612001, -0.029608922036120, 0.026647997408873, 0.075029358607835, 0.065948393672587, -0.005166759299702, -0.083865971268769, -0.099783818465137, -0.031746352370223, 0.067295398841963};
	double coeff7[NUMBER_OF_COEFFICIENTS_VOICE] = {-0.002328044407612, 0.008570486744891, 0.013764004884018, -0.008416894263038, -0.031678468298399, -0.007216597147966, 0.044465077150508, 0.038630226857527, -0.037480564910818, -0.073442918951193, 0.003833162645278, 0.091382995269710, 0.047674233327393, -0.076723715280945, -0.094683156680924, 0.030012437633434};
	double coeffH[NUMBER_OF_COEFFICIENTS_VOICE] = {0.001312494665377, 0.016659798183393, -0.017351963141730, -0.017994960653474, 0.034354276498494, 0.000118691159879, -0.023290979658111, 0.004238585658406, -0.010341448311411, 0.041537377425001, 0.011580124240907, -0.115854486322918, 0.063373009263578, 0.140010694923593, -0.176989149991739, -0.063948285346085};
	double samples[NUMBER_OF_COEFFICIENTS_VOICE]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};     
    double sampleTemp = 0;                                                                          //remember last sample
    /*static struct timespec prevTime;                                                              //COMPUTATION TIME MEASUREMENT ONLY
    struct timespec currentTime;
    double diff = 0;*/
    while(1) {
        sampleTemp = samples[NUMBER_OF_COEFFICIENTS_VOICE-1];                                       //remember last sample before removing it
		firV2=rFIRV1->front();                                                                      //read the sample and output of previous processor
		//if( clock_gettime( CLOCK_REALTIME, &prevTime) == -1 ) { perror( "clock gettime" ); }      //COMPUTATION TIME MEASUREMENT ONLY
		memmove(samples + 1, samples, (NUMBER_OF_COEFFICIENTS_VOICE - 1)*sizeof(double));           //shift samples to the right
		samples[0] = firV2.sample;                                                                  //place the current sample in first element.
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_VOICE;i++)                                           //FIR calculations
		{
			firV2.outputL += samples[i]*coeffL[i];		
            firV2.output2 += samples[i]*coeff2[i];		
			firV2.output3 += samples[i]*coeff3[i];		
			firV2.output4 += samples[i]*coeff4[i];		
			firV2.output5 += samples[i]*coeff5[i];		
			firV2.output6 += samples[i]*coeff6[i];		
			firV2.output7 += samples[i]*coeff7[i];		
			firV2.outputH += samples[i]*coeffH[i];		

		}
        firV2.sample = sampleTemp;                                                                  
        
        /*if( clock_gettime( CLOCK_REALTIME, &currentTime) == -1 ) { perror( "clock gettime" );}    //COMPUTATION TIME MEASUREMENT ONLY    
        diff = (long)( currentTime.tv_nsec - prevTime.tv_nsec );*/
		wFIRV2->push(firV2);                                                                        //push the output and last sample before it was shifted
		rFIRV1->pop();                                                                              //remove previous output from buffer
		firV2.outputL = 0;                                                                          //reset output
        firV2.output2 = 0;
        firV2.output3 = 0;
        firV2.output4 = 0;
        firV2.output5 = 0;
        firV2.output6 = 0;
        firV2.output7 = 0;
        firV2.outputH = 0;
	}
	return NULL;
}

void *procFirV3(void *arg) {                                                                        //the third FIR filter, filtering the signal of voice
	wFIRV3->validate();
    rFIRV2->validate();
                                                                                                    //third 16 coeffiecients of the FIR filter
    double coeffL[NUMBER_OF_COEFFICIENTS_VOICE] = {0.061066825977263, 0.060069759596744, 0.057129039644912, 0.052392939538794, 0.046098258350861, 0.038555602481433, 0.030130253394294, 0.021219960056116, 0.012231196686886, 0.003555512963873, -0.004452430590061, -0.011493700736024, -0.017339617334486, -0.021841119566456, -0.024932519751067, -0.026629611173050};
    double coeff2[NUMBER_OF_COEFFICIENTS_VOICE] = {0.060276859955755, 0.058122898881404, 0.051854384476944, 0.042031248800494, 0.029521791405358, 0.015413157473536, 0.000900728490742, -0.012831047823733, -0.024723098802707, -0.033938987160995, -0.039934847799431, -0.042495328209234, -0.041733035635186, -0.038053341835886, -0.032090370494862, -0.024623108387785};
    double coeff3[NUMBER_OF_COEFFICIENTS_VOICE] = {0.062991176958543, 0.058704763992838, 0.046516833855282, 0.028324132615340, 0.006919528810690, -0.014487249855673, -0.032809203092594, -0.045582807993428, -0.051340763563091, -0.049794644352933, -0.041803649079250, -0.029147034768953, -0.014153916507945, 0.000732157242514, 0.013369270025653, 0.022239208172513};
    double coeff4[NUMBER_OF_COEFFICIENTS_VOICE] = {0.065222410998837, 0.057078400453065, 0.034859323481132, 0.004557836959637, -0.025791862929218, -0.048408173926062, -0.057914614332365, -0.052715175469964, -0.035217317438108, -0.010896071399416, 0.013453310140714, 0.031700684658135, 0.039998680470660, 0.037587208757029, 0.026627017567914, 0.011217003706662};
    double coeff5[NUMBER_OF_COEFFICIENTS_VOICE] = {0.068497985286153, 0.053933919032289, 0.016773221078528, -0.026462590312242, -0.056972193155566, -0.062264741181699, -0.041526714122110, -0.005354958846325, 0.029734263935740, 0.049122104899377, 0.046403772174674, 0.025383005032015, -0.002773816647987, -0.025386320172461, -0.034042074646718, -0.027637221911951};
    double coeff6[NUMBER_OF_COEFFICIENTS_VOICE] = {0.113639330430100, 0.067295398841963, -0.031746352370223, -0.099783818465137, -0.083865971268769, -0.005166759299702, 0.065948393672587, 0.075029358607835, 0.026647997408873, -0.029608922036120, -0.049041850612001, -0.027467488868565, 0.005746650639745, 0.021508862937617, 0.015127068099604, 0.001806130991557};
    double coeff7[NUMBER_OF_COEFFICIENTS_VOICE] = {0.113684089382586, 0.030012437633434, -0.094683156680924, -0.076723715280945, 0.047674233327393, 0.091382995269710, 0.003833162645278, -0.073442918951193, -0.037480564910818, 0.038630226857527, 0.044465077150508, -0.007216597147966, -0.031678468298399, -0.008416894263038, 0.013764004884018, 0.008570486744891};
    double coeffH[NUMBER_OF_COEFFICIENTS_VOICE] = {0.232330624547268, -0.063948285346085, -0.176989149991739, 0.140010694923593, 0.063373009263578, -0.115854486322918, 0.011580124240907, 0.041537377425001, -0.010341448311411, 0.004238585658406, -0.023290979658111, 0.000118691159879, 0.034354276498494, -0.017994960653474, -0.017351963141730, 0.016659798183393};
	double samples[NUMBER_OF_COEFFICIENTS_VOICE]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    double sampleTemp = 0;
    /*static struct timespec prevTime;                                                              //COMPUTATION TIME MEASUREMENT ONLY
    struct timespec currentTime;
    double diff = 0;*/
	while(1) {
        sampleTemp = samples[NUMBER_OF_COEFFICIENTS_VOICE-1];                                       //remember last sample before removing it
		firV3=rFIRV2->front();
		//if( clock_gettime( CLOCK_REALTIME, &prevTime) == -1 ) { perror( "clock gettime" ); }      //COMPUTATION TIME MEASUREMENT ONLY    
		memmove(samples + 1, samples, (NUMBER_OF_COEFFICIENTS_VOICE - 1)*sizeof(double));           //shift (old sample) array elements to the right
		samples[0] = firV3.sample;                                                                  //place the current sample in first element.
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_VOICE;i++)                                           //FIR calculations
		{
			firV3.outputL += samples[i]*coeffL[i];		
			firV3.output2 += samples[i]*coeff2[i];		
			firV3.output3 += samples[i]*coeff3[i];		
			firV3.output4 += samples[i]*coeff4[i];		
			firV3.output5 += samples[i]*coeff5[i];		
			firV3.output6 += samples[i]*coeff6[i];		
			firV3.output7 += samples[i]*coeff7[i];		
			firV3.outputH += samples[i]*coeffH[i];		
		}
        firV3.sample = sampleTemp;
        //if( clock_gettime( CLOCK_REALTIME, &currentTime) == -1 ) { perror( "clock gettime" ); }   //COMPUTATION TIME MEASUREMENT ONLY
		wFIRV3->push(firV3);
		rFIRV2->pop();
		firV3.outputL = 0;
		firV3.output2 = 0;
		firV3.output3 = 0;
		firV3.output4 = 0;
		firV3.output5 = 0;
		firV3.output6 = 0;
		firV3.output7 = 0;
		firV3.outputH = 0;
		//diff = (long)( currentTime.tv_nsec - prevTime.tv_nsec );                                  //COMPUTATION TIME MEASUREMENT ONLY
	}
	return NULL;
}

void *procFirV4(void *arg) {                                                                        //the fourth and last FIR filter, filtering the signal of voice
    rFIRV3->validate();
    wEnv->validate();
                                                                                                    //the last 17 of 65 coefficients:
	double coeffL[NUMBER_OF_COEFFICIENTS_VOICE+1] = {-0.027022507667696, -0.026263964657108, -0.024554236028881, -0.022123736645131, -0.019214891797955, -0.016064556546795, -0.012888282408866, -0.009867507627148, -0.007140468915944, -0.004797301628876, -0.002879439797339, -0.001383076815682, -0.000266129923033, 0.000542108161679, 0.001129629658942, 0.001589776213765, 0.002009797714883};
	double coeff2[NUMBER_OF_COEFFICIENTS_VOICE+1] = {-0.016482476737109, -0.008460677878210, -0.001233186790668, 0.004698435913559, 0.009037840152308, 0.011697802698854, 0.012778841064371, 0.012527687586138, 0.011283005449624, 0.009416827453657, 0.007280084861265, 0.005159343632905, 0.003249707196761, 0.001646123177970, 0.000352455259332, -0.000694934711938, -0.001594342843359};
	double coeff3[NUMBER_OF_COEFFICIENTS_VOICE+1] = {0.026624940033079, 0.026625216891559, 0.023021373030660, 0.017038832776553, 0.010062071439526, 0.003364339448142, -0.002096999075264, -0.005788695810532, -0.007606934274455, -0.007796102179399, -0.006813806540355, -0.005181857429666, -0.003359831600897, -0.001666490516444, -0.000258387944813, 0.000841140019805, 0.001681702677228};
	double coeff4[NUMBER_OF_COEFFICIENTS_VOICE+1] = {-0.004015900041785, -0.015278641133925, -0.020524352683802, -0.019707329678306, -0.014408586506634, -0.007054094417358, -0.000040915964557, 0.004939048090860, 0.007206128780592, 0.007014909392857, 0.005207876669386, 0.002783340573114, 0.000547301109234, -0.001053741878834, -0.001915461910994, -0.002143695820440, -0.001904979513933};
	double coeff5[NUMBER_OF_COEFFICIENTS_VOICE+1] = {-0.011651762869775, 0.005427433474339, 0.016401804083837, 0.018247379073870, 0.012514992391961, 0.003565916374712, -0.004055096680685, -0.007668951547568, -0.007106170011957, -0.004012538631502, -0.000530418575543, 0.001816248082992, 0.002556138503085, 0.002051302525254, 0.000985790736354, -0.000056022594752, -0.000749265947793};
	double coeff6[NUMBER_OF_COEFFICIENTS_VOICE+1] = {-0.004098857968139, -0.001841050120118, 0.000610299053879, -0.001405715398890, -0.004765271639935, -0.004274608590728, 0.000417907441575, 0.004775799239451, 0.004852564849775, 0.001239654476244, -0.002341935061096, -0.003137985152701, -0.001456357651484, 0.000589942226046, 0.001411739621265, 0.000934960898828, 0.000032766768318};
	double coeff7[NUMBER_OF_COEFFICIENTS_VOICE+1] = {-0.002328044407612, -0.001839669502584, 0.000166228510538, -0.003181632702084, -0.003043302441441, 0.003320178791202, 0.005483895551463, -0.000512840852606, -0.005111219515949, -0.001971789323416, 0.002896541982954, 0.002618520697641, -0.000773965280287, -0.001942678450245, -0.000330683686573, 0.000998863216021, 0.000581384922188};
	double coeffH[NUMBER_OF_COEFFICIENTS_VOICE+1] = {0.001312494665377, -0.001765070724298, -0.001777441649843, -0.006062256870050, 0.008234658656892, 0.002643416033895, -0.008157230243151, 0.001730068159807, 0.003043796655958, -0.001148499469490, 0.000059447854077, -0.001230351661134, 0.000274891598705, 0.001799246285214, -0.001248028815229, -0.000864228597482, 0.001222154680657};
	double samples[NUMBER_OF_COEFFICIENTS_VOICE+1]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    double sampleTemp = 0;
    /*static struct timespec prevTime;                                                              //COMPUTATION TIME MEASUREMENT ONLY
     struct timespec currentTime;
     double diff = 0;*/
	while(1) {
        sampleTemp = samples[NUMBER_OF_COEFFICIENTS_VOICE];                                         //remember last sample before removing it
		firV4=rFIRV3->front();
		//if( clock_gettime( CLOCK_REALTIME, &prevTime) == -1 ) { perror( "clock gettime" ); }      //COMPUTATION TIME MEASUREMENT ONLY
        memmove(samples + 1, samples, (NUMBER_OF_COEFFICIENTS_VOICE )*sizeof(double));              //shift (old sample) array elements to the right
		samples[0] = firV4.sample;                                                                  //place the current sample in first element.
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_VOICE+1;i++)                                         //FIR calculations
		{
			firV4.outputL += samples[i]*coeffL[i];		
			firV4.output2 += samples[i]*coeff2[i];		
			firV4.output3 += samples[i]*coeff3[i];		
			firV4.output4 += samples[i]*coeff4[i];		
			firV4.output5 += samples[i]*coeff5[i];		
			firV4.output6 += samples[i]*coeff6[i];		
			firV4.output7 += samples[i]*coeff7[i];		
			firV4.outputH += samples[i]*coeffH[i];		

		}
        firV4.sample = sampleTemp;                                                                  //only needed when there is a next processor to send the sample to (incase you want to expand the number of processors).
        firVenv.outputL = firV4.outputL;                                                            //placing result of 8 fir filters into struct for envelope buffer.
        firVenv.output2 = firV4.output2;    
        firVenv.output3 = firV4.output3;
        firVenv.output4 = firV4.output4;
        firVenv.output5 = firV4.output5;
        firVenv.output6 = firV4.output6;
        firVenv.output7 = firV4.output7;
        firVenv.outputH = firV4.outputH;
        //if( clock_gettime( CLOCK_REALTIME, &currentTime) == -1 ) { perror( "clock gettime" ); }   //COMPUTATION TIME MEASUREMENT ONLY
        wEnv->push(firVenv);
		rFIRV3->pop();
		firV4.outputL = 0;
		firV4.output2 = 0;
		firV4.output3 = 0;
		firV4.output4 = 0;
		firV4.output5 = 0;
		firV4.output6 = 0;
		firV4.output7 = 0;
		firV4.outputH = 0;
		//diff = (long)( currentTime.tv_nsec - prevTime.tv_nsec );                                  //COMPUTATION TIME MEASUREMENT ONLY
	}
	return NULL;
}
//Processor functions for overlapping sound
void *procSamplerO(void *arg) {                                                                     //creates a simulation of the ADC that should process an overlapping sound
    wSampleO->validate();                                                                           //validate the buffer
    int tick = 0;                                                                                   //remember the elapsed time for creating a sqaure wave
    int timeElapsed = 0;                                                                            //the elapsed time indicates when to switch to another frequency; demonstration purpose only
    double sample = 25;                                                                             //sample that is pushed to the first FIR filter
    int j=1;                                                                                        //possitve or negative in order to create a sqaure wave 
    int freq_tot = FREQ_TOT1;                                                                       //frequency starts at 800Hz
    int freq_high = FREQ_HIGH1;
    /*static struct timespec prevTime;                                                              //COMPUTATION TIME MEASUREMENT ONLY
    struct timespec currentTime;
    double diff = 0;*/
	while(true){
		if( clock_gettime( CLOCK_REALTIME, &prevTime) == -1 ) { perror( "clock gettime" ); }        //COMPUTATION TIME MEASUREMENT ONLY
		sample = 10 * j;
		wSampleO->push(sample);                                                                     //push sample to first FIR (out of 4) filter
		if(tick >= freq_high)                                                                       //creating a sqaure wave by switching from negative to possitive and visa versa
		{
			j = -1;
			if(tick >= freq_tot)
			{
				j = 1;
				tick = 0;
			}
		}
		if(timeElapsed >= 500)                                                                      //switching to 100Hz frequency after 500 iterations
		{
			freq_tot = FREQ_TOT2;
			freq_high = FREQ_HIGH2;
			timeElapsed = 0;
		}
		tick++;
		timeElapsed++;
		usleep(SAMPLE_FREQUENCY);                                                                   //sleep for certain amount of time

		/*if( clock_gettime( CLOCK_REALTIME, &currentTime) == -1 ) { perror( "clock gettime" ); }   //COMPUTATION TIME MEASUREMENT ONLY
		diff = (double)( currentTime.tv_nsec - prevTime.tv_nsec );*/
	}
	return NULL;
}


void *procFirO1(void *arg) {                                                                        //the first FIR filter, filtering the signal of overlapping sound
	rSampleO->validate();                                                                           //validate the buffers
	wFIRO1->validate();
                                                                                                    //the first 16 (out of 65) FIR coefficients
	double coeffL[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.002009797714883, 0.001589776213765, 0.001129629658942, 0.000542108161679, -0.000266129923033, -0.001383076815682, -0.002879439797339, -0.004797301628876, -0.007140468915944, -0.009867507627148, -0.012888282408866, -0.016064556546795, -0.019214891797955, -0.022123736645131, -0.024554236028881, -0.026263964657108};
	double coeff2[NUMBER_OF_COEFFICIENTS_OVERLAP] = {-0.001594342843359, -0.000694934711938, 0.000352455259332, 0.001646123177970, 0.003249707196761, 0.005159343632905, 0.007280084861265, 0.009416827453657, 0.011283005449624, 0.012527687586138, 0.012778841064371, 0.011697802698854, 0.009037840152308, 0.004698435913559, -0.001233186790668, -0.008460677878210};
	double coeff3[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.001681702677228, 0.000841140019805, -0.000258387944813, -0.001666490516444, -0.003359831600897, -0.005181857429666, -0.006813806540355, -0.007796102179399, -0.007606934274455, -0.005788695810532, -0.002096999075264, 0.003364339448142, 0.010062071439526, 0.017038832776553, 0.023021373030660, 0.026625216891559};
	double coeff4[NUMBER_OF_COEFFICIENTS_OVERLAP] = {-0.001904979513933, -0.002143695820440, -0.001915461910994, -0.001053741878834, 0.000547301109234, 0.002783340573114, 0.005207876669386, 0.007014909392857, 0.007206128780592, 0.004939048090860, -0.000040915964557, -0.007054094417358, -0.014408586506634, -0.019707329678306, -0.020524352683802, -0.015278641133925};
	double coeff5[NUMBER_OF_COEFFICIENTS_OVERLAP] = {-0.000749265947793, -0.000056022594752, 0.000985790736354, 0.002051302525254, 0.002556138503085, 0.001816248082992, -0.000530418575543, -0.004012538631502, -0.007106170011957, -0.007668951547568, -0.004055096680685, 0.003565916374712, 0.012514992391961, 0.018247379073870, 0.016401804083837, 0.005427433474339};
	double coeff6[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.000032766768318, 0.000934960898828, 0.001411739621265, 0.000589942226046, -0.001456357651484, -0.003137985152701, -0.002341935061096, 0.001239654476244, 0.004852564849775, 0.004775799239451, 0.000417907441575, -0.004274608590728, -0.004765271639935, -0.001405715398890, 0.000610299053879, -0.001841050120118};
	double coeff7[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.000581384922188, 0.000998863216021, -0.000330683686573, -0.001942678450245, -0.000773965280287, 0.002618520697641, 0.002896541982954, -0.001971789323416, -0.005111219515949, -0.000512840852606, 0.005483895551463, 0.003320178791202, -0.003043302441441, -0.003181632702084, 0.000166228510538, -0.001839669502584};
	double coeffH[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.001222154680657, -0.000864228597482, -0.001248028815229, 0.001799246285214, 0.000274891598705, -0.001230351661134, 0.000059447854077, -0.001148499469490, 0.003043796655958, 0.001730068159807, -0.008157230243151, 0.002643416033895, 0.008234658656892, -0.006062256870050, -0.001777441649843, -0.001765070724298};
	double samples[NUMBER_OF_COEFFICIENTS_OVERLAP]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	firO1.outputL = 0;
	firO1.output2 = 0;
	firO1.output3 = 0;
	firO1.output4 = 0;
	firO1.output5 = 0;
	firO1.output6 = 0;
	firO1.output7 = 0;
	firO1.outputH = 0;
	while(1) {
		firO1.sample = samples[NUMBER_OF_COEFFICIENTS_OVERLAP-1];		                            //remember last sample before removing it
		memmove(samples + 1, samples, (NUMBER_OF_COEFFICIENTS_OVERLAP - 1)*sizeof(double));         //shift (old sample) array elements to the right
		samples[0] = rSampleO->front();                                                             //place the current sample in first element.
        for(int i = 0;i<NUMBER_OF_COEFFICIENTS_OVERLAP;i++)                                         //FIR calculations
		{
            firO1.outputL += samples[i]*coeffL[i];		
			firO1.output2 += samples[i]*coeff2[i];		
			firO1.output3 += samples[i]*coeff3[i];		
			firO1.output4 += samples[i]*coeff4[i];		
			firO1.output5 += samples[i]*coeff5[i];		
			firO1.output6 += samples[i]*coeff6[i];		
			firO1.output7 += samples[i]*coeff7[i];		
			firO1.outputH += samples[i]*coeffH[i];		

		}
		wFIRO1->push(firO1);						                                                //send results to the second FIR filter
		rSampleO->pop();
		firO1.outputL = 0;                                                                          //reset outputs, otherwise you will get wrong results
		firO1.output2 = 0;
		firO1.output3 = 0;
		firO1.output4 = 0;
		firO1.output5 = 0;
		firO1.output6 = 0;
		firO1.output7 = 0;
		firO1.outputH = 0;
	}
	return NULL;
}

void *procFirO2(void *arg) {                                                                        //second FIR filter, filtering the signal of overlapping sound
	wFIRO2->validate();
    rFIRO1->validate();
                                                                                                    //the second 16 FIR coefficients
	double coeffL[NUMBER_OF_COEFFICIENTS_OVERLAP] = {-0.027022507667696, -0.026629611173050, -0.024932519751067, -0.021841119566456, -0.017339617334486, -0.011493700736024, -0.004452430590061, 0.003555512963873, 0.012231196686886, 0.021219960056116, 0.030130253394294, 0.038555602481433, 0.046098258350861, 0.052392939538794, 0.057129039644912, 0.060069759596744};
	double coeff2[NUMBER_OF_COEFFICIENTS_OVERLAP] = {-0.016482476737109, -0.024623108387785, -0.032090370494862, -0.038053341835886, -0.041733035635186, -0.042495328209234, -0.039934847799431, -0.033938987160995, -0.024723098802707, -0.012831047823733, 0.000900728490742, 0.015413157473536, 0.029521791405358, 0.042031248800494, 0.051854384476944, 0.058122898881404};
	double coeff3[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.026624940033079, 0.022239208172513, 0.013369270025653, 0.000732157242514, -0.014153916507945, -0.029147034768953, -0.041803649079250, -0.049794644352933, -0.051340763563091, -0.045582807993428, -0.032809203092594, -0.014487249855673, 0.006919528810690, 0.028324132615340, 0.046516833855282, 0.058704763992838};
	double coeff4[NUMBER_OF_COEFFICIENTS_OVERLAP] = {-0.004015900041785, 0.011217003706662, 0.026627017567914, 0.037587208757029, 0.039998680470660, 0.031700684658135, 0.013453310140714, -0.010896071399416, -0.035217317438108, -0.052715175469964, -0.057914614332365, -0.048408173926062, -0.025791862929218, 0.004557836959637, 0.034859323481132, 0.057078400453065};
	double coeff5[NUMBER_OF_COEFFICIENTS_OVERLAP] = {-0.011651762869775, -0.027637221911951, -0.034042074646718, -0.025386320172461, -0.002773816647987, 0.025383005032015, 0.046403772174674, 0.049122104899377, 0.029734263935740, -0.005354958846325, -0.041526714122110, -0.062264741181699, -0.056972193155566, -0.026462590312242, 0.016773221078528, 0.053933919032289};
	double coeff6[NUMBER_OF_COEFFICIENTS_OVERLAP] = {-0.004098857968139, 0.001806130991557, 0.015127068099604, 0.021508862937617, 0.005746650639745, -0.027467488868565, -0.049041850612001, -0.029608922036120, 0.026647997408873, 0.075029358607835, 0.065948393672587, -0.005166759299702, -0.083865971268769, -0.099783818465137, -0.031746352370223, 0.067295398841963};
	double coeff7[NUMBER_OF_COEFFICIENTS_OVERLAP] = {-0.002328044407612, 0.008570486744891, 0.013764004884018, -0.008416894263038, -0.031678468298399, -0.007216597147966, 0.044465077150508, 0.038630226857527, -0.037480564910818, -0.073442918951193, 0.003833162645278, 0.091382995269710, 0.047674233327393, -0.076723715280945, -0.094683156680924, 0.030012437633434};
	double coeffH[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.001312494665377, 0.016659798183393, -0.017351963141730, -0.017994960653474, 0.034354276498494, 0.000118691159879, -0.023290979658111, 0.004238585658406, -0.010341448311411, 0.041537377425001, 0.011580124240907, -0.115854486322918, 0.063373009263578, 0.140010694923593, -0.176989149991739, -0.063948285346085};
	double samples[NUMBER_OF_COEFFICIENTS_OVERLAP]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    double sampleTemp = 0;
	while(1) {
        sampleTemp = samples[NUMBER_OF_COEFFICIENTS_OVERLAP-1];
		firO2=rFIRO1->front();
        memmove(samples + 1, samples, (NUMBER_OF_COEFFICIENTS_OVERLAP - 1)*sizeof(double));         //shift (old sample) array elements to the right
		samples[0] = firO2.sample;                                                                  //place the current sample in first element.
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_OVERLAP;i++)                                         //FIR calculations
		{
			firO2.outputL += samples[i]*coeffL[i];		
			firO2.output2 += samples[i]*coeff2[i];		
			firO2.output3 += samples[i]*coeff3[i];		
			firO2.output4 += samples[i]*coeff4[i];		
			firO2.output5 += samples[i]*coeff5[i];		
			firO2.output6 += samples[i]*coeff6[i];		
			firO2.output7 += samples[i]*coeff7[i];		
			firO2.outputH += samples[i]*coeffH[i];		

		}
        firO2.sample = sampleTemp;
		wFIRO2->push(firO2);
		rFIRO1->pop();
		firO2.outputL = 0;
		firO2.output2 = 0;
		firO2.output3 = 0;
		firO2.output4 = 0;
		firO2.output5 = 0;
		firO2.output6 = 0;
		firO2.output7 = 0;
		firO2.outputH = 0;
	}
	return NULL;
}

void *procFirO3(void *arg) {                                                                        //the third FIR filter, filtering the signal of overlapping sound
	wFIRO3->validate();
    rFIRO2->validate();
                                                                                                    //the third 16 coefficients
	double coeffL[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.061066825977263, 0.060069759596744, 0.057129039644912, 0.052392939538794, 0.046098258350861, 0.038555602481433, 0.030130253394294, 0.021219960056116, 0.012231196686886, 0.003555512963873, -0.004452430590061, -0.011493700736024, -0.017339617334486, -0.021841119566456, -0.024932519751067, -0.026629611173050};
	double coeff2[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.060276859955755, 0.058122898881404, 0.051854384476944, 0.042031248800494, 0.029521791405358, 0.015413157473536, 0.000900728490742, -0.012831047823733, -0.024723098802707, -0.033938987160995, -0.039934847799431, -0.042495328209234, -0.041733035635186, -0.038053341835886, -0.032090370494862, -0.024623108387785};
	double coeff3[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.062991176958543, 0.058704763992838, 0.046516833855282, 0.028324132615340, 0.006919528810690, -0.014487249855673, -0.032809203092594, -0.045582807993428, -0.051340763563091, -0.049794644352933, -0.041803649079250, -0.029147034768953, -0.014153916507945, 0.000732157242514, 0.013369270025653, 0.022239208172513};
	double coeff4[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.065222410998837, 0.057078400453065, 0.034859323481132, 0.004557836959637, -0.025791862929218, -0.048408173926062, -0.057914614332365, -0.052715175469964, -0.035217317438108, -0.010896071399416, 0.013453310140714, 0.031700684658135, 0.039998680470660, 0.037587208757029, 0.026627017567914, 0.011217003706662};
	double coeff5[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.068497985286153, 0.053933919032289, 0.016773221078528, -0.026462590312242, -0.056972193155566, -0.062264741181699, -0.041526714122110, -0.005354958846325, 0.029734263935740, 0.049122104899377, 0.046403772174674, 0.025383005032015, -0.002773816647987, -0.025386320172461, -0.034042074646718, -0.027637221911951};
	double coeff6[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.113639330430100, 0.067295398841963, -0.031746352370223, -0.099783818465137, -0.083865971268769, -0.005166759299702, 0.065948393672587, 0.075029358607835, 0.026647997408873, -0.029608922036120, -0.049041850612001, -0.027467488868565, 0.005746650639745, 0.021508862937617, 0.015127068099604, 0.001806130991557};
	double coeff7[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.113684089382586, 0.030012437633434, -0.094683156680924, -0.076723715280945, 0.047674233327393, 0.091382995269710, 0.003833162645278, -0.073442918951193, -0.037480564910818, 0.038630226857527, 0.044465077150508, -0.007216597147966, -0.031678468298399, -0.008416894263038, 0.013764004884018, 0.008570486744891};
	double coeffH[NUMBER_OF_COEFFICIENTS_OVERLAP] = {0.232330624547268, -0.063948285346085, -0.176989149991739, 0.140010694923593, 0.063373009263578, -0.115854486322918, 0.011580124240907, 0.041537377425001, -0.010341448311411, 0.004238585658406, -0.023290979658111, 0.000118691159879, 0.034354276498494, -0.017994960653474, -0.017351963141730, 0.016659798183393};
	double samples[NUMBER_OF_COEFFICIENTS_OVERLAP]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    double sampleTemp = 0;
    while(1) {
        sampleTemp = samples[NUMBER_OF_COEFFICIENTS_OVERLAP-1];
		firO3=rFIRO2->front();
        memmove(samples + 1, samples, (NUMBER_OF_COEFFICIENTS_OVERLAP - 1)*sizeof(double));         //shift (old sample) array elements to the right
		samples[0] = firO3.sample;                                                                  //place the current sample in first element.
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_OVERLAP;i++)                                         //FIR calculations
		{
			firO3.outputL += samples[i]*coeffL[i];		
			firO3.output2 += samples[i]*coeff2[i];		
			firO3.output3 += samples[i]*coeff3[i];		
			firO3.output4 += samples[i]*coeff4[i];		
			firO3.output5 += samples[i]*coeff5[i];		
			firO3.output6 += samples[i]*coeff6[i];		
			firO3.output7 += samples[i]*coeff7[i];		
			firO3.outputH += samples[i]*coeffH[i];		
		}
        firO3.sample=sampleTemp;
		wFIRO3->push(firO3);
		rFIRO2->pop();
		firO3.outputL = 0;
		firO3.output2 = 0;
		firO3.output3 = 0;
		firO3.output4 = 0;
		firO3.output5 = 0;
		firO3.output6 = 0;
		firO3.output7 = 0;
		firO3.outputH = 0;
	}
	return NULL;
}

void *procFirO4(void *arg) {                                                                        //the fourth and last FIR filter, filtering the signal of overlapping sound
    rFIRO3->validate();
    wVCAFIR->validate();
                                                                                                    //the fourth and last 17 (out of 65) coeffiecients
	double coeffL[NUMBER_OF_COEFFICIENTS_OVERLAP+1] = {-0.027022507667696, -0.026263964657108, -0.024554236028881, -0.022123736645131, -0.019214891797955, -0.016064556546795, -0.012888282408866, -0.009867507627148, -0.007140468915944, -0.004797301628876, -0.002879439797339, -0.001383076815682, -0.000266129923033, 0.000542108161679, 0.001129629658942, 0.001589776213765, 0.002009797714883};
	double coeff2[NUMBER_OF_COEFFICIENTS_OVERLAP+1] = {-0.016482476737109, -0.008460677878210, -0.001233186790668, 0.004698435913559, 0.009037840152308, 0.011697802698854, 0.012778841064371, 0.012527687586138, 0.011283005449624, 0.009416827453657, 0.007280084861265, 0.005159343632905, 0.003249707196761, 0.001646123177970, 0.000352455259332, -0.000694934711938, -0.001594342843359};
	double coeff3[NUMBER_OF_COEFFICIENTS_OVERLAP+1] = {0.026624940033079, 0.026625216891559, 0.023021373030660, 0.017038832776553, 0.010062071439526, 0.003364339448142, -0.002096999075264, -0.005788695810532, -0.007606934274455, -0.007796102179399, -0.006813806540355, -0.005181857429666, -0.003359831600897, -0.001666490516444, -0.000258387944813, 0.000841140019805, 0.001681702677228};
	double coeff4[NUMBER_OF_COEFFICIENTS_OVERLAP+1] = {-0.004015900041785, -0.015278641133925, -0.020524352683802, -0.019707329678306, -0.014408586506634, -0.007054094417358, -0.000040915964557, 0.004939048090860, 0.007206128780592, 0.007014909392857, 0.005207876669386, 0.002783340573114, 0.000547301109234, -0.001053741878834, -0.001915461910994, -0.002143695820440, -0.001904979513933};
	double coeff5[NUMBER_OF_COEFFICIENTS_OVERLAP+1] = {-0.011651762869775, 0.005427433474339, 0.016401804083837, 0.018247379073870, 0.012514992391961, 0.003565916374712, -0.004055096680685, -0.007668951547568, -0.007106170011957, -0.004012538631502, -0.000530418575543, 0.001816248082992, 0.002556138503085, 0.002051302525254, 0.000985790736354, -0.000056022594752, -0.000749265947793};
	double coeff6[NUMBER_OF_COEFFICIENTS_OVERLAP+1] = {-0.004098857968139, -0.001841050120118, 0.000610299053879, -0.001405715398890, -0.004765271639935, -0.004274608590728, 0.000417907441575, 0.004775799239451, 0.004852564849775, 0.001239654476244, -0.002341935061096, -0.003137985152701, -0.001456357651484, 0.000589942226046, 0.001411739621265, 0.000934960898828, 0.000032766768318};
	double coeff7[NUMBER_OF_COEFFICIENTS_OVERLAP+1] = {-0.002328044407612, -0.001839669502584, 0.000166228510538, -0.003181632702084, -0.003043302441441, 0.003320178791202, 0.005483895551463, -0.000512840852606, -0.005111219515949, -0.001971789323416, 0.002896541982954, 0.002618520697641, -0.000773965280287, -0.001942678450245, -0.000330683686573, 0.000998863216021, 0.000581384922188};
	double coeffH[NUMBER_OF_COEFFICIENTS_OVERLAP+1] = {0.001312494665377, -0.001765070724298, -0.001777441649843, -0.006062256870050, 0.008234658656892, 0.002643416033895, -0.008157230243151, 0.001730068159807, 0.003043796655958, -0.001148499469490, 0.000059447854077, -0.001230351661134, 0.000274891598705, 0.001799246285214, -0.001248028815229, -0.000864228597482, 0.001222154680657};
	double samples[NUMBER_OF_COEFFICIENTS_OVERLAP+1]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    double sampleTemp = 0;
    while(1) {
        sampleTemp = samples[NUMBER_OF_COEFFICIENTS_OVERLAP-1];
		firO4=rFIRO3->front();
        memmove(samples + 1, samples, (NUMBER_OF_COEFFICIENTS_OVERLAP)*sizeof(double));             //shift (old sample) array elements to the right
		samples[0] = firO4.sample;                                                                  //place the current sample in first element.
		for(int i = 0;i<NUMBER_OF_COEFFICIENTS_OVERLAP+1;i++)                                       //FIR calculations
		{
			firO4.outputL += samples[i]*coeffL[i];		
			firO4.output2 += samples[i]*coeff2[i];		
			firO4.output3 += samples[i]*coeff3[i];		
			firO4.output4 += samples[i]*coeff4[i];		
			firO4.output5 += samples[i]*coeff5[i];		
			firO4.output6 += samples[i]*coeff6[i];		
			firO4.output7 += samples[i]*coeff7[i];		
			firO4.outputH += samples[i]*coeffH[i];		
		}
        firO4.sample = sampleTemp;
        firOVCA.outputL = firO4.outputL;                                                            //placing result of 8 fir filters into struct for VCA's buffer
        firOVCA.output2 = firO4.output2;
        firOVCA.output3 = firO4.output3;
        firOVCA.output4 = firO4.output4;
        firOVCA.output5 = firO4.output5;
        firOVCA.output6 = firO4.output6;
        firOVCA.output7 = firO4.output7;
        firOVCA.outputH = firO4.outputH;
        wVCAFIR->push(firOVCA);                                                                     //push result to the VCA's processor via a CFIFO buffer
		rFIRO3->pop();
		firO4.outputL = 0;
		firO4.output2 = 0;
		firO4.output3 = 0;
		firO4.output4 = 0;
		firO4.output5 = 0;
		firO4.output6 = 0;
		firO4.output7 = 0;
		firO4.outputH = 0;
    }
	return NULL;
}
void *procEnv(void *arg) {                                                                          //envelope follower, taking the absolute of the FIR filters
    rEnv->validate();                                                                               //validate the buffer
    double outputLtmp=0;                                                                            //temporary store the absolute of the FIR filters.
    double output2tmp=0;
    double output3tmp=0;
    double output4tmp=0;
    double output5tmp=0;
    double output6tmp=0;
    double output7tmp=0;
    double outputHtmp=0;
    /*static struct timespec prevTime;                                                              //COMPUTATION TIME MEASUREMENT ONLY
	struct timespec currentTime;
	double diff = 0;*/
	while(1) {
		envInternal=rEnv->front();                                                                  //read from the buffer
        
		//if( clock_gettime( CLOCK_REALTIME, &prevTime) == -1 ) { perror( "clock gettime" ); }      //COMPUTATION TIME MEASUREMENT ONLY
		outputLtmp  = fabs(envInternal.outputL);                                                    //determine the absolute value
		output2tmp  = fabs(envInternal.output2);
		output3tmp  = fabs(envInternal.output3);
		output4tmp  = fabs(envInternal.output4);
		output5tmp  = fabs(envInternal.output5);
		output6tmp  = fabs(envInternal.output6);
		output7tmp  = fabs(envInternal.output7);
		outputHtmp  = fabs(envInternal.outputH);
		envInternal.outputL = outputLtmp;                                                           //store in struct which is sended to the VCA
		envInternal.output2 = output2tmp;
		envInternal.output3 = output3tmp;
		envInternal.output4 = output4tmp;
		envInternal.output5 = output5tmp;
		envInternal.output6 = output6tmp;
		envInternal.output7 = output7tmp;
		envInternal.outputH = outputHtmp;
		//if( clock_gettime( CLOCK_REALTIME, &currentTime) == -1 ) { perror( "clock gettime" );}    //COMPUTATION TIME MEASUREMENT ONLY
        wVCAEnv->push(envInternal);                                                                 //push results to the VCA
        rEnv->pop();          
        //diff = (double)( currentTime.tv_nsec - prevTime.tv_nsec );                                //COMPUTATION TIME MEASUREMENT ONLY
	}
	return NULL;
}

void *procVCA(void *arg) {                                                                                                              //the VCA scales the output accordingly, resulting in a different voice 
    rVCAEnv->validate();                                                                                                                //validate buffers
    rVCAFIR->validate();
    double multFirEnvL=0;                                                                                                               //store the multiplication of overlapping sound with envelope follower
    double multFirEnv2=0;
    double multFirEnv3=0;
    double multFirEnv4=0;
    double multFirEnv5=0;
    double multFirEnv6=0;
    double multFirEnv7=0;
    double multFirEnvH=0;
    double sumFir = 0;                                                                                                                  //store the summation of all 8 multiplications (comming from 8 different filters)
    double pixelsSum[PIXEL_SIZE];                                                                                                       //store a certain amount of points that should be drawn every TICKS_PER_RENDER iterations
    double pixelsFIRL[PIXEL_SIZE];
    double pixelsFIR2[PIXEL_SIZE];
    double pixelsFIR3[PIXEL_SIZE];
    double pixelsFIR4[PIXEL_SIZE];
    double pixelsFIR5[PIXEL_SIZE];
    double pixelsFIR6[PIXEL_SIZE];
    double pixelsFIR7[PIXEL_SIZE];
    double pixelsFIRH[PIXEL_SIZE];
    int tick = 0;                                                                                                                       //number of iterations
    /*static struct timespec prevTime;                                                                                                  //COMPUTATION TIME MEASUREMENT ONLY                                                
	struct timespec currentTime;
	long double diff = 0;*/
	while(true){
		VCAenvInternal=rVCAEnv->front();                                                                                                //read the structs from buffer
		VCAfirInternal=rVCAFIR->front();
		//if( clock_gettime( CLOCK_REALTIME, &prevTime) == -1 ) {  perror( "clock gettime" );	}                                       //COMPUTATION TIME MEASUREMENT ONLY
	    multFirEnvL = VCAenvInternal.outputL * VCAfirInternal.outputL;                                                                  //amplitude scaling: SUM(|output FIRV| * output FIRO); where |output FIRV| = output ENV
	    multFirEnv2 = VCAenvInternal.output2 * VCAfirInternal.output2;
	    multFirEnv3 = VCAenvInternal.output3 * VCAfirInternal.output3;
	    multFirEnv4 = VCAenvInternal.output4 * VCAfirInternal.output4;
	    multFirEnv5 = VCAenvInternal.output5 * VCAfirInternal.output5;
	    multFirEnv6 = VCAenvInternal.output6 * VCAfirInternal.output6;
	    multFirEnv7 = VCAenvInternal.output7 * VCAfirInternal.output7;
	    multFirEnvH = VCAenvInternal.outputH * VCAfirInternal.outputH;
	    sumFir = multFirEnvL + multFirEnv2 + multFirEnv3 + multFirEnv4 + multFirEnv5 + multFirEnv6 + multFirEnv7 + multFirEnvH;

	    //#####################draw purpose only begin#############################
		memmove(pixelsFIRL + 1, pixelsFIRL, (PIXEL_SIZE-1)*sizeof(double));                                                             //the output values of FIRO are stored in the array
		pixelsFIRL[0] = VCAfirInternal.outputL*SCALE_OUTPUT;                                        
		memmove(pixelsFIR2 + 1, pixelsFIR2, (PIXEL_SIZE-1)*sizeof(double)); 
		pixelsFIR2[0] = VCAfirInternal.output2*SCALE_OUTPUT; 
		memmove(pixelsFIR3 + 1, pixelsFIR3, (PIXEL_SIZE-1)*sizeof(double)); 
		pixelsFIR3[0] = VCAfirInternal.output3*SCALE_OUTPUT; 
		memmove(pixelsFIR4 + 1, pixelsFIR4, (PIXEL_SIZE-1)*sizeof(double)); 
		pixelsFIR4[0] = VCAfirInternal.output4*SCALE_OUTPUT; 
		memmove(pixelsFIR5 + 1, pixelsFIR5, (PIXEL_SIZE-1)*sizeof(double)); 
		pixelsFIR5[0] = VCAfirInternal.output5*SCALE_OUTPUT; 
		memmove(pixelsFIR6 + 1, pixelsFIR6, (PIXEL_SIZE-1)*sizeof(double)); 
		pixelsFIR6[0] = VCAfirInternal.output6*SCALE_OUTPUT; 
		memmove(pixelsFIR7 + 1, pixelsFIR7, (PIXEL_SIZE-1)*sizeof(double)); 
		pixelsFIR7[0] = VCAfirInternal.output7*SCALE_OUTPUT; 
		memmove(pixelsFIRH + 1, pixelsFIRH, (PIXEL_SIZE-1)*sizeof(double));
		pixelsFIRH[0] = VCAfirInternal.outputH*SCALE_OUTPUT; 
		memmove(pixelsSum + 1, pixelsSum, (PIXEL_SIZE-1)*sizeof(double));                                                               //the sum of the FIR filters, placed in an array
		pixelsSum[0] = sumFir*SCALE_OUTPUT; 
		if(tick >= TICKS_PER_RENDER)                                                                                                    //every TICKS_PER_RENDER iterations, the values currently in the array are drawn on screen                                           
		{
			fillrect(0,0,DVI_WIDTH, DVI_HEIGHT, white);                                                                                 //draw white screen to remove all the previous drawings
			for(int n = 0; n < PIXEL_SIZE-2; n++)                                                                                       //draw the array containing the values of FIRO and sumFir
			{
                drawline(X_AXIS_SCALING*n,pixelsFIRL[n]+40,(X_AXIS_SCALING*n)+1, pixelsFIRL[n+1]+40,black,-1,-1);                       //draw ouput of FIRO       
				drawline(X_AXIS_SCALING*n,pixelsFIR2[n]+80,(X_AXIS_SCALING*n)+1, pixelsFIR2[n+1]+80,black,-1,-1);
				drawline(X_AXIS_SCALING*n,pixelsFIR3[n]+120,(X_AXIS_SCALING*n)+1, pixelsFIR3[n+1]+120,black,-1,-1);
				drawline(X_AXIS_SCALING*n,pixelsFIR4[n]+160,(X_AXIS_SCALING*n)+1, pixelsFIR4[n+1]+160,black,-1,-1);
				drawline(X_AXIS_SCALING*n,pixelsFIR5[n]+200,(X_AXIS_SCALING*n)+1, pixelsFIR5[n+1]+200,black,-1,-1);
				drawline(X_AXIS_SCALING*n,pixelsFIR6[n]+240,(X_AXIS_SCALING*n)+1, pixelsFIR6[n+1]+240,black,-1,-1);
				drawline(X_AXIS_SCALING*n,pixelsFIR7[n]+280,(X_AXIS_SCALING*n)+1, pixelsFIR7[n+1]+280,black,-1,-1);
				drawline(X_AXIS_SCALING*n,pixelsFIRH[n]+320,(X_AXIS_SCALING*n)+1, pixelsFIRH[n+1]+320,black,-1,-1);
				drawline(X_AXIS_SCALING*n,pixelsSum[n]+400,X_AXIS_SCALING*n, pixelsSum[n+1]+400,red,-1,-1);                             //draw sumFir
			}
			render_flip_buffer();                                                                                                       //render buffer, aka draw pixels on screen
		}
		tick++;
	    //#####################draw purpose only end#############################
		if( clock_gettime( CLOCK_REALTIME, &currentTime) == -1 ) {  perror( "clock gettime" );}                                         //COMPUTATION TIME MEASUREMENT ONLY
        rVCAFIR->pop();                                                                                                                 //remove the struct from buffer
        rVCAEnv->pop();

		/*diff = (long double)( currentTime.tv_nsec - prevTime.tv_nsec );                                                               //COMPUTATION TIME MEASUREMENT ONLY
		printf( "VCA: %Lf\n", diff );
		printf("sumFir %lf \n",sumFir);*/                                                                                               //incase you want to print the result
	}
	return NULL;
}

int main(int argc, char **argv) {                                                                                                                   //creating, starting and validating buffers and processors
	pid_t pidV0, pidV1, pidV2, pidV3, pidV4, pidO0, pidO1, pidO2, pidO3, pidO4, pidEnv, pidVCA;
	CFifoPtr<double> cFifoSampleV = CFifo<double>::Create(1, wSampleV, 2, rSampleV, 10);                                                            //create cFIFO for FIR voice(V)
	CFifoPtr<firVstruct> cFifoFIRV1 = CFifo<firVstruct>::Create(2, wFIRV1, 3, rFIRV1, 10);
	CFifoPtr<firVstruct> cFifoFIRV2 = CFifo<firVstruct>::Create(3, wFIRV2, 4, rFIRV2, 10);
	CFifoPtr<firVstruct> cFifoFIRV3 = CFifo<firVstruct>::Create(4, wFIRV3, 5, rFIRV3, 10);
	CFifoPtr<double> cFifoSampleO = CFifo<double>::Create(6, wSampleO, 7, rSampleO, 10);                                                            //create cFIFO for FIR overlapping(O) sound
	CFifoPtr<firOstruct> cFifoFIRO1 = CFifo<firOstruct>::Create(7, wFIRO1, 8, rFIRO1, 10);
	CFifoPtr<firOstruct> cFifoFIRO2 = CFifo<firOstruct>::Create(8, wFIRO2, 9, rFIRO2, 10);
	CFifoPtr<firOstruct> cFifoFIRO3 = CFifo<firOstruct>::Create(9, wFIRO3, 10, rFIRO3, 10);
    CFifoPtr<outpStruct> cFifoEnv = CFifo<outpStruct>::Create(5, wEnv, 11, rEnv, 10);                                                               //create cFIFO for envelope follower
    CFifoPtr<outpStruct> cFifoVCAFIR = CFifo<outpStruct>::Create(10, wVCAFIR, 12, rVCAFIR, 10);                                                     //create cFIFO for VCA
    CFifoPtr<outpStruct> cFifoVCAEnv = CFifo<outpStruct>::Create(11, wVCAEnv, 12, rVCAEnv, 10);
    
    FlushDCache();                                                                                                                                  //flush the cash to prevent cache issues inside buffers
    
	if(!cFifoSampleV.valid()) ERREXIT("Error creating buffer");                                                                                     //validate cFIFO for FIR voice
	if(!cFifoFIRV1.valid()) ERREXIT("Error creating buffer");
	if(!cFifoFIRV2.valid()) ERREXIT("Error creating buffer");
	if(!cFifoFIRV3.valid()) ERREXIT("Error creating buffer");
	if(!cFifoSampleO.valid()) ERREXIT("Error creating buffer");                                                                                     //validate cFIFO for FIR overlapping sound
	if(!cFifoFIRO1.valid()) ERREXIT("Error creating buffer");
	if(!cFifoFIRO2.valid()) ERREXIT("Error creating buffer");
	if(!cFifoFIRO3.valid()) ERREXIT("Error creating buffer");
	if(!cFifoEnv.valid()) ERREXIT("Error creating buffer");                                                                                         //validate cFIFO for envelope follower        
	if(!cFifoVCAEnv.valid()) ERREXIT("Error creating buffer");                                                                                      //validate cFIFO for VCA  
	if(!cFifoVCAFIR.valid()) ERREXIT("Error creating buffer");

	printf("Starting drawing demo\n");                                                              
	if(render_init(1) != RENDER_OK) { printf("Error: init display!\n");	return 0; }                                                                 //initialize and validate the render
	fillrect(0,0,DVI_WIDTH,DVI_HEIGHT,white);                                                                                                       //draw the first white screen

	if(int e=CreateProcess(pidV0, procSamplerV, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 1)) ERREXIT2("Process creation failed: %i", e);   //create processes for voice filtering
	if(int e=CreateProcess(pidV1, procFirV1, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 2)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidV2, procFirV2, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 3)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidV3, procFirV3, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 4)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidV4, procFirV4, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 5)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidO0, procSamplerO, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 6)) ERREXIT2("Process creation failed: %i", e);   //create processes for overlapping sound filtering
	if(int e=CreateProcess(pidO1, procFirO1, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 7)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidO2, procFirO2, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 8)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidO3, procFirO3, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 9)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidO4, procFirO4, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 10)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pidEnv, procEnv, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 11)) ERREXIT2("Process creation failed: %i", e);      //create process for envelope follower
	if(int e=CreateProcess(pidVCA, procVCA, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 12)) ERREXIT2("Process creation failed: %i", e);      //create process for VCA
    
	if(int e=SetProcessFlags(pidV0, PROC_FLAG_JOINABLE, 1)) ERREXIT2("While setting process flags: %i", e);                                         //set process flaggs for voice
	if(int e=SetProcessFlags(pidV1, PROC_FLAG_JOINABLE, 2)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidV2, PROC_FLAG_JOINABLE, 3)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidV3, PROC_FLAG_JOINABLE, 4)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidV4, PROC_FLAG_JOINABLE, 5)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidO0, PROC_FLAG_JOINABLE, 6)) ERREXIT2("While setting process flags: %i", e);                                         //set process flaggs for overlapping sound
	if(int e=SetProcessFlags(pidO1, PROC_FLAG_JOINABLE, 7)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidO2, PROC_FLAG_JOINABLE, 8)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidO3, PROC_FLAG_JOINABLE, 9)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidO4, PROC_FLAG_JOINABLE, 10)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pidEnv, PROC_FLAG_JOINABLE, 11)) ERREXIT2("While setting process flags: %i", e);                                       //set process flaggs for envelope follower
	if(int e=SetProcessFlags(pidVCA, PROC_FLAG_JOINABLE, 12)) ERREXIT2("While setting process flags: %i", e);                                       //set process flaggs for VCA
	
	if(int e=StartProcess(pidV0, 1)) ERREXIT2("Could not start procSamplerV: %i", e);                                                               //start processes of voice
	if(int e=StartProcess(pidV1, 2)) ERREXIT2("Could not start procFirV1: %i", e);
	if(int e=StartProcess(pidV2, 3)) ERREXIT2("Could not start procFirV2: %i", e);
	if(int e=StartProcess(pidV3, 4)) ERREXIT2("Could not start procFirV3: %i", e);
	if(int e=StartProcess(pidV4, 5)) ERREXIT2("Could not start procFirV4: %i", e);
	if(int e=StartProcess(pidO0, 6)) ERREXIT2("Could not start procSamplerO: %i", e);                                                               //start processes of overlapping sound
	if(int e=StartProcess(pidO1, 7)) ERREXIT2("Could not start procFirO1: %i", e);
	if(int e=StartProcess(pidO2, 8)) ERREXIT2("Could not start procFirO2: %i", e);
	if(int e=StartProcess(pidO3, 9)) ERREXIT2("Could not start procFirO3: %i", e);
	if(int e=StartProcess(pidO4, 10)) ERREXIT2("Could not start procFirO4: %i", e);
	if(int e=StartProcess(pidEnv, 11)) ERREXIT2("Could not start procEnv: %i", e);                                                                  //start processes of envelope follower
	if(int e=StartProcess(pidVCA, 12)) ERREXIT2("Could not start procVCA: %i", e);                                                                  //start processes of VCA    

    
	if(int e=WaitProcess(pidV0, NULL, 1)) ERREXIT2("Waiting on pidV0 %i@%i: %i\n", pidV0, 1, e);                                                    //CFIFOs of voice are destroyed when the pointers goes out of scope
	if(int e=WaitProcess(pidV1, NULL, 2)) ERREXIT2("Waiting on pidV1 %i@%i: %i\n", pidV1, 2, e);
	if(int e=WaitProcess(pidV2, NULL, 3)) ERREXIT2("Waiting on pidV2 %i@%i: %i\n", pidV2, 3, e);
	if(int e=WaitProcess(pidV3, NULL, 4)) ERREXIT2("Waiting on pidV3 %i@%i: %i\n", pidV3, 4, e);
	if(int e=WaitProcess(pidV4, NULL, 5)) ERREXIT2("Waiting on pidV4 %i@%i: %i\n", pidV4, 5, e);
	if(int e=WaitProcess(pidO0, NULL, 6)) ERREXIT2("Waiting on pidO0 %i@%i: %i\n", pidO0, 6, e);                                                    //CFIFOs of overlapping sound are destroyed when the pointers goes out of scope
	if(int e=WaitProcess(pidO1, NULL, 7)) ERREXIT2("Waiting on pidO1 %i@%i: %i\n", pidO1, 7, e);
	if(int e=WaitProcess(pidO2, NULL, 8)) ERREXIT2("Waiting on pidO2 %i@%i: %i\n", pidO2, 8, e);
	if(int e=WaitProcess(pidO3, NULL, 9)) ERREXIT2("Waiting on pidO3 %i@%i: %i\n", pidO3, 9, e);
	if(int e=WaitProcess(pidO4, NULL, 10)) ERREXIT2("Waiting on pidO4 %i@%i: %i\n", pidO4, 10, e);                                                  //CFIFO of envelope follower is destroyed when the pointers goes out of scope    
	if(int e=WaitProcess(pidEnv, NULL, 11)) ERREXIT2("Waiting on pidEnv %i@%i: %i\n", pidEnv, 11, e);
	if(int e=WaitProcess(pidVCA, NULL, 12)) ERREXIT2("Waiting on pidVCA %i@%i: %i\n", pidVCA, 12, e);                                               //CFIFO of VCA is destroyed when the pointers goes out of scope
	return 0;
}

 
