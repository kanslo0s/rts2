clc;
clear all;
close all;

excelfile = 'Frequencies.xlsx';
sheet = 3;
xlRange = 'E4:F11';

P = xlsread(excelfile,sheet,xlRange);

Fs = 16000;

fig1 = true;
fig2 = true;
fig3 = true;
fig4 = true;
printtest = false;
exportfir = false;

%% Generate test data 1

A  = 1;
f  = 400;       % f in Hz
dc = 10;        % dutycycle
fp = f*2*pi;    % f in rad/s
Ts = 1/Fs;
t  = 0:Ts:1;
St = 100;       % start value print
Nn = 300;       % end value print

y = A*square(fp*t,dc);

%% digital filter design %%
Q = xlsread(excelfile,sheet,xlRange); % read file for digital filter specifications
n = 64; % number of taps

% calculate filter coefficients based on number of taps
if false
    for i = 1:8
        firr(:,i) = fir1(n, [Q(i,1) Q(i,2)]); 
    end
% or read from file    
else
    F = textread('firvalues_saved.txt','%f','delimiter',',');
    for i=0:7
        firr(:,i+1) = [F(1+i*16:16+i*16);F(129+i*16:144+i*16);F(257+i*16:272+i*16);F(385+i*16:401+i*16)]; 
    end
end

%% filter test signal 1
if fig1
    figure(1);
    subplot(2,1,1);
    
    plot(t(St:Nn),y(St:Nn));
    hold on;
    for i = 1:8
        z(:,i) = filter(firr(:,i),1,y);
        plot(t(St:Nn),z(St:Nn,i));
    end
    zz = z(:,1)+z(:,2)+z(:,3)+z(:,4)+z(:,5)+z(:,6)+z(:,7)+z(:,8);

    subplot(2,1,2);
    plot(t(St:Nn),zz(St:Nn));
    hold off;
end

%% Test bar graphs

amplitudeA  = 1;
amplitudeB  = 0.6;
freqA       = 800;
freqB       = 100;
dutycycle   = 50;

test1       = amplitudeA*square(freqA*2*pi*t,dutycycle);
test2       = amplitudeA*square(freqB*2*pi*t,dutycycle);
test3       = amplitudeB*square(freqA*2*pi*t,dutycycle);
test4       = amplitudeB*square(freqB*2*pi*t,dutycycle);

if fig2
    figure(2);
    subplot(2,2,1);
    plot(t(St:Nn),test1(St:Nn));
    axis([0.005 0.02 0 1]);
    title('amp A, freq A');

    subplot(2,2,2);
    plot(t(St:Nn),test2(St:Nn));
    axis([0.005 0.02 0 1]);
    title('amp A, freq B');

    subplot(2,2,3);
    plot(t(St:Nn),test3(St:Nn));
    axis([0.005 0.02 0 1]);
    title('amp B, freq A');

    subplot(2,2,4);
    plot(t(St:Nn),test4(St:Nn));
    axis([0.005 0.02 0 1]);
    title('amp B, freq B');
end

if fig3
    figure();
    for i=1:8
        testOut(:,i,1) = filter(firr(:,i),1,test1); % input 1
        testOut(:,i,2) = filter(firr(:,i),1,test2); % input 2
        testOut(:,i,3) = filter(firr(:,i),1,test3);
        testOut(:,i,4) = filter(firr(:,i),1,test4);
        subplot(2,2,1);
        hold on;
        plot(t(St:Nn),testOut(St:Nn,i,1));
        subplot(2,2,2);
        hold on;
        plot(t(St:Nn),testOut(St:Nn,i,2));
        subplot(2,2,3);
        hold on;
        plot(t(St:Nn),testOut(St:Nn,i,3));
        subplot(2,2,4);
        hold on;
        plot(t(St:Nn),testOut(St:Nn,i,4));
    end
    hold off;
    
    % plot all fir filter outputs for freqA and ampA
    figure();
    for i=1:8
       subplot(4,2,i); 
       plot(t(St:Nn),testOut(St:Nn,i,1));
       hold on;
       plot(t(St:Nn),testOut(St:Nn,i,2));
       axis([0.005 0.019 -1 1]);
       title(num2str(i));
    end
    
    % extract amplitude information from all the filters
    figure();
    xs = 1:8;
    for k = 1:4
       for i = 1:8
          testOutRms(i,k) = rms(testOut(:,i,k));
       end
       subplot(2,2,k);
       bar(xs,testOutRms(:,k),1);
       axis tight;
    end
    
    % actual vocoder 
    for i = 1:8
        Voc1OutT(:,i) = abs(testOut(:,i,1)) .* testOut(:,i,2);
        Voc2OutT(:,i) = abs(testOut(:,i,2)) .* testOut(:,i,1);
    end
    Voc1Out = sum(Voc1OutT,2);
    Voc2Out = sum(Voc2OutT,2);
    figure();
    subplot(2,1,1);
    plot(t(St:2*Nn),Voc1Out(St:2*Nn));
    subplot(2,1,2);
    plot(t(St:2*Nn),Voc2Out(St:2*Nn));
    
end

%% plot figure
if fig4
    figure(); % plot them using the freqz tool
    for i = 1:8
        freqz(firr(:,i),1,Fs);
        hold all;
    end

    % alternate line color between red and blue to aid in visual inspection
    lines = findall(gcf,'type','line');

    for i = 1:2:8
        set(lines(i),'color','red');
    end

    hold off;
end

%% export test values
if printtest
    fileID = fopen('testvalues.txt','w');
    for i = 1:1
       fprintf(fileID,'\n\n filter %i :\n\n{',i);
        for k=1:100
            if k < 100
                fprintf(fileID,'%8.15f, ',zz(k));
            else
                fprintf(fileID,'%8.15f }',zz(k));
            end
       end
    end
end

%% export coefficients filter
if exportfir
    fileID = fopen('firvalues.txt','w');
    for q = 1:4
        for i = 1:8
            if q < 4
                if i == 1
                    fprintf(fileID,'double coeffL[NUMBER_OF_COEFFICIENTS_VOICE] = {');
                elseif i == 8
                    fprintf(fileID,'double coeffH[NUMBER_OF_COEFFICIENTS_VOICE] = {');
                else
                    fprintf(fileID,'double coeff%i[NUMBER_OF_COEFFICIENTS_VOICE] = {',i);
                end    
                for j = 1:n/4;
                    temp = j+(q-1)*n/4;
                    if j<n/4
                        fprintf(fileID,'%8.15f, ',firr(temp,i));
                    else
                        fprintf(fileID,'%8.15f',firr(temp,i));
                    end
                end
                fprintf(fileID,'}\n'); 
            else
                if i == 1
                    fprintf(fileID,'double coeffL[NUMBER_OF_COEFFICIENTS_VOICE] = {');
                elseif i == 8
                    fprintf(fileID,'double coeffH[NUMBER_OF_COEFFICIENTS_VOICE] = {');
                else
                    fprintf(fileID,'double coeff%i[NUMBER_OF_COEFFICIENTS_VOICE] = {',i);
                end 
                for j = 1:(n/4+1);
                    temp = j+(q-1)*n/4;
                    if j<n/4+1
                        fprintf(fileID,'%8.15f, ',firr(temp,i));
                    else
                        fprintf(fileID,'%8.15f',firr(temp,i));
                    end

                end
                fprintf(fileID,'}\n');        
            end
        end    
    end
end