
\section{System design}\label{sec:design}

In this section a general description of the system will be given. In section \ref{sec:vocexp} the basic explanation of the algorithm and the system is given. In sections \ref{sec:digimp} and \ref{sec:model} the consequences of implementing the algorithm in a digital environment are discussed and a dataflow model is presented. Finally in section \ref{sec:enabmulti} the requirements for parallel implementation of the algorithm are discussed.

\subsection{Vocoder}\label{sec:vocexp}

A so-called channel vocoder divides an input signal into different spectra using a set of bandpass filters. The amplitude of these outputs determine the amplitude of the same spectral regions of a different input signal. This means that for a real time system a lot of (parallel) processing power is necessary. 

The vocoder has been popular as a musical effect since the 1970s. Bands like Kraftwerk (Autobahn), Electric Light Orchestra (Mr. Blue Sky) and more recently Daft Punk all use or have used the vocoder. Especially for the latter the effect has been of great importance in forming the characteristic sound of the band.

To give a better overview of the system that was implemented figure \ref{fig:overview} shows a schematic overview of the system. There are eight parallel band pass filters on the 'voice' input and eight on the 'instrument' input. The amplitude of each part of the 'voice' input is determined using an envelope follower which has been tweaked to allow for following the amplitude of the signal without tracking all the peaks, so it functions as a sort of low pass filter.

The signals from these eight envelope followers are then used to control an amplifier which determines the output volume of the corresponding filter bands of the 'instrument' input. 
\begin{figure}[!hbt]
	\centering
	\input{./src/systemoverview.tex}
	\caption{An overview of the vocoder}
	\label{fig:overview}
\end{figure}


\subsection{Digital implementation}\label{sec:digimp}

In the digital domain there are a couple of things that need to be taken into account. The first step is getting the sampling frequency, this determines not only the bandwidth for the input signal but indirectly also how many coefficients are needed for a steep enough band pass filter. Therefore a balance needs to be found between bandwidth and system resources used for the filters. 

This leads to the implementation of the filters themselves. In the digital domain there are two main classes of filters, FIR's and IIR's. The Final Impulse Response filter has as the advantage of a linear pass band which guarantees stability for all input signals. Furthermore it is possible to make efficient implementations for this class of filters.

However it is not possible to simply implement a single FIR filter on a single core of the Spark multiprocessor since its register size is too limited for the large number of coefficients needed for the band pass filters. Therefore the implementation given in this report spreads the workload of a single FIR filter over four processors and calculates the answers sequentially instead of fully parallel. 

By using the above solution in combination with the pipe lining of the input data and coefficients it is still possible to get very high throughput and thus 'simulate' a number of parallel filters.

\clearpage

\subsection{Dataflow model}\label{sec:model}

Figure \ref{fig:dflow} shows a basic data flow model of a vocoder. This model still assumes parallel implementation of the various components. The system frequency is determined by the CLK actor which can fire according to its own firing duration. After a token is produced on both outputs of the CLK actor the ADC's take a 'sample' and consume the token produced by respectively V(oice)IN and I(nstrument)IN. These actors can instantaneously consume and produce another token to model the continuous time domain in front of the ADC's. The ADC's produce 8 tokens to model the fact that the eight parallel executions have to take place which is also indicated by the succeeding components that all contain a self-edge with 8 initial tokens to facilitate 8 concurrent executions. After the amplifier (VCA) the outputs of the eight parallel paths are SUMmed and send to the output. 

The firing rate of the operators are estimated as follows:
\begin{align}
	\rho_{CLK} = \frac{1}{f_{s}} = \frac{1}{16\cdot10^3} &= \SI{6250}{\micro s}\\
	\rho_{VIN} = \rho_{IIN} &= \SI{0}{\micro s}\\
	\rho_{ADC} &= \SI{5}{\micro s}\\
	\rho_{FIR} = \frac{n_{*}+n_{+}+n_{\text{overhead}}}{f_{\text{system}}}= \frac{65+65+40}{100\cdot10^6} &= \SI{1.7}{\micro s}\\
	\rho_{ENV} = \frac{n_{\text{abs}}}{f_{\text{system}}} = \frac{2}{100\cdot10^6} &= \SI{0.02}{\micro s}\\
	\rho_{SUM} = \frac{n_{\text{abs}}}{f_{\text{system}}} = \frac{8}{100\cdot10^6} &= \SI{0.08}{\micro s}\\
	\rho_{OUT} &= \SI{0}{\micro s}
\end{align}  

\begin{figure}[ht]
	\centering
	\input{./src/dflow.tex}
	\caption{A data flow model of the vocoder}
	\label{fig:dflow}
\end{figure}

\subsection{Enabling multicore usage}\label{sec:enabmulti}

Because of the very nature of a vocoder it is easy to envision an implementation where every filter is calculated by its own processor. However due to the fact that the register size of the cores on the microblaze are limited this is not readily done.

Therefore we decided to implement a pipelined construct where a set of samples is calculated by each filter and afterwards passed on to the next core. This means only the sample needs to be loaded into the register and the coefficients can be kept in the register space, greatly decreasing the load on the communication bus.