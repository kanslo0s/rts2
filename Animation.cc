// Rendering library demo
#include <helix.h>
#include <render/render.h>

int main(int argc, char **argv) {
	printf("Starting drawing demo\n");
	if(render_init(1) != RENDER_OK) {
		printf("Error: init display!\n");
		return 0;
	}


	int offset = 0;
	struct timeval tv;
	struct timeval tv2;

	while(1){
		gettimeofday(&tv, NULL);
		fillrect(0,0,DVI_WIDTH,DVI_HEIGHT,white);
		for(int j=0;j<5;j++)
		{
			for(int i=0;i<5;i++)
			{
				fillrect((i*2)*80+offset,(j*2)*60,(i*2+1)*80+offset,(j*2+1)*60,black);
				fillrect((i*2+1)*80+offset,(j*2+1)*60,(i*2+2)*80+offset,(j*2+2)*60,black);
			}
		}
		render_flip_buffer();

		if(offset>-160)
		{
			offset--;
		}
		else
		{
			offset=0;
		}
		gettimeofday(&tv2, NULL);
		printf("Framerate: %ld \n", 1000000/(((tv2.tv_sec - tv.tv_sec)*1000000L+tv2.tv_usec) - tv.tv_usec));

	}
	printf("Done\n");
	printf("i");
}

