#include <helix.h>

CFifo<bool,CFifo<>::w> *wr; // Write end point of a FIFO
CFifo<bool,CFifo<>::r> *rd; // Read end point of a FIFO

#define ERREXIT(str) {fprintf(stderr, "Error: " str "\n"); exit(1);}
#define ERREXIT2(str, ...) {fprintf(stderr, "Error: " str "\n",__VA_ARGS__); exit(1);}

void *ping(void *arg) {
	wr->validate(1);
	while(1) {
		printf("Ping1\n");
		wr->push(true);
		sleep(1);
		rd->pop();
	}
	return NULL;
}

void *ping2(void *arg) {
	wr->validate(2);
	while(1) {
		printf("\tPing2\n");
		wr->push(true);
		sleep(1);
		rd->pop();
	}
	return NULL;
}

void *ping3(void *arg) {
	wr->validate(3);
	while(1) {
		printf("\t\tPing3\n");
		wr->push(true);
		sleep(1);
		rd->pop();
	}
	return NULL;
}

void *ping4(void *arg) {
	wr->validate(4);
	while(1) {
		printf("\t\t\tPing4\n");
		wr->push(true);
		sleep(1);
		rd->pop();
	}
	return NULL;
}

int main(int argc, char **argv) {
	pid_t pid0, pid1, pid2, pid3;

	CFifoPtr<bool> fifo1 = CFifo<bool>::Create(1, wr, 2, rd, 4);
	CFifoPtr<bool> fifo2 = CFifo<bool>::Create(2, wr, 3, rd, 1);
	CFifoPtr<bool> fifo3 = CFifo<bool>::Create(3, wr, 4, rd, 2);
	CFifoPtr<bool> fifo4 = CFifo<bool>::Create(4, wr, 1, rd, 3);

	if(!fifo1.valid()) ERREXIT("Error creating buffer");
	if(!fifo2.valid()) ERREXIT("Error creating buffer");
	if(!fifo3.valid()) ERREXIT("Error creating buffer");
	if(!fifo4.valid()) ERREXIT("Error creating buffer");

	if(int e=CreateProcess(pid0, ping, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 1)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid1, ping2, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 2)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid2, ping3, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 3)) ERREXIT2("Process creation failed: %i", e);
	if(int e=CreateProcess(pid3, ping4, NULL, PROC_DEFAULT_TIMESLICE, PROC_DEFAULT_STACK, 4)) ERREXIT2("Process creation failed: %i", e);


	if(int e=SetProcessFlags(pid0, PROC_FLAG_JOINABLE, 1)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid1, PROC_FLAG_JOINABLE, 2)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid2, PROC_FLAG_JOINABLE, 3)) ERREXIT2("While setting process flags: %i", e);
	if(int e=SetProcessFlags(pid3, PROC_FLAG_JOINABLE, 4)) ERREXIT2("While setting process flags: %i", e);


	if(int e=StartProcess(pid0, 1)) ERREXIT2("Could not start ping: %i", e);
	if(int e=StartProcess(pid1, 2)) ERREXIT2("Could not start pong: %i", e);
	if(int e=StartProcess(pid2, 3)) ERREXIT2("Could not start pong: %i", e);
	if(int e=StartProcess(pid3, 4)) ERREXIT2("Could not start pong: %i", e);

	// FIFOs are destroyed when the pointers goes out of scope
	if(int e=WaitProcess(pid0, NULL, 1)) ERREXIT2("Waiting on ping %i@%i: %i\n", pid0, 1, e);

	if(int e=WaitProcess(pid1, NULL, 2)) ERREXIT2("Waiting on ping %i@%i: %i\n", pid1, 2, e);

	if(int e=WaitProcess(pid2, NULL, 3)) ERREXIT2("Waiting on pong %i@%i: %i\n", pid2, 3, e);

	if(int e=WaitProcess(pid3, NULL, 4)) ERREXIT2("Waiting on pong %i@%i: %i\n", pid3, 4, e);

	return 0;
}

